<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

//define vars
define('OIOPUB_LOAD_LITE', 1);

//init
include_once(str_replace("\\", "/", dirname(__FILE__)) . "/index.php");

//is plugin enabled?
if($oiopub_set->enabled != 1) {
	exit();
}

//clear vars
$output = '';
$data = array();
$types = array( 'banner'=>"banners", 'link'=>"links" );
$tracker = (bool) ($oiopub_module->tracker == 1 && $oiopub_set->tracker['enabled'] == 1);

//input vars
$type = oiopub_var('type', 'get');
$zone = (int) oiopub_var('zone', 'get');

//get zone info
$z = $types[$type] . "_" . $zone;
$z_def = $z . '_defaults';

//cache vars
$cache_update = false;
$cache_key = "json_api_" . $z;
$cache_timeout = 3600;

//output cached?
if($cache_timeout > 0 && $output = $oiopub_cache->get($cache_key, $cache_timeout)) {
	echo $output;
	exit();
}

//find matches from database
$ads = $oiopub_db->GetAll("SELECT * FROM " . $oiopub_set->dbtable_purchases . " WHERE item_channel='" . oiopub_type_check($type) . "' AND item_status='1' AND payment_status='1' AND item_type='$zone' ORDER BY payment_time ASC");

//process results
foreach($ads as $ad) {
	//start / end times
	$start = (int) $ad->payment_time;
	$end = (int) $ad->item_duration > 0 ? ($ad->payment_time + (86400 * $ad->item_duration)) : 0;
	//get clicks / impressions data
	if($tracker) {
		$date_sql = $end > 0 ? " AND date BETWEEN '" . date('Y-m-d', $start) . "' AND '" . date('Y-m-d', $end) . "'" : "";
		$stats = $oiopub_db->GetRow("SELECT SUM(total_clicks) as clicks, SUM(total_visits) as visits FROM " . $oiopub_set->dbtable_tracker_archive . " WHERE pid='" . $ad->item_id . "'" . $date_sql);
	}
	//add data row
	$data[] = array(
		'id' => $ad->item_id,
		'type' => $type,
		'zone' => $zone,
		'image' => $type == "link" ? "" : $ad->item_url,
		'anchor' => $type == "link" ? $ad->item_page : "",
		'link' => $type == "link" ? $ad->item_url : $ad->item_page,
		'link_track' => $oiopub_set->tracker_url . "/go.php?id=" . $ad->item_id,
		'tooltip' => $ad->item_tooltip,
		'html' => $ad->item_notes,
		'width' => (int) $oiopub_set->{$z}['width'],
		'height' => (int) $oiopub_set->{$z}['height'],
		'start' => $start,
		'end' => $end,
		'advertiser' => $ad->adv_name,
		'clicks' => (int) isset($stats->clicks) ? $stats->clicks : 0,
		'impressions' => (int) isset($stats->visits) ? $stats->visits : 0,
		'is_default' => false,
	);
}

//include default ads?
if(isset($oiopub_set->{$z_def}) && isset($_GET['defaults'])) {
	//set local vars
	$default = $oiopub_set->{$z_def};
	//loop through default ads
	for($i=1; $i <= count($default['type']); $i++) {
		//add data row
		$data[] = array(
			'id' => null,
			'type' => $type,
			'zone' => $zone,
			'image' => $type == "link" ? "" : $default['image'][$i],
			'anchor' => $type == "link" ? $default['anchor'][$i] : "",
			'link' => $type == "link" ? $default['url'][$i] : $default['site'][$i],
			'link_track' => "",
			'tooltip' => "",
			'html' => $type == "link" ? $default['desc'][$i] : $default['html'][$i],
			'width' => (int) $oiopub_set->{$z}['width'],
			'height' => (int) $oiopub_set->{$z}['height'],
			'start' => null,
			'end' => null,
			'advertiser' => null,
			'clicks' => 0,
			'impressions' => 0,
			'is_default' => true,
		);
	}
}

//stop here?
if(empty($data)) {
	echo "no data";
	exit();
}

//convert to json
$json = json_encode($data);
$json_strlen = strlen($json);
$output_pos = 0;

//human readable
for($i=0; $i<=$json_strlen; $i++) {
	$char = substr($json, $i, 1);
	if($char == '}' || $char == ']') {
		$output .= "\n";
		$output_pos --;
		for($j=0; $j < $output_pos; $j++) {
			$output .= '  ';
		}
	}
	$output .= $char;
	if($char == ',' || $char == '{' || $char == '[') {
		$output .= "\n";
		if($char == '{' || $char == '[') {
			$output_pos ++;
		}
		for($j=0; $j < $output_pos; $j++) {
			$output .= '  ';
		}
	}
}

//update cache?
if($cache_timeout > 0 && $output) {
	$oiopub_cache->write($cache_key, $output, $cache_timeout);
}

//display
echo $output;
exit();