<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

if(!defined('oiopub')) die();

//google checkout class
class oiopub_payment_gcheckout extends oiopub_payment {

	//init
	function oiopub_payment_gcheckout() {

	}
	
	//process form
	function process_form($rand_id) {
		//get purchase data
		$purchases = $this->_purchase_form_data($rand_id);
		//form data
		$this->_build_form($purchases);
	}
	
	//build form
	function _build_form($item) {
		global $oiopub_set;
		//form data
		$form  = '<html>';
		$form .= '<head>';
		$form .= '<title>' . $oiopub_set->site_name . ' - Processing Payment</title>';
		$form .= '</head>';
		$form .= '<body onload="document.form.submit();">';
		$form .= '<h3>Processing Payment...</h3>';
		if($oiopub_set->sandbox == 1) {
			$form .= '<form action="https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/' . $oiopub_set->gcheckout['merchant_id'] . '" name="form" method="post" accept-charset="utf-8">';
		} else {
			$form .= '<form action="https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/' . $oiopub_set->gcheckout['merchant_id'] . '" name="form" method="post" accept-charset="utf-8">';
		}
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.item-name" value="' . $item['name'] . '" />';
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.item-description" value="' . $item['description'] . '" />';
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.unit-price" value="' . $item['cost'] . '" />';
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.unit-price.currency" value="' . $item['currency'] . '" />';
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.quantity" value="' . $item['quantity'] . '" />';
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.digital-content.display-disposition" value="OPTIMISTIC" />';
		$form .= '<input type="hidden" name="shopping-cart.items.item-1.digital-content.description" value="It may take up to 24 hours to fully process your order. Please contact ' . $oiopub_set->admin_mail . ' if you have any questions about your purchase." />';
		$form .= '<input type="hidden" name="_charset_"/>';
		$form .= '<input type="submit" value="Continue to checkout" id="continue" />';
		$form .= '</form>';
		$form .= '<script type="text/javascript">';
		$form .= 'document.getElementById("continue").style.visibility = "hidden";';
		$form .= '</script>';
		$form .= '</body>';
		$form .= '</html>';
		echo $form;
	}
	
}

?>