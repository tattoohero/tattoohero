<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

//define vars
define('OIOPUB_STATS', 1);

//init
include_once(str_replace("\\", "/", dirname(__FILE__)) . "/index.php");

//is plugin enabled?
if($oiopub_set->enabled != 1) {
	die("Plugin Currently Offline");
}

//start session
oiopub_session_start();

//$_POST form?
if(isset($_POST['process']) && $_POST['process'] == 'credentials') {
	//set vars
	$email = oiopub_clean($_POST['email']);
	$rand_id = oiopub_clean($_POST['rand']);
	//valid data?
	if($email && $rand_id) {
		//db check passed?
		if($check = $oiopub_db->GetRow("SELECT * FROM " . $oiopub_set->dbtable_purchases . " WHERE rand_id='$rand_id' AND adv_email='$email'")) {
			//set session
			$_SESSION['oio_stats_email'] = md5($email);
			//redirect user
			header("Location: stats.php?rand=" . $rand_id);
			exit();
		}
	}
}

//check for ID
$output = '';
$action = false;
$rand_id = oiopub_clean($_GET['rand']);

//purchase data
$purchase = $oiopub_db->GetRow("SELECT * FROM " . $oiopub_set->dbtable_purchases . " WHERE rand_id='$rand_id'");
$pdata = oiopub_adtype_info($purchase);

//email address check
if(isset($_SESSION['oio_stats_email']) && $_SESSION['oio_stats_email'] == md5($purchase->adv_email)) {
	$allow_access = true;
	$email = $purchase->adv_email;
} else {
	$allow_access = false;
}

//admin over-ride?
if($purchase && (oiopub_auth_check() || $oiopub_set->demo)) {
	$allow_access = true;
	$_SESSION['oio_stats_email'] = md5($purchase->adv_email);
}

//get all purchases
$purchase_list = array();
$all_purchases = $oiopub_db->GetAll("SELECT item_channel,item_type,submit_time,rand_id FROM " . $oiopub_set->dbtable_purchases . " WHERE adv_email='$purchase->adv_email' AND adv_email!='' ORDER BY submit_time DESC");
foreach($all_purchases as $p) {
	$adtype = oiopub_adtype_info($p);
	$purchase_list[$p->rand_id] = $adtype['type'] . " - " . date("jS M Y", $p->submit_time) . " &nbsp; (" . $p->rand_id . ")";
}

//template vars
$templates = array();
$templates['page'] = "purchase_stats";
$templates['title'] = __oio("Advertising dashboard") . '<form method="get" action="' . $oiopub_set->request_uri . '" name="purchase" style="margin:8px 0 0 0; font-size:12px;"><b>' . __oio("My ads") . ':</b> &nbsp; ' . oiopub_dropmenu_kv($purchase_list, 'rand', $purchase->rand_id, 340, 'document.purchase.submit()') . '</form>';
$templates['title_head'] = __oio("Advertising dashboard");
//load template
include_once($oiopub_set->folder_dir . "/templates/core/main.tpl");

?>