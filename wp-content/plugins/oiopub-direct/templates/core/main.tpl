<?php
//header include
function oiopub_header_inc($templates) {
	if(!defined('NO_HEADER')) {
		global $oiopub_set;
		include_once($oiopub_set->template_header);
	}
}
oiopub_header_inc($templates);
?>

<link type="text/css" href="<?php echo $oiopub_set->plugin_url; ?>/libs/bubble/bubble.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $oiopub_set->plugin_url; ?>/libs/bubble/bubble.js"></script>
<script type="text/javascript" src="<?php echo $oiopub_set->plugin_url; ?>/libs/misc/oiopub.js"></script>
<script type="text/javascript">window.onload = function(){ enableTooltip('oiopub-container'); }</script>

<div id="oiopub-container" style="padding:20px 0;">

<?php
//content paths
$paths = array(
	'custom' => $oiopub_set->folder_dir . "/templates/core_custom",
	'standard' => $templates['path'] ? $templates['path'] : $oiopub_set->folder_dir . "/templates/core",
);
//loop through paths
foreach($paths as $path) {
	//get file
	$file = $path . "/" . $templates['page'] . ".tpl";
	//does file exist?
	if($key == 'standard' || is_file($file)) {
		include_once($file);
		break;
	}
}
?>

<?php
//javascript insert
echo $templates['javascript'];
$oiopub_hook->fire('content_end');
?>

</div>

<?php
//footer include
function oiopub_footer_inc($templates) {
	if(!defined('NO_FOOTER')) {
		global $oiopub_set;
		include_once($oiopub_set->template_footer);
	}
}
oiopub_footer_inc($templates);
?>