=======================================
OIOpublisher Ad Manager, v2 Readme File
=======================================


(1) REQUIREMENTS

- a web host with php 4.2 and MySQL 4.1 (or above)
- a domain name linked to the web hosting account


(2) TUTORIALS / TROUBLESHOOTING

- a selection of tutorials is available at http://docs.oiopublisher.com, to help you with installation and configuration
- if you experience any installation problems, please check out the troubleshooting tutorial at http://docs.oiopublisher.com/troubleshoot/
- also included is information on how to find your web server FTP & MySQL info, a full installation guide and ad zone configuration help
- support is also available via the forum at http://forum.oiopublisher.com


(3) QUICK INSTALLATION

a. Using the Web Installer:

- an automated installer that uploads and configures your installation, available at http://www.oiopublisher.com/installer/

b. Wordpress Mode:

- see the installation tutorial at http://forum.oiopublisher.com/discussion/507/installing-the-ad-manager/#Item_5

c. Standalone Mode:

- see the installation tutorial at http://forum.oiopublisher.com/discussion/507/installing-the-ad-manager/#Item_5


(4) UPGRADING THE SCRIPT

- see the upgrade tutorial at http://forum.oiopublisher.com/discussion/507/installing-the-ad-manager/#Item_6


(5) API Key

- an API key is NOT required to use OIOpublisher, it is only used for transferring data to and from the Marketplace
- each install of OIOpublisher requires a separate API key, which you can obtain by registering the site
- to register your site, login to your OIOpublisher account and go to http://www.oiopublisher.com/submit.php
- once your listing has been approved, you can find your unique API key at http://www.oiopublisher.com/account_data.php
