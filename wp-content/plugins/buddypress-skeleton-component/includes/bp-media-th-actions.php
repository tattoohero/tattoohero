<?php
require_once( ABSPATH . '/wp-content/plugins/buddypress-media/app/main/includes/BPMediaHostWordpress.php' );
/*
* This file makes the adjustments to the plugin, BP Media, that were needed.
* Record of Change format: Change, File, function, hook added, code removed.
*/


/*
* Change: Add Image to the edit screen.
* File: app/main/profile/BPMediaScreen.php
* Function: edit_screen_content
* Hook Added: th_edit_screen_image
* Code Removed: N/A
*/
function add_image_to_edit_screen(){
	?>
	<h4>*** Random Message ***</h4>
	<?php
}
add_action('th_edit_screen_image', 'add_image_to_edit_screen');
?>