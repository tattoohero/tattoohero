<?php

/*
 * The main plugin class, holds everything our plugin does,
 * initialized right after declaration
 */
class Settings_API_Buddy_Socialness_Plugin {
	
	/*
	 * For easier overriding we declared the keys
	 * here as well as our tabs array which is populated
	 * when registering settings
	 */
	private $components_settings_key = 'my_components_settings';
	private $advanced_settings_key = 'my_advanced_settings';
	private $plugin_options_key = 'my_plugin_options';
	private $plugin_settings_tabs = array();
	
	/*
	 * Fired during plugins_loaded (very very early),
	 * so don't miss-use this, only actions and filters,
	 * current ones speak for themselves.
	 */
	function __construct() {
		add_action( 'init', array( &$this, 'load_settings' ) );
		add_action( 'admin_init', array( &$this, 'register_general_settings' ) );
		add_action( 'admin_init', array( &$this, 'register_advanced_settings' ) );
		add_action( 'admin_menu', array( &$this, 'add_admin_menus' ) );
	}
	
	/*
	 * Loads both the general and advanced settings from
	 * the database into their respective arrays. Uses
	 * array_merge to merge with default values if they're
	 * missing.
	 */
	function load_settings() {
		$this->general_settings = (array) get_option( $this->components_settings_key );
		$this->advanced_settings = (array) get_option( $this->advanced_settings_key );
		
		// Merge with defaults
		$this->general_settings = array_merge( array(
			'buddypress_component' => 'both',
			'buddy_social_color_0' => '',
			'buddy_social_color_1' => ''
		), $this->general_settings );
		
		$this->advanced_settings = array_merge( array(
			'advanced_option' => 'Advanced value'
		), $this->advanced_settings );
	}
	
	/*
	 * Registers the general settings via the Settings API,
	 * appends the setting to the tabs array of the object.
	 */
	function register_general_settings() {
		$this->plugin_settings_tabs[$this->components_settings_key] = 'Components';
		
		register_setting( $this->components_settings_key, $this->components_settings_key );
		add_settings_section( 'section_general', 'Buddy Social components', array( &$this, 'section_general_desc' ), $this->components_settings_key );
		add_settings_field( 'buddy_social_component', 'Available social networks', array( &$this, 'field_buddy_social_component' ), $this->components_settings_key, 'section_general' );
		add_settings_field( 'buddy_social_color', 'Social button colors', array( &$this, 'field_buddy_social_color' ), $this->components_settings_key, 'section_general' );

	}
	
	/*
	 * Registers the advanced settings and appends the
	 * key to the plugin settings tabs array.
	 */
	function register_advanced_settings() {
		$this->plugin_settings_tabs[$this->advanced_settings_key] = 'Premium Version';
		
		register_setting( $this->advanced_settings_key, $this->advanced_settings_key );
		add_settings_section( 'section_advanced', '<!--Limited offer at only $3.50! <a href="">Get it now..</a>-->', array( &$this, 'section_advanced_desc' ), $this->advanced_settings_key );
	}
	
	/*
	 * The following methods provide descriptions
	 * for their respective sections, used as callbacks
	 * with add_settings_section
	 */
	function section_general_desc() { echo ''; }
	function section_advanced_desc() { echo '<a href=""><img src="' . plugins_url('images/premium-banner.png', __FILE__ ) . '"></a>'; }
	
	/*
	 * General Option field callback, renders a
	 * text input, note the name and value.
	 */

	function field_buddy_social_component() {
		?>

		<!--Facebook-->
		<input type="checkbox" name="<?php echo $this->components_settings_key; ?>[buddy_social_component_0]" value="facebook" <?php if (esc_attr( $this->general_settings['buddy_social_component_0'] ) == 'facebook') { echo 'checked';}; ?> style="margin: -2px 0 0">
		<span class="social foundicon-facebook" style="font-size: 16px;"></span> Facebook<br>
		<!--Twitter-->
		<input type="checkbox" name="<?php echo $this->components_settings_key; ?>[buddy_social_component_1]" value="twitter" <?php if (esc_attr( $this->general_settings['buddy_social_component_1'] ) == 'twitter') { echo 'checked';}; ?> style="margin: -2px 0 0">
		<span class="social foundicon-twitter" style="font-size: 16px;"></span> Twitter<br>
		<!--Google+-->
		<input type="checkbox" name="<?php echo $this->components_settings_key; ?>[buddy_social_component_2]" value="google" <?php if (esc_attr( $this->general_settings['buddy_social_component_2'] ) == 'google') { echo 'checked';}; ?> style="margin: -2px 0 0">
		<span class="social foundicon-google-plus" style="font-size: 16px;"></span> Google+<br>
		<!--Email-->
		<input type="checkbox" name="<?php echo $this->components_settings_key; ?>[buddy_social_component_3]" value="email" <?php if (esc_attr( $this->general_settings['buddy_social_component_3'] ) == 'email') { echo 'checked';}; ?> style="margin: -2px 0 0">
		<span class="general foundicon-mail" style="font-size: 16px;"></span> Email<br>

		<?php
	}

	function field_buddy_social_color() {
		?>

		<input type="text" name="<?php echo $this->components_settings_key; ?>[buddy_social_color_0]" value="<?php echo esc_attr( $this->general_settings['buddy_social_color_0'] ) ?>" class="my-color-field" /> Button color<br>
		<input type="text" name="<?php echo $this->components_settings_key; ?>[buddy_social_color_1]" value="<?php echo esc_attr( $this->general_settings['buddy_social_color_1'] ) ?>" class="my-color-field" /> Button hover color

		<?php
	}
	
	/*
	 * Called during admin_menu, adds an options
	 * page under Settings called My Settings, rendered
	 * using the plugin_options_page method.
	 */
	function add_admin_menus() {
		add_options_page( 'My Plugin Settings', 'Buddy Social', 'manage_options', $this->plugin_options_key, array( &$this, 'plugin_options_page' ) );
	}

	/*
	 * Plugin Options page rendering goes here, checks
	 * for active tab and replaces key with the related
	 * settings key. Uses the plugin_options_tabs method
	 * to render the tabs.
	 */
	function plugin_options_page() {
		$tab = isset( $_GET['tab'] ) ? $_GET['tab'] : $this->components_settings_key;
		?>
		<div class="wrap">
			<?php $this->plugin_options_tabs(); ?>
			<form method="post" action="options.php">
				<?php wp_nonce_field( 'update-options' ); ?>
				<?php settings_fields( $tab ); ?>
				<?php do_settings_sections( $tab ); ?>
				<?php if ($tab == "my_components_settings") submit_button(); ?>
			</form>

				<!-- Begin MailChimp Signup Form -->
					<br>
					<br>
					<link href="http://cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
					<style type="text/css">
						#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
						/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
						   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
					</style>
					<div id="mc_embed_signup">
					<form style="padding:0;" action="http://ngiya.us7.list-manage1.com/subscribe/post?u=379ab6f5cfb395578ae35f5c5&amp;id=c8c4f2204f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<h2>Get notified when Buddypress Social Premium launches!</h2>
					<div class="mc-field-group" style="width: 300px;padding-bottom: 10px;">
						<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
					</label>
						<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
					</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>	<div class="clear"><input style="color: #ffffff;text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); margin-bottom: 30px;background-color: #49afcd; background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4); background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4)); background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4); background-image: -o-linear-gradient(top, #5bc0de, #2f96b4); background-image: linear-gradient(to bottom, #5bc0de, #2f96b4); background-repeat: repeat-x; border-color: #2f96b4 #2f96b4 #1f6377; border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#ff5bc0de", endColorstr="#ff2f96b4", GradientType=0); filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					</form>
					</div>

				<!--End mc_embed_signup-->

		</div>
		<?php
	}
	
	/*
	 * Renders our tabs in the plugin options page,
	 * walks through the object's tabs array and prints
	 * them one by one. Provides the heading for the
	 * plugin_options_page method.
	 */
	function plugin_options_tabs() {
		$current_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : $this->components_settings_key;

		echo '<div id="icon-buddypress" class="icon32"><br></div>';
		echo '<h2 class="nav-tab-wrapper">';
		foreach ( $this->plugin_settings_tabs as $tab_key => $tab_caption ) {
			$active = $current_tab == $tab_key ? 'nav-tab-active' : '';
			echo '<a class="nav-tab ' . $active . '" href="?page=' . $this->plugin_options_key . '&tab=' . $tab_key . '">' . $tab_caption . '</a>';	
		}
		echo '</h2>';
	}
};

// Initialize the plugin
add_action( 'plugins_loaded', create_function( '', '$settings_api_tabs_demo_plugin = new Settings_API_Buddy_Socialness_Plugin;' ) );