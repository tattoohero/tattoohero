<?php
 
if ( !defined( 'BP_AVATAR_THUMB_WIDTH' ) )
define( 'BP_AVATAR_THUMB_WIDTH', 50 ); //change this with your desired thumb width
 
if ( !defined( 'BP_AVATAR_THUMB_HEIGHT' ) )
define( 'BP_AVATAR_THUMB_HEIGHT', 50 ); //change this with your desired thumb height
 
if ( !defined( 'BP_AVATAR_FULL_WIDTH' ) )
define( 'BP_AVATAR_FULL_WIDTH', 300 ); //change this with your desired full size,weel I changed it to 260 <img src="http://buddydev.com/wp-includes/images/smilies/icon_smile.gif?84cd58" alt=":)" class="wp-smiley"> 
 
if ( !defined( 'BP_AVATAR_FULL_HEIGHT' ) )
define( 'BP_AVATAR_FULL_HEIGHT', 300 ); //change this to default height for full avatar
 
function restrict_access(){
	global $bp, $bp_unfiltered_uri;
 	
 	if (!is_user_logged_in() && (BP_MEMBERS_SLUG == $bp_unfiltered_uri[0] || BP_GROUPS_SLUG == $bp->current_component || BP_BLOGS_SLUG == $bp->current_component || 'forums' == $bp->current_component || is_page_template('private-page.php'))){
 	bp_core_redirect( get_option('home') . '/private/' );
 	}
}
add_action( 'wp', 'restrict_access', 3 );
?>