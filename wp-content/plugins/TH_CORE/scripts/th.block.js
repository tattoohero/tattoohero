$_ = jQuery.noConflict();
$_(document).ready(function(){
	$_(".th_block .content .panel:first-child").addClass('active');
	$_(".th_block .nav li:first-child a").addClass('active');
	$_(".th_block .nav a").on('click',function(e){
		$_('.th_block .nav a').removeClass('active');
		$_(this).addClass('active');
		var target = $_(this).attr('href');
		var panels = $_(".th_block .content .panel");
		panels.removeClass('active');
		$_(target).addClass('active');
	});
	if(window.location.hash){
		$_(".th_block .content .panel").removeClass('active');
		$_(window.location.hash).addClass('active');
		$_('.th_block .nav a').removeClass('active');
		$_('[href="'+window.location.hash+'"]').addClass('active');
	}
});