<?php
/*
Plugin Name: TH ADMIN Core
Version: 1.0
Author: TH
Description: TH modules
*/

if (!function_exists('add_action')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit("This page can't be loaded outside of WordPress!"); }
global $wp_version;
if (version_compare($wp_version, "3.4.2", "<")) { exit("<b>TH ADMIN Core</b> requires WordPress 3.4.2 OR newer. <a href=\"http:/codex.wordpress.org/Upgrading_WordPress\">Please update!</a>"); }

define('TH_CORE_URL', plugin_dir_url( __FILE__ ));

function th_css() { if (is_admin()) { wp_enqueue_style('th_core_css',TH_CORE_URL.'css/th.core.css'); } }

add_action( "admin_print_styles", th_css() );

/**
 *	Q8 BLOCK and DEPENDANCIES
 */

if ( !class_exists( 'TH_BLOCK' ) ):
class TH_BLOCK {

	var $arr = array();
	/**
	 *	@function	draw_block
	 *	@params 	<title:string>,<version:string>,<description:string>,<footer:string>
	 *	@return 	<void>
	 */
	public function draw_block($title,$version=NULL,$description=NULL,$footer=NULL){
		?>
		<div class="wrap">
       		<div class="th_block">
            	<div class="header">
            		<h3><?php echo $title; ?> <small><?php echo $version; ?></small></h3>
            		<div class="description">
            			<p><?php echo $description; ?></p>
            		</div>
            	</div>
            	<div class="body">
            		<div class="nav">
            			<ul>
            				<?php echo $this->create_navigation(); ?>
            			</ul>
            		</div>
            		<div class="content">
            			<?php echo $this->create_tab_block(); ?>
            		</div>
            	</div>
            	<div class="footer">
            		<p><?php echo $footer; ?></p>
            	</div>
            </div>
        </div>
        <?php
	}
	/**
	 *	@function	add_tab
	 *	@params 	<id:string>,<title:string>,<callback:array>
	 *	@return 	<void>
	 */
	public function add_tab($id,$title,$callback){
		$this->arr[] = array('id'=>$id,'title'=>$title,'callback'=>$callback);
	}
	/**
	 *	@function	create_navigation
	 *	@params 	<void>
	 *	@return 	<li:string>
	 */
	private function create_navigation() {
		$arr = $this->arr;
		foreach($arr as $a):
			$li .= '<li><a href="#'.$a['id'].'">'.$a['title'].'</a></li>';
		endforeach;
		return $li;
	}

	/**
	 *	@function	create_tab_block
	 *	@params 	<void>
	 *	@return 	<tab_block:string>
	 */
	private function create_tab_block() {
		$arr = $this->arr;
		$i=0;
		foreach($arr as $a): 
			$i++;
			ob_start();
			call_user_func($a['callback']);
			$callback = ob_get_contents();
			ob_end_clean();
			$tab_block .= '<div id="'.$a['id'].'" class="panel">'.$callback.'</div>';
		endforeach;
		return $tab_block;
	}

	public static function css() { if (is_admin()) { wp_enqueue_style('th_block_css',TH_CORE_URL.'css/th.block.css'); } }
	public static function js() { if (is_admin()) { wp_enqueue_script('th_block_js',TH_CORE_URL.'scripts/th.block.js'); } }
}
endif;

add_action( "admin_print_styles", array( 'TH_BLOCK', "css" ) );
add_action( "admin_print_scripts", array( 'TH_BLOCK', "js" ) );



?>