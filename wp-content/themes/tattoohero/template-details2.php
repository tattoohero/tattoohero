<?php
/* TEMPLATE NAME: PROFILE DETAILS - PAGE 2*/ 
$tpl_name = 'details2';

//session_start();

//check if user's logged in
if (!is_user_logged_in()) wp_redirect( '/login' );
else $current_url = get_permalink($post->ID);

?>

<?php get_header(); ?>
	<div class="subpage container container-article profile-details profile-details2">
		<div class="grid_12 article-banner">
        	<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/details_image02b.jpg" />
        	<div class="grid_4 push_6 pd_slogan">
        		<img class="big" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_slogan_01.png" />
        		<img class="sub" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_txt_02.png" />
        	</div>
		</div>
        <div class="container-article-content">
            <section class="subpage grid_10">
                <div class="progressBox">
	                <div id="info" class="completed col_1of3"><a href="/profiledetails" title="quick info"></a>
		                <div class="dot"></div>
		                <div class="line"></div>
	                </div>
	                <div id="share" class="active col_1of3"><a href="" title="share and upload"></a>
	                	<div class="dot"></div>
	                	<div class="line"></div>
	                </div>
	                <div id="start" class="col_1of3"><a href="/profiledetails3" title="start browsing"></a>
	                </div>
                </div>
                <?php do_action( 'template_notices' ); ?>
                <?php $TH->show_errors(); ?>
                
                <div class="pre">
	                <?php //loop to get content
					if (have_posts()) : while (have_posts()) : ?>
					<?php the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif;?>              
				</div>
				<div class="content">
					<?php th_display_upload_form(); ?>
				</div>
            </section>
            <section class="albumContainer grid_12">
        	<?php //This only runs once (page load), even if we upload new images 	
        	if($_SESSION['albumIDs'] != null){
        		/*while( list( $field, $value ) = each( $_POST )) {
				   echo "<p>" . $field . " = " . $value . "</p>\n";
				}*/
				foreach($_SESSION['albumIDs'] as $ID){
					th_edit_new_images_form($ID); 
				}
			}
			
			?>
				<div class="loaderBox hide">
					<div class="ajaxLoader"></div>
				</div>	
            </section>
            <section class="grid_10">
            	<form id="profileForm" method="POST" action="<?php echo $current_url; ?>">
		        	<div class="profileButtons">
		        		<a href="<?php if(th_isArtist(bp_loggedin_user_id()) && (!empty($_SESSION['shopName']) && !is_shop($_SESSION['shopName']))) echo '/profiledetails1-5'; else echo '/profiledetails' ?>" class="btn btn-primary btn-large">Prev</a>
		        		<input type="hidden" name="profileDetails_action" value="step2"/>
		        		<input id="details_submit" class="btn btn-primary btn-large col_3of4" type="submit" value="<?php _e('Next', 'tattoohero'); ?>"/>
	                </div>
	            </form>
                <!--<script type="text/javascript">
    		    	jQuery(document).ready(function(){
    		    		//var submits = $nextButton.attr('data-submits');
					    $nextButton = jQuery('#details_submit');
				    	$nextButton.click(function(){
					    <?php foreach($_SESSION['albumIDs'] as $ID): ?>
					    	//jQuery('#<?php echo $ID; ?>').find('input[type=submit]').trigger('click');//.submit();
				    	<?php endforeach; ?>
				    	
				    	});
				    });
				</script>-->
            </section>
        </div><!--.pull-up -->
    </div>
<?php get_footer(); ?>