<?php

 /* TEMPLATE NAME: INSURANCE */ 
global $prefix, $captcha_instance;



$captcha_instance = new ReallySimpleCaptcha();
$word = $captcha_instance->generate_random_word();
$prefix = mt_rand();
$captcha_instance->generate_image( $prefix, $word );

 ?>
  <STYLE type="text/css">
  #insurance {padding-right:20px; padding-left:20px;}
   #insurance-sig p {margin-left: 35%; margin-top: 0; margin-bottom: 0;}
 </STYLE>
 <?php get_header(); ?>
	<div class="ins-container" width="1500">
	 	<div class="col_2of3">
	 		<a href="http://tattoohero.com/?attachment_id=3689" rel="attachment wp-att-3689"><img src="http://tattoohero.com/wp-content/uploads/2013/08/TH_Insurance_Form_LEFT_IMAGE.png" alt="TH_Insurance_Form_LEFT_IMAGE" width="954" height="865" class="alignnone size-full wp-image-3689" /></a>
	 	</div>
	 	<!-- <div class="clr"></div> -->
	 	<div class="col_1of3" id="insurance">
	 		<img src="<?php ABSPATH ?>/wp-content/themes/tattoohero/images/Insurance_logo_cut.png" alt="SW Logo" width="585" height="460" class="alignnone size-full wp-image-3690" /></a>
	 		<h2>GET COVERED.</h2>
	 		<h2>INSURANCE THAT FITS YOUR BUSINESS.</h2>
	 		<p>Tattoos and body piercing are increasing in popularity … but not so much with insurance companies. That's why South Western Group & Donnie Conner, specialists in niche products and hard-to-place risks, drew up some coverage for practitioners of Body Art. Introducing our new Professional & General Liability – Claims Made Policy. </p>
			<table >
				<tr>
					<td width="300">FREE, NO OBLIGATION QUOTE</td>
					<td width="200" align="right">SAVE UP TO 30%</td>
				</tr>
			</table>
			<p></p>

			<!-- <p>FREE, NO OBLIGATION QUOTE</p><span><p align="right">SAVE UP TO 30%</p></span> -->

			<form method="post">
			  <input type="text" name="ins_name" value="<?php if($_POST['ins_name']) {echo $_POST['ins_name'];}else{echo 'NAME';}?>"><br>
			  <input type="email" name="ins_email" value="<?php if($_POST['ins_email']) {echo $_POST['ins_email'];}else{echo 'EMAIL';}?>"><br>
			  <input type="text" name="ins_shop" value="<?php if($_POST['ins_shop']) {echo $_POST['ins_shop'];}else{echo 'SHOP NAME';}?>"><br>
			  <img src="<?php echo "wp-content/plugins/really-simple-captcha/tmp/".$captcha_instance->generate_image( $prefix, $word ); ?>" >
			  <input type="text" name="ins_captcha_response" Value = "Enter the four charactors above."><br>
			  <input type="hidden" name="ins_captcha_prefix" value="<?php echo $prefix ?>">

			  <input type="submit" value="SUBMIT FOR QUOTE" align="right">
			</form>
			<img src="<?php ABSPATH ?>/wp-content/themes/tattoohero/images/Donnie_Sig_cut.png" alt="SW Logo" width="585" height="460" class="alignnone size-full wp-image-3690" /></a>
			<div id="insurance-sig">
				<p>Donnie Conner,</p>
				<p>BA, Econ, B. Comm, RIBO</p>
				<p>Account Executive</p>
				<p>Commercial Insurance</p>
			</div>
	 	</div>
	</div>

