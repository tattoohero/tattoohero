<?php /* TEMPLATE NAME: USERAVATARS */ ?>

<?php  
	function get_avatar_url($get_avatar){
    	preg_match('/src="(.*)" alt=/', $get_avatar, $matches);
    	if ($matches[1] == ""){
    		preg_match('/src="(.*)" class=/', $get_avatar, $matches);
    		$matches[1] = str_replace("square", "large",$matches[1]);
    	}
    	return $matches[1];
	}
?>
<?php if ( bp_has_members('per_page=2000')) : ?>
	<?php 

	$i=0; while ( bp_members() ) : bp_the_member(); $i++; 
	$avatar = bp_core_fetch_avatar( array(
		    'item_id' => bp_get_member_user_id(),
		    'type'     => 'full',
		    'width'    => 150,
		    'height'  => 150
		));
	$avatar_url = get_avatar_url($avatar);
	if (!preg_match('/mystery-man.jpg/i',$avatar_url )){
		print bp_get_member_user_id()."| ".$avatar_url;
		?>
		<br>
		<?php
	}
	
	//print bp_get_member_username();
	
	
	endwhile; ?>
<?php endif; ?>
