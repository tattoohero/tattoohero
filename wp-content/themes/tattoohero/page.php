<?php get_header(); ?>
	<div class="subpage container">
		<div class="grid_12">

			<?php do_action( 'bp_before' ); ?>

			<div role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<h2><?php the_title(); ?></h2>

					<?php the_content(); ?>

				<?php endwhile; endif; ?>

			</div>
		</div><!-- .grid_12 -->
	</div>

<?php get_footer(); ?>
