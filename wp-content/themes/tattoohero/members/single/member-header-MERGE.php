<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>

<div class="user-avatar grid_4 alpha">
	<a href="<?php bp_displayed_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?>

	</a>
</div><!-- .user-avatar -->

<div class="member-header-content grid_8 omega prefix_half">

	<h2>
		<a href="<?php bp_displayed_user_link(); ?>"><?php bp_displayed_user_fullname(); ?></a>
		<span class="user-nicename">@<?php bp_displayed_user_username(); ?></span>
	</h2>
	<span class="user-city"><?php bp_member_profile_data( 'field=City' ); ?></span> 
	<span class="user-city">, <?php bp_member_profile_data( 'field=Province/State' ); ?></span>	
	<!-- <span class="activity"><?php bp_last_activity( bp_displayed_user_id() ); ?></span> -->

	<?php do_action( 'bp_before_member_header_meta' ); ?>

	<div id="item-meta">

		<?php if ( bp_is_active( 'activity' ) ) : ?>

			<div id="latest-update">

				<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>

			</div>

		<?php endif; ?>


		<?php
		/***
		 * If you'd like to show specific profile fields here use:
		 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
		 */
	  do_action( 'bp_profile_header_meta' );

		 ?>

	</div><!-- #item-meta -->
	
	<div class="user-likes verticalSpacer">
		<p class="icon iview"><span class="data-views"> 167,872 </span>VIEWS</p>
		<p class="icon"><span class="data-like"> 4,771 </span>LIKES</p>
	</div><!--.user-likes-->

	<div id="item-buttons" class="user-socialActions verticalSpacer">

		<?php do_action( 'bp_member_header_actions' ); ?>
		<div class="clr"></div>
		<div class="verticalSpacer">
			<button class="btn icon iplus">Share</button>
			<button class="btn icon">Like</button>
		</div>
	</div><!-- #item-buttons -->
	<div class="clr"></div>
	

</div><!-- .member-header-content -->

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>