<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>
<div class="member-options-nav">
	<ul>
		<li class="btn-profilemenu">
			<span class="arrowfont">&#xe00b;</span>&nbsp;&nbsp;Profile Menu
			<ul class="dropdown">
				<?php bp_get_displayed_user_nav(); ?>
				<?php do_action( 'bp_member_options_nav' ); ?>
			</ul>
		</li>
	</ul>
</div>
<div class="user-avatar grid_2 alpha">
	<a href="<?php bp_displayed_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?>

	</a>
</div><!-- .user-avatar -->

<div class="member-header-content grid_10 omega prefix_half">

	<h2>
		<a href="<?php bp_displayed_user_link(); ?>"><?php bp_displayed_user_fullname(); ?></a>
		<span class="user-nicename">@<?php bp_displayed_user_username(); ?></span>
	</h2>
	<div class="col_3of10 alpha">
		<div class="meta">
			<?php 
				$city = xprofile_get_field_data( "City" , $curauth->ID );
				$province = xprofile_get_field_data( "Province/State" , $curauth->ID );
				$memtype = xprofile_get_field_data( "Member Type" , $curauth->ID );
			?>
			<?php if($memtype): ?>
				<h3 class="user-memtype"> <?=$memtype;?></h3>
			<?php endif; ?>
			<?php if($city): ?>
				<span class="user-city"><?=$city;?><?php if($province) { echo trim(','); } ?> </span>
			<?php endif; ?>
			<?php if($province): ?>
				<span class="user-province"> <?=$province;?></span>
			<?php endif; ?>

		</div>
		<!-- <span class="activity"><?php bp_last_activity( bp_displayed_user_id() ); ?></span> -->

		<?php do_action( 'bp_before_member_header_meta' ); ?>

		<div id="item-meta">

			<?php if ( bp_is_active( 'activity' ) ) : ?>

				<div id="latest-update">

					<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>

				</div>

			<?php endif; ?>

			<?php
			/***
			 * If you'd like to show specific profile fields here use:
			 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
			 */
		  do_action( 'bp_profile_header_meta' );

			 ?>

		</div><!-- #item-meta -->
	</div>
	<div class="col_2of10">
			<?php vlc_view('profile'); ?>
		
		<div class="user-socialActions verticalSpacer">
			<!-- <button class="btn-social icon iplus">Share</button>
			<button class="btn-social icon">Like</button> -->
		</div><!--.user-socialActions"-->
	</div>

	<div class="col_5of10 omega" style="width:50%;">
		<?php //th_header_gallery(); ?>
	</div>
	<div class="clr"></div>
	<div id="item-buttons">
		<?php vlc_buttons('profile'); ?>
		<?php do_action( 'bp_member_header_actions' ); ?>
	</div><!-- #item-buttons -->
</div><!-- .member-header-content -->

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>