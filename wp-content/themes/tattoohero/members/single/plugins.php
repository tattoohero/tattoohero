<?php

/**
 * BuddyPress - Users Plugins
 *
 * This is a fallback file that external plugins can use if the template they
 * need is not installed in the current theme. Use the actions in this template
 * to output everything your plugin needs.
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>

<?php get_header(); ?>
<?php do_action( 'bp_before_member_plugin_template' ); ?>
<div class="container user">
	<div id="item-body">
	<?php locate_template( array( 'members/single/member-header-sub.php' ), true ); ?>
	<div class="item-list-tabs no-ajax grid_12" id="subnav" role="navigation">
		<ul>
			<?php if ( bp_is_my_profile() ) bp_get_options_nav(); ?>
		</ul>
	</div>

	<div class="grid_12" role="main">


		<h3><?php do_action( 'bp_template_title' ); ?></h3>

		<?php do_action( 'bp_template_content' ); ?>

		<?php do_action( 'bp_after_member_body' ); ?>

	</div>

	<?php do_action( 'bp_after_member_plugin_template' ); ?>
	</div><!-- #item-body -->
</div>

<?php get_footer(); ?>
