<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-default
 */
global $bp;
?>

<?php do_action( 'bp_before_member_header' ); ?>
	
	<div class="user-breadcrumb grid_12 alpha omega">
		<div class="user-avatar-sub">
			<a href="<?php bp_displayed_user_link(); ?>">
		
				<?php bp_displayed_user_avatar( 'type=thumb' ); ?>
		
			</a>
		</div><!-- .user-avatar-sub -->
		<h2>
			<a href="<?php bp_displayed_user_link(); ?>"><?php bp_displayed_user_fullname(); ?></a>
			<span class="current-subpage"> / <?=$bp->canonical_stack['component'];?></span>
		</h2>
		<div class="member-options-nav">
		<ul>
			<li class="btn-profilemenu">
				<span class="arrowfont">&#xe00b;</span>&nbsp;&nbsp;Profile Menu
				<ul class="dropdown">
					<?php bp_get_displayed_user_nav(); ?>
					<?php do_action( 'bp_member_options_nav' ); ?>
				</ul>
			</li>
		</ul>
		</div>
	</div><!--.user-breadcrumb-->

	<?php do_action( 'bp_before_member_header_meta' ); ?>

		<?php
		/***
		 * If you'd like to show specific profile fields here use:
		 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
		 */
		 do_action( 'bp_profile_header_meta' );

		 ?>

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>