<?php

/**
 * BuddyPress - Users Home
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

get_header( 'buddypress' ); ?>

	<div class="container user">

			<div id="item-body">

				<?php do_action( 'bp_before_member_body' );

				if ( bp_is_user_activity() || !bp_current_component() ) :
					do_action( 'bp_before_member_home_content' );
					?>
					<section class="user-header grid_12" role="complementary">
						<?php locate_template( array( 'members/single/member-header.php' ), true ); ?>
					</section>
					<div class="grid_4">
						<div class="update-status">
							<?php locate_template( array( 'members/single/activity-profile.php'  ), true ); ?>
						</div>
						<?php

							$about = xprofile_get_field_data( "About Me" , $curauth->ID );
							$interests = xprofile_get_field_data( "Interests" , $curauth->ID );
							$website = xprofile_get_field_data( "Website" , $curauth->ID );

							if($about || $interests || $website):
						?>
						<aside class="profile-summary">
		        	<h3 class="scnHeader">Profile</h3>
							<?php if($about): ?>
		            <p class="verticalSpacer"><?=$about;?></p>
	            <?php endif; ?>
	            <?php if($interests): ?>
		            <h4 class="verticalSpacer">Interests</h4>
		        		<p><?=$interests;?></p>
		        	<?php endif; ?>
		        	<?php if($website): ?>
		            <h4 class="verticalSpacer">Website</h4>
		            <a href="#"><?=$website;?></a>
		          <?php endif; ?>
		        </aside>
			      <?php endif; ?>
						<div class="friend-list">
							<?php locate_template( array( 'members/members-loop-profile.php'  ), true ); ?>
						</div>
					</div>
					<div class="grid_8">
						<?php /* <section class="profile-socialGallery">
		        	<nav class="filter-socialGallery">
		            	<ul class="clearfix">
		                	<li class="selected">Uploaded Images</li>
		                    <li>Following</li>
		                    <li>Followed Artists</li>
		                </ul>
		            </nav>
		            <div class="mask-slider verticalSpacer">
		            <ul class="slider clearfix">
		            	<li><a href="#"><img src="http://placehold.it/400x300" alt="Sexy Tattoo" /></a></li>
	                <li><a href="#"><img src="http://placehold.it/400x300" alt="Sexy Tattoo" /></a></li>
	                <li><a href="#"><img src="http://placehold.it/400x300" alt="Sexy Tattoo" /></a></li>
	                <li><a href="#"><img src="http://placehold.it/400x300" alt="Sexy Tattoo" /></a></li>
	                <li><a href="#"><img src="http://placehold.it/400x300" alt="Sexy Tattoo" /></a></li>
	                <li><a href="#"><img src="http://placehold.it/400x300" alt="Sexy Tattoo" /></a></li>
		            </ul>
		            <div class="mask-shadow"><div class="left-shadow"></div></div>
		            <div class="mask-shadow right"><div class="right-shadow"></div></div>
		            </div><!--mask-slider-->
		            <span class="pull_right"><a href="#">See More</a></span>
		        </section> */ ?>
		        <section class="activity">
		        	<?php locate_template( array( 'members/single/activity.php'  ), true ); ?>
		        </section>
		      </div>
					<?php

				elseif ( bp_is_user_blogs() ) :
					locate_template( array( 'members/single/blogs.php'     ), true );

				elseif ( bp_is_user_friends() ) :
					locate_template( array( 'members/single/friends.php'   ), true );

				elseif ( bp_is_user_groups() ) :
					locate_template( array( 'members/single/groups.php'    ), true );

				elseif ( bp_is_user_messages() ) :
					locate_template( array( 'members/single/messages.php'  ), true );

				elseif ( bp_is_user_profile() ) :
					locate_template( array( 'members/single/profile.php'   ), true );

				elseif ( bp_is_user_forums() ) :
					locate_template( array( 'members/single/forums.php'    ), true );

				elseif ( bp_is_user_settings() ) :
					locate_template( array( 'members/single/settings.php'  ), true );

				// If nothing sticks, load a generic template
				else :
					locate_template( array( 'members/single/plugins.php'   ), true );

				endif;

				do_action( 'bp_after_member_body' ); ?>

			</div><!-- #item-body -->

			<?php do_action( 'bp_after_member_home_content' ); ?>

	</div><!-- .container -->

<?php // get_sidebar( 'buddypress' ); ?>
<?php get_footer( 'buddypress' ); ?>
