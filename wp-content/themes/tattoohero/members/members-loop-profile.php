<?php

/**
 * BuddyPress - Members Loop
 *
 * Querystring is set via AJAX in _inc/ajax.php - bp_dtheme_object_filter()
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>

<?php do_action( 'bp_before_members_loop' ); ?>


<?php if ( bp_has_members( bp_ajax_querystring( 'members' ) ) ) : ?>

	<?php do_action( 'bp_before_directory_members_list' ); ?>
	<h3>Friends</h3>
	<ul id="members-list" class="item-list" role="main">

	<?php $i = 0; while ( bp_members() && !$stop ) : bp_the_member(); $i++ ?>

		<li class="th_list">
			<div class="item-avatar">
				<a href="<?php bp_member_permalink(); ?>"><?php bp_member_avatar(); ?></a>
			</div>
			<div class="item">
				<div class="item-title uppercase">
					<a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a>
				</div>
				<div class="item-info">
					<p class="userloc uppercase">
						<?php 
							$city = xprofile_get_field_data( "City" , bp_get_member_user_id() );
							$province = xprofile_get_field_data( "Province/State" , bp_get_member_user_id() );
						?>
						<?php if($city): ?>
							<span class="user-city"><?=$city;?><?php if($province) { echo trim(','); } ?> </span>
						<?php endif; ?>
						<?php if($province): ?>
							<span class="user-province"> <?=$province;?></span>
						<?php endif; ?>
					</p>
					</p>
					<p class="usertagline"><?php bp_activity_latest_update( bp_get_member_user_id() ); ?></p>
				</div><!-- .item-info -->

				<?php do_action( 'bp_directory_members_item' ); ?>

				<?php
				 /***
				  * If you want to show specific profile fields here you can,
				  * but it'll add an extra query for each member in the loop
				  * (only one regardless of the number of fields you show):
				  *
				  * bp_member_profile_data( 'field=the field name' );
				  */
				?>
			</div>
			<div class="clr"></div>
			<?php if(is_user_logged_in( )): ?>
				<div class="action">
					<?php do_action( 'bp_directory_members_actions' ); ?>
				</div>
			<?php endif; ?>

			<div class="clear"></div>
		</li>

	<?php 
	if($i > 4) { $stop = true; }
	endwhile; 
	?>

	</ul>
	<a href="<?=TH_HOMEPAGE;?>/members/<?php bp_displayed_user_username(); ?>/friends">See More</a>

	<?php do_action( 'bp_after_directory_members_list' ); ?>

	<?php bp_member_hidden_fields(); ?>

<?php else: ?>


<?php endif; ?>

<?php do_action( 'bp_after_members_loop' ); ?>
