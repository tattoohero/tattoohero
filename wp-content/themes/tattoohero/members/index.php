<?php

get_header( ); 
	$filter = $_GET['f'];
	if ($filter == ""){
		$filter = "Members";
	}?>

	<?php do_action( 'bp_before_directory_members_page' ); ?>

	<div class="subpage container members">

			
			<?php do_action( 'bp_before_directory_members_content' ); ?>

		<?php do_action( 'bp_before_directory_members' ); ?>
			<header>
			<h1 class="search-heading align-center">MEMBERS</h1>
			</header>
			<nav class="browse-nav">
				<ul>
					<li><a href="<?php print(home_url()."/members/?f=Artists"); ?>" class="<?php if($filter == "Artists") {print('active'); }?>">Artists</a></li>
					
					<li><a href="<?php print(home_url()."/members/?f=Apprentices"); ?>" class="<?php if($filter == "Apprentices") {print('active'); }?>">Apprentices</a></li>
					<li><a href="<?php print(home_url()."/members/?f=Collectors"); ?>" class="<?php if($filter == "Collectors") {print('active'); }?>">Collectors</a></li>
				</ul>
			</nav>
			<div class="search grid_6 push_3" role="search">
				<!-- <form action="" method="get" id="search-members-form">
					<label><input type="text" name="s" id="members_search" placeholder="Search Members..."></label>
					<input type="submit" id="members_search_submit" name="members_search_submit" value="Search" class="btn btn-primary">
				</form> -->
				<?php  bp_directory_members_search_form(); ?>
			</div>
		<?php 
		$pref_city_name = get_city_name(get_location_pref());
		if (isset($_REQUEST['s'])){
			$member_string = 'Search';
		} else if ($pref_city_name == ""){
			$member_string = '(All)';
		}else {
			$member_string = '(in '.get_city_name(get_location_pref()).')';
		}?>
		<h2 class="grid_12"><?php printf( __( $filter.' %s', 'buddypress' ), $member_string); ?></h2>
		<form action="" method="post" id="members-directory-form" class="dir-form">

			<?php do_action( 'bp_before_directory_members_content' ); ?>

<?php /* 			<div class="grid_3">
				<label for="members-order-by"><?php _e( 'Order By:', 'buddypress' ); ?></label>
				<select id="members-order-by">
					<option value="active"><?php _e( 'Last Online', 'buddypress' ); ?></option>
					<option value="newest"><?php _e( 'Newest Registered', 'buddypress' ); ?></option>

					<?php if ( bp_is_active( 'xprofile' ) ) : ?>

						<option value="alphabetical"><?php _e( 'Alphabetical', 'buddypress' ); ?></option>

					<?php endif; ?>
				</select>
			</div> */ ?>

			<div id="members-dir-list" class="members dir-list grid_12">

				<?php locate_template( array( 'members/members-loop.php' ), true ); ?>

			</div><!-- #members-dir-list -->

			<?php do_action( 'bp_directory_members_content' ); ?>

			<?php wp_nonce_field( 'directory_members', '_wpnonce-member-filter' ); ?>

			<?php do_action( 'bp_after_directory_members_content' ); ?>

		</form><!-- #members-directory-form -->

		<?php do_action( 'bp_after_directory_members' ); ?>

	</div>

	<?php do_action( 'bp_after_directory_members_page' ); ?>

<?php get_footer( ); ?>
