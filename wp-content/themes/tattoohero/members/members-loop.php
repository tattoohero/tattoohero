<?php

/**
 * BuddyPress - Members Loop
 *
 * Querystring is set via AJAX in _inc/ajax.php - bp_dtheme_object_filter()
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>
<?php do_action( 'bp_before_members_loop' ); ?>

<!-- <div style="padding:10px; border:1px solid #CCC; margin:1em 0em;">
<h1>This is Adam's Button test</h1>
<input type="submit" value="test" class="btn" />
<input type="submit" value="test" class="btn active" />
<input type="submit" value="test" class="btn btn-primary" />
<input type="submit" value="test" class="btn btn-primary active" />
<input type="submit" value="test" class="btn btn-secondary" />
<input type="submit" value="test" class="btn disabled" />
<input type="submit" value="test" class="btn btn-large" />
<input type="submit" value="test" class="btn btn-small" />
<input type="submit" value="test" class="btn btn-mini" />
<input type="submit" value="test" class="btn btn-block" />
<hr />
<button class="btn">test</button>
<button class="btn active">test</button>
<button class="btn btn-primary">test</button>
<button class="btn btn-primary active">test</button>
<button class="btn btn-secondary">test</button>
<button class="btn disabled">test</button>
<button class="btn btn-large">test</button>
<button class="btn btn-small">test</button>
<button class="btn btn-mini">test</button>
<button class="btn btn-block">test</button>
</div> --><!-- button test -->
<?php 
	if (!isset($_REQUEST['s'])){
		$filter = $_GET['f'];
		if ($filter == ""){
			$filter = "Members";
		}
		$location_pref = get_location_pref();
		if ($location_pref != ""){
			$profiles_text = get_profiles_in($location_pref,"city");
			if($profiles_text == ""){
				echo 'No '.$filter.' have signed up for your area yet.  If you would like us to research your area or if you have a recommendation please email us at <a href="mailto:hello@tattoohero.com?subject=A suggestion from one of Tattoo Hero\'s beloved users&Body=Please help me find an awesome Tattoo Artist in '.get_city_name($location_pref).'. I would also like to recommend (artist name/shop name) to you guys!">hello@tattoohero.com</a>.';
				//artist suggestion form
				echo "<h2>".$filter." that are close by </h2>";
				$profiles_text =  get_profiles_in(get_region_id($location_pref), "region");
				if($profiles_text == ""){
					$profiles_text = get_profiles_in(get_country_id($location_pref), "country");
				}
			}
		}
		switch ($filter){
			case "Artists":
				$profiles_text = get_profiles_filter($profiles_text, 9, "Tattoo Artist");
				if($profiles_text==""){
					$profiles_text = get_profiles_filter($profiles_text, 9, "Tattoo Artist");
				}
				break;
			case "Apprentices":
				$profiles_text = get_profiles_filter($profiles_text, 9, "Tattoo Artist (Apprentice)");
				if($profiles_text==""){
					$profiles_text = get_profiles_filter($profiles_text, 9, "Tattoo Artist (Apprentice)");
				}
				break;
			case "Collectors":
				$profiles_text = get_profiles_filter($profiles_text, 9, "Tattoo Collector");
				if($profiles_text==""){
					$profiles_text = get_profiles_filter($profiles_text, 9, "Tattoo Collector");
				}
				break;
		}		
	}

	 $current_action = bp_current_action();
	 if (bp_current_action() || isset($_REQUEST['s'])){
	 	$has_members_text = bp_ajax_querystring( 'members' );
	  } else {
	 	$has_members_text = 'include='.$profiles_text;
	  }
?>

<?php if ( bp_has_members( $has_members_text)) : ?>
<?php //if ( bp_has_members( bp_ajax_querystring( 'members' ) ) ) : ?>

	<div id="pag-top" class="pagination grid_12 alpha omega" style="margin-bottom: 0;">

		<div class="pag-count" id="member-dir-count-top">
			<?php if(is_user_logged_in( )): ?>
				<!-- <hgroup>
				<h1 class="headline" style="margin-bottom: 0;">YOUR FRIENDS</h1>
				<p class="headline"><?php bp_members_pagination_count(); ?></p>
				</hgroup> -->
			<?php endif; ?>

		</div>

		<div class="pagination-links" id="member-dir-pag-top">

			<?php bp_members_pagination_links(); ?>

		</div>

	</div>

	<?php do_action( 'bp_before_directory_members_list' ); ?>

	<ul id="members-list" class="item-list inner_container" role="main">

	<?php $i=0; while ( bp_members() ) : bp_the_member(); $i++; ?>

		<li class="col_1of3 th_list">
			<div class="item-avatar">
				<a href="<?php bp_member_permalink(); ?>"><?php bp_member_avatar(); ?></a>
			</div>

			<div class="item">
				<div class="item-title uppercase">
					<a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a>
				</div>
				<div class="item-info">
					<p class="userloc uppercase">
						<?php 
							$member_type = xprofile_get_field_data( "Member Type" , bp_get_member_user_id() );
							$city = xprofile_get_field_data( "City" , bp_get_member_user_id() );
							$province = xprofile_get_field_data( "Province/State" , bp_get_member_user_id() );
						?>
							<p class="user-type"><?=$member_type;?> </p>
						<?php if($city): ?>
							<span class="user-city"><?=$city;?><?php if($province) { echo trim(','); } ?> </span>
						<?php endif; ?>
						<?php if($province): ?>
							<span class="user-province"> <?=$province;?></span>
						<?php endif; ?>
					</p>
					<p class="usertagline"><?php bp_activity_latest_update( bp_get_member_user_id() ); ?></p>
				</div><!-- .item-info -->

				<?php do_action( 'bp_directory_members_item' ); ?>

				<?php
				 /***
				  * If you want to show specific profile fields here you can,
				  * but it'll add an extra query for each member in the loop
				  * (only one regardless of the number of fields you show):
				  *
				  * bp_member_profile_data( 'field=the field name' );
				  */
				?>
			</div> <!-- .item -->
			<div class="clr"></div>
			<div class="action">

				<?php do_action( 'bp_directory_members_actions' ); ?>

			</div>

			<div class="clear"></div>
		</li>
		<?php if($i % 3 == 0) { echo '<div class="clr"></div>'; } ?>

	<?php endwhile; ?>

	</ul>

	<?php do_action( 'bp_after_directory_members_list' ); ?>

	<?php bp_member_hidden_fields(); ?>

	<div id="pag-bottom" class="pagination">

		<div class="pagination-links" id="member-dir-pag-bottom">

			<?php bp_members_pagination_links(); ?>

		</div>

	</div>

<?php else: ?>

	<div id="message" class="info">
		<p><?php _e( "Sorry, no members were found.", 'buddypress' ); ?></p>
	</div>

<?php endif; ?>

<?php do_action( 'bp_after_members_loop' ); ?>
