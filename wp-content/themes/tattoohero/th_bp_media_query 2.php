

<?php
function th_gallery_album($albumID){
	$images = array();
	$images = th_get_media(array('posts_per_page'=>6, 'post_parent'=>$albumID));
	th_display_gallery($images);
	$page = get_url_var();
}

function th_header_gallery(){

	$images = array();
	$images = th_get_media(array('posts_per_page'=>3, 'author'=>bp_displayed_user_id()));
	th_display_head_gallery($images);
	$page = get_url_var();
}


function th_gallery_profile(){
	$images = array();
	if (bp_current_component() == "groups") {
		$images = th_get_media(array('posts_per_page'=>6, 'meta_value' => "-".bp_get_group_id()));
	} else {
		$images = th_get_media(array('posts_per_page'=>6, 'author'=>bp_displayed_user_id()));
	}
	//$images = th_get_media(array('posts_per_page'=>6, 'author'=>bp_displayed_user_id()));
	th_display_gallery($images);
	$page = get_url_var();
}

function th_gallery_featured(){
	$images = array();
	
	$order = $_GET['o'];

	$search = $_GET['q'];

	if ($order == 'popular' ){
		$images = th_get_media(array('posts_per_page'=>12, 'meta_key' => 'like_count', 'orderby'=>'meta_value_num'), false);
		$popActive = 'active';
	} else if ($order == 'most_viewed'){
		$images = th_get_media(array('posts_per_page'=>12, 'meta_key' => 'view_count', 'orderby'=>'meta_value_num'), false);
		$viewActive = 'active';
	} else if ($order == 'recent'){
		$images = th_get_media(array('posts_per_page'=>12), false);
		$browseActive = 'active';
	} else if (isset($search)) {
		$images = th_get_media(array('posts_per_page'=>12, 's'=>$search), false);
	} else {
		$images = th_get_media(array('orderby' => 'rand', 'posts_per_page'=>12), false);
	}
	
	
	?>
	<section class="browse grid_12">
		<header>
			<h2>Browse Tattoos</h2>
		</header>

		<nav class="browse-nav">
			<ul>
				<li><a href="<?php print(home_url()."/browse/?o=recent"); ?>" class="<?php print($browseActive)?>">Newest</a></li>
				
				<li><a href="<?php print(home_url()."/browse/?o=most_viewed"); ?>" class="<?php print($viewActive)?>">Most Viewed</a></li>
				<li><a href="<?php print(home_url()."/browse/?o=popular"); ?>" class="<?php print($popActive)?>">Popular</a></li>
			</ul>
		</nav>

		<div class="search grid_6 push_3">
			<div id="tattoo-search" class="tattoo-search" role="search">
				<form action="<?php print(home_url()."/browse/"); ?>" method="post" class="search">
		    		<input type="text" name="q" size="27"/>
		    		<button class="btn" type="submit">Search Tattoos</button>
				</form>
			</div>
		</div><!-- .search -->
		<div class="clr"></div>

		<?php if (isset($search)){?>
			<p>Search results for: "<?php print($search)?>" </p>
		<?php } ?>

		
			<?php th_display_gallery($images); ?>
			

	</section>
	<?php
}
add_action('th_browse_featured', 'th_gallery_featured');


function th_gallery_all(){
	

	$images = array();
	
	$order = $_GET['o'];

	$search = $_GET['q'];

	if ($order == 'popular' ){
		$images = th_get_media(array('posts_per_page'=>12, 'meta_key' => 'like_count', 'orderby'=>'meta_value_num'));
		$popActive = 'active';
	} else if ($order == 'most_viewed'){
		$images = th_get_media(array('posts_per_page'=>12, 'meta_key' => 'view_count', 'orderby'=>'meta_value_num'));
		$viewActive = 'active';
	} else if ($order == 'recent'){
		$images = th_get_media(array('posts_per_page'=>12));
		$browseActive = 'active';
	} else if (isset($search)) {
		$images = th_get_media(array('posts_per_page'=>12, 's'=>$search));
	} else {
		$images = th_get_media(array('orderby' => 'rand', 'posts_per_page'=>12));
	}
	
	
	?>
	<section class="browse grid_12">
		<header>
			<h2>Browse Tattoos</h2>
		</header>

		<nav class="browse-nav">
			<ul>
				<li><a href="<?php print(home_url()."/browse/?o=recent"); ?>" class="<?php print($browseActive)?>">Newest</a></li>
				
				<li><a href="<?php print(home_url()."/browse/?o=most_viewed"); ?>" class="<?php print($viewActive)?>">Most Viewed</a></li>
				<li><a href="<?php print(home_url()."/browse/?o=popular"); ?>" class="<?php print($popActive)?>">Popular</a></li>
			</ul>
		</nav>

		<div class="search grid_6 push_3">
			<div id="tattoo-search" class="tattoo-search" role="search">
				<form action="<?php print(home_url()."/browse/"); ?>" method="post" class="search">
		    		<input type="text" name="q" size="27"/>
		    		<button class="btn" type="submit">Search Tattoos</button>
				</form>
			</div>
		</div><!-- .search -->
		<div class="clr"></div>

		<?php if (isset($search)){?>
			<p>Search results for: "<?php print($search)?>" </p>
		<?php } ?>

		
			<?php th_display_gallery($images); ?>
			

	</section>
	<?php
}
add_action('th_browse', 'th_gallery_all');

function search_redirect(){
		if (isset($_POST['q'])){
		$url = "?q=".$_POST['q'];
		//$url = home_url();
		wp_redirect( home_url().'/browse?q='.$_POST['q']);
		//bp_core_redirect(get_option('siteurl')."/register");
		exit();
		
	}
}
add_action('bp_head','search_redirect');

function th_display_gallery($images){
	
	$i = 0;
	?>

	<div class="inner_container verticalSpacer">
			<ul class="browse-list">
	<?php
	foreach ($images as $image) :
		$k++;
	?>
		
			<?php $artist = get_user_by('id',$image['artist_id']); ?>
			<li class="col_1of3 media-<?=$image['post_id'];?>">
						<a href="<?=$image['permalink'];?>">
							<?= wp_get_attachment_image($image['post_id'],'thumbnail'); ?>
						</a>
						<div class="details">
							<div class="title"><a href="<?=$image['permalink'];?>"><?=$image['title']?></a></div>
							<div class="artist"><a href="<?=TH_HOMEPAGE;?>/members/<?=$artist->user_login;?>"><?=$artist->display_name;?></a></div>
							<div class="clr"></div>
							<div class="actionbar-image">
								<div class="view left"><?=($image['views'])? $image['views'] : 0;?> <span class="icon icon-eye"></span></div>
								<div class="likes left"><span class="like-count"><?=($image['likes'])? $image['likes'] : 0;?></span> <span class="icon icon-heart <?=($image['current_user_likes'])? 'liked' : '';?>"></span></div>
								<?php if ($image['current_user_likes'] != 'liked') { ?>
								<div class="right"><?php printf( '<a href="#" data-params="%s" data-class="media-'.$image['post_id'].'" data-url="'.TH_HOMEPAGE.'" class="set-like btn btn-mini" title="Like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$image['post_id'], 'bp_example_send_high_five' ) ) ?></div>
								<?php } ?>
								<div class="clr"></div>
							</div><!-- .actionbar-image -->
						</div>
			</li>
			<?php if(($k % 3) == 0) { echo '<div class="clr"></div>'; } endforeach;?>
			</ul>
			<div class="clr"></div>
			<div class="browse-pagination"><?php pagination('prev', 'next'); ?></div>
		</div><!-- .inner-container -->
	<?php
}

function th_display_head_gallery($images){
	
	$i = 0;
	?>
	<div class="head-gallery">
			<ul class="head-gal-list">
				
			<li class="gallery">
						<a href="<?=TH_HOMEPAGE;?>/?p=<?=$image['post_id'];?>">
							<?= wp_get_attachment_image($image['post_id'],array(120,67.5)); ?>
						</a>
			</li>
			</ul>

		</div>
	<?php
}


function browse_screen(){
	?>
	<section class="browse grid_12">
		<header>
			<h2>Browse Tattoos</h2>
		</header>

		<nav class="browse-nav">
			<ul>
				<li><a href="#" class="active">Most Viewed</a></li>
				<li><a href="#">Most Popular artist/shop</a></li>
				<li><a href="#">Recent Tattoos</a></li>
			</ul>
		</nav>
		<div class="inner_container">
			<ul class="browse-list">
	<?php
	$args = array(
		'owner_id' =>false,
		'id' => false,
		'post_type' => 'attachment',
		'post_status' => 'any',
		'post_mime_type' => false,
		'meta_query' => false,
		'posts_per_page' => -1
	);
	$media_query = new WP_Query($args);
	$i=0;
	while ($media_query->have_posts()) {
		$i++;
        $media_query->the_post();
       
        ?>
      		<li class="col_1of3 media-<?php the_ID();?>">
						<a href="<?=TH_HOMEPAGE;?>/?p=<?=the_ID();?>">
							<?= wp_get_attachment_image($media_query->post->ID,'thumbnail'); ?>
						</a>
						<div class="details">
							<div class="title"><a href="#"><?php echo(the_title())?></a></div>
							<div class="artist"><a href="#"><?php echo(the_title())?></a></div>
							<p><?php printf( '<a href="%s" title="Like">Like2</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$media_query->post->ID, 'bp_example_send_high_five' ) ) ?></p>
							<p><?php printf( '<a href="#" data-params="%s" data-url="'.TH_HOMEPAGE.'" class="set-like" title="Like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$media_query->post->ID, 'bp_example_send_high_five' ) ) ?></p>
							<div class="clr"></div>
							<div class="views">11,382 views</div>
						</div>
				</li>
		<?php if(($i % 3) == 0) { echo '<div class="clr"></div>'; }
		
        /*<p><?php echo(the_title().'/'.the_attachment_link().'/'.the_meta().'/'.wp_link) ?></p> */
    }
    ?>
    		</ul>
		</div>
	</section>
    <?php

	/*$args = array();

	$args['owner_id'] = false;	// Do *not* change this
	$args['id'] = false;		// Do *not* change this
	$args['page']=1;		// Do *not* change this
	$args['max']=false;		// Do *not* change this
	$args['privacy']='public';	// Do *not* change this
	$args['priv_override']=false;	// Do *not* change this
	$args['groupby']=false;	// Do *not* change this
	$args['post_type']='attachment';

	$args['per_page']=24;
	$args['ordersort']='DESC';
	$args['orderkey']='id';	// You can also use 'random' to shuffle images, but this will slow down your site
	$images_per_row = 5;*/
}

// add_action('th_browse','browse_screen');
/*
 * th_get_media
 */
function th_get_media($args = array(), $hidePages) {
	
	$hidePages = ( $hidePages === "undefined") ? false : $hidePages;
	
	global $media_query, $th_query_details;
	$current_user_likes_array = maybe_unserialize(get_user_meta(bp_loggedin_user_id(), 'image_like', true));
	$current_user_collects_array = maybe_unserialize(get_user_meta(bp_loggedin_user_id(), 'image_collect', true));
	$temp = array();
	$th_query_details = array();

	$x=0;
	$hidePages ? $defaults = array(

		'owner_id' =>false,
		'id' => false,
		'author'=>false,
		'paged' => get_page_no(),
		'posts_per_page' => -1,

		'post_type' => 'attachment',
		'post_status' => 'any',
		'post_mime_type' => false,
		'meta_key' => 'bp-media-key'
	) : $defaults = array(

		'owner_id' =>false,
		'id' => false,
		'author'=>false,
		'paged' => 4,
		'posts_per_page' => 3,
		'post_type' => 'attachment',
		'post_status' => 'any',
		'post_mime_type' => false,
		'meta_key' => 'bp-media-key'
	);
	
	
	$args = wp_parse_args($args, $defaults);
	$media_query = new WP_Query($args);
	$th_query_details['paged'] = $args['paged'];
	$th_query_details['max_num_pages'] = $media_query->max_num_pages;

	while ($media_query->have_posts()) {
        $media_query->the_post();

    	$artist_id = get_post_meta($media_query->post->ID,'artist',true); 
    	$likes = get_post_meta($media_query->post->ID, 'like_count', true);
    	$views = get_post_meta($media_query->post->ID, 'view_count', true);
    	$collects = get_post_meta($media_query->post->ID, 'collect_count', true);

  		$temp[$x]['post_id'] = $media_query->post->ID;
			$temp[$x]['src'] = $media_query->post->guid;
			$temp[$x]['title'] = $media_query->post->post_title;
			$temp[$x]['description'] = $media_query->post->post_content;
			$temp[$x]['author_id'] = $media_query->post->post_author;
			$temp[$x]['artist_id'] = $artist_id;
			$temp[$x]['likes'] = $likes;// user_meta likes
			$temp[$x]['views'] = $views; // user_meta views 
			$temp[$x]['collects'] = $collects; // user_meta collects 
			$temp[$x]['current_user_likes'] = in_array( $media_query->post->ID, (array)$current_user_likes_array );
			$temp[$x]['current_user_collects'] = in_array( $media_query->post->ID, (array)$current_user_collects_array );
			$temp[$x]['permalink'] = get_permalink();
		$x++;

	}
	
	//wp_reset_postdata();
		// foreach ($media_query->post as $key=>$value){
		// 	echo "$key, ";
		// }
// 	if($args['format'] == 'json') {
// 		return json_encode($temp);
// 	}
	return $temp;
}
function pagination($prev = '«', $next = '»') {
    global $media_query, $wp_rewrite, $th_query_details;
    $th_query_details['paged'] > 1 ? $current = $th_query_details['paged'] : $current = 1;
    $pagination = array(
        //'base' => $_SERVER['REQUEST_URI']."?"."page=%#%",
        'base' => add_query_arg( 'page', '%#%' ),
        'format' => '',
        'total' => $th_query_details['max_num_pages'],
        'current' => $current,
        'prev_text' => __($prev),
        'next_text' => __($next),
        'type' => 'plain'
	);
    if( $wp_rewrite->using_permalinks() )
        //$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

    if( !empty($th_query_details->query_vars['s']) )
        $pagination['add_args'] = array( 's' => get_query_var( 's' ) );
    //$pagination['base'] = $pagination['base']."?".get_url_variables();

    echo paginate_links( $pagination );
};
function get_page_no()  
{  
    $strURL = $_SERVER['REQUEST_URI'];  
    $arrVals = split("/",$strURL);  
    $page = split("\?", $arrVals[count($arrVals)-1]);
    if ($page[0] != '') {
    	return $page[0];
    } else {
    	return $_GET['page'];
    }
    //return $_GET['page'];

}  
function get_url_variables($part){
	$strURL = $_SERVER['REQUEST_URI'];  
    $strVals = split("\?",$strURL);
    return $strVals[1];
}

?>