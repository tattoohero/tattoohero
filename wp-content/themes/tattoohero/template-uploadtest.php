<?php /* TEMPLATE NAME: UPLOADTEST */ ?>

<?php



//function th_show_upload_form(){


function getShops(){
	//global $shopsArray;
	
	$shopsArray = array();
	
	global $bp;
	
	if(bp_has_groups(array('user_id' => false, 'type'=>'alphabetical', 'per_page'=>1000000))):
		$x = 0;
		while ( bp_groups() ) : 
			bp_the_group();
			if(bp_group_is_visible()){ //only shops that's not private/hidden
				$shopsArray[$x]['id'] 	= bp_get_group_id();
				$shopsArray[$x]['name'] = bp_get_group_name();
				//$shopsArray[$x]['location'] = xprofile_get_field_data('Location', );
				$x++;		
			}
		endwhile;
	endif;	

	//new shop option
	//array_unshift($shopsArray, array("id" => -1, "name" => 'New shop'));
	return $shopsArray;	
}

function shopSelection(){
	$shops = getShops();
	
	echo"<select name=\"shop\" class=\"selectShop\">";
	
	foreach($shops as $sh){
		echo "<option value=\"". $sh['id'] ."\">". $sh['name'] . "</option>";
	}	
	
	echo "</select>";
}

function shopselectfield(){
	global $shopString;
	$shop_list = getShops();
	
	foreach ($shop_list as $shop){
		$shopString = $shopString.'{value: "'.$shop['id'].'", label: "'.$shop['name'].'"},';//, location: ",';//'.$shop['location'].'"},';
	}
	$shopString = $shopString.'{}';
	//printf($shopString);		
	
	//store 
	?>
	
	<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />-->
	<!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
	<!--<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
	<!--<link rel="stylesheet" href="/resources/demos/style.css" />-->
	<style>
	#up-shop-label {
		display: block;
		font-weight: bold;
		margin-bottom: 1em;
	}
	</style>
	<script type="text/javascript">
		/*	store result as global javascript variable */
		var shops = [ <?php echo ($shopString); ?>];
	</script>
		
<?php 
}

function autocompleteshops($id){ 
  ?>
  <script>
  jQuery(function($) {
	$( "#up-shop<?php echo ($id); ?>" ).autocomplete({
		minLength: 0,
		source: shops,
		focus: function( event, ui ) {
		  $( "#up-shop<?php echo ($id); ?>" ).val( ui.item.label );
		  return false;
		},
		select: function( event, ui ) {
		  $( "#up-shop<?php echo ($id); ?>" ).val( ui.item.label );
		  $( "#up-shop-id<?php echo ($id); ?>" ).val( ui.item.value );
		  
		  return false;
		}
	})

    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label + "</a>")
        
        .appendTo( ul );
    };
  });
  </script>
  <?php 
}

function getArtists() {

	$memberArray = array();

	global $members_template;

	if (bp_has_members(array('user_id' => false, 'type'=>'alphabetical', 'per_page'=>1000000))) :
		$x=0;
		while (bp_members()) :

			bp_the_member();

			$theFieldValue = bp_get_member_profile_data( 'field=Member Type' );

			if ($theFieldValue=='Tattoo Artist' or $theFieldValue=='Tattoo Artist (Apprentice)') {
				$artistid = bp_get_member_user_id();
				
				$memberArray[$x]['id'] = $artistid;
				$memberArray[$x]['name'] = xprofile_get_field_data('Name', $artistid);
				$memberArray[$x]['location'] = th_get_user_location($artistid);
				//$memberArray[$x]['location'] = bp_get_member_profile_data( 'field=City' ).', '.bp_get_member_profile_data( 'field=Province').', '.bp_get_member_profile_data( 'field=Country' );
				$x++;
			}		
		endwhile;
	endif;

	//$theIncludeString=implode(",",$memberArray);

	//return $theIncludeString;
	return $memberArray;
}



function artistselectfield(){
	//global $ArtistString;
	$artist_list = getArtists();
	
	//print_r($artist_list);
	//$ArtistString='"'.implode('","',$artist_list).'"';
	
	foreach ($artist_list as $artist){
		$ArtistString = $ArtistString.'{value: "'.$artist['id'].'", label: "'.$artist['name'].'", desc: "'.$artist['location'].'"},';
	}
	
	$ArtistString = $ArtistString.'{}';

	//$artistString = 
	//printf($ArtistString);		
	
	//store 
	?>
	
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
	<!--<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
	<!--<link rel="stylesheet" href="/resources/demos/style.css" />-->
	<style>
	#up-artist-label {
		display: block;
		font-weight: bold;
		margin-bottom: 1em;
	}
	#up-artist-icon {
		float: left;
		height: 32px;
		width: 32px;
	}
	#up-artist-description {
		margin: 0;
		padding: 0;
	}
	</style>
	<script type="text/javascript">
		/*	store result as global javascript variable */
		var projects = [ <?php echo ($ArtistString); ?>];
	</script>
		
<?php 
}

function autocompleteartist($id){ 
  ?>
  <script>
  jQuery(function($) {
	$( "#up-artist<?php echo ($id); ?>" ).autocomplete({
		minLength: 0,
		source: projects,
		focus: function( event, ui ) {
		  $( "#up-artist<?php echo ($id); ?>" ).val( ui.item.label );
		  return false;
		},
		select: function( event, ui ) {
		  $( "#up-artist<?php echo ($id); ?>" ).val( ui.item.label );
		  $( "#up-artist-id<?php echo ($id); ?>" ).val( ui.item.value );
		  $( "#up-artist-description<?php echo ($id); ?>" ).html( ui.item.desc );
		  //$( "#project-icon" ).attr( "src", "images/" + ui.item.icon );
		
		  return false;
		}
	})

    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label + "<br>" + item.desc + "</a>")
        .appendTo( ul );
    };
  });
  </script>
  <?php 
}


function testuploadform(){
//get the list of albums for the logged in user
$album_list = array();
$results = new wp_query(
	array( 
		'author'=> bp_loggedin_user_id(),
		'posts_per_page' => -1,
		//'post_author' => 5,
		'post_type' =>'bp_media_album',
		'post_status' => 'any'
	));

$x = 0;
while ($results->have_posts()) {
    $results->the_post();
    $album_list[$x]['name'] = $results->post->post_title;
    $album_list[$x]['id'] = $results->post->ID;
	$x++;
}
wp_reset_postdata();
//query the list of artists


?>



<?php get_header(); ?>
<?php do_action( 'bp_before' ); ?>

<div class="subpage container">
	<?php do_action( 'template_notices' ) ?>
	<form method="post" enctype="multipart/form-data">
		<label for="up-album">Album:</label>
	    <select name="up-album">
	    	<?php 
			foreach ($album_list as $album){
				echo ('<option value="'.$album['id'].'">'.$album['name'].'</option>');
			}
			?>

		</select>
	    <label for="file">Filename:</label>
	    	<input type="file" name="th_myfile" id="th_myfile" required>
	    <label for="up-title">Title:</label>
	    	<input type="text" name="up-title" required>
	    <label for="up-artist">Artist:</label>
		<!-- <img id="project-icon" src="images/transparent_1x1.png" class="ui-state-default" alt="" /> -->
			<input id="up-artist" name="up-artist" required>
			<input type="hidden" id="up-artist-id" name="up-artist-id"/>
			<p id="up-artist-description"></p>
		<label for="up-description">Description:</label>
	    	<textarea rows="5" name="up-description"></textarea>

		<input type="submit" name="submit" value="Submit">
    <input type="submit" name="upload-another" value="Upload Next Image">
	</form>
	<img id="prevImage" style="display:none;" />
</div>
<?php get_footer(); ?>
<?php } ?>

