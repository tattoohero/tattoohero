<?php 

	function citiesArray($param){
		global $wpdb;
		$return_array = array();
		$cities = $wpdb->get_results("SELECT full_loc_text, geonameid FROM location_search where asciiname like '%".$param."%';");
		foreach ( $cities as $city ) {
			//array_push($return_array, '{ city: "'+ $city->full_loc_text + '", city_id: "'+ $city->geonameid + '"}');
			array_push($return_array, array($city->full_loc_text, $city->geonameid));
		}
		//$return_array2 = $wpdb->get_col("SELECT full_loc_text FROM region_search where region_name_ascii like '".$param."%';", 0);
		//$return_array3 = array_merge($return_array, $return_array2);
		return $return_array;
		//echo json_endcode($return_array);
		//print_r($return_array3);
	}
	function get_city_id($city_name,$region_name){
		global $wpdb;
		$cities = $wpdb->get_results("SELECT geonameid FROM location_search where asciiname like '%".$city_name."%' AND region_name_ascii like '%".$region_name."%' limit 0,1;");
		return $cities[0]->geonameid;
	}
	
	function get_region_id($city_id){
		global $wpdb;
		$region = $wpdb->get_results("SELECT admin1_code, country_code FROM location_search where geonameid = '".$city_id."' limit 0,1;");
		return $region[0]->country_code.".".$region[0]->admin1_code;
	}
	function get_country_id($city_id){
		global $wpdb;
		$country = $wpdb->get_results("SELECT country_code FROM location_search where geonameid = '".$city_id."' limit 0,1;");
		return $country[0]->country_code;
	}
	function get_city_name($city_id){
		global $wpdb;
		$cities = $wpdb->get_results("SELECT full_loc_text FROM location_search where geonameid = '".$city_id."';");
		return $cities[0]->full_loc_text;
	}
	function get_country_name($country_id){

	}
	function get_region_name($region_id){

	}
	//location type should be 'city', 'region' or 'country'
	function get_profiles_in($location_id,$location_type){
		global $wpdb;
		//returns the city codes for each type and id 
		// printf(substr($location_id,0,2));
		// printf(substr($location_id,3,2));
		$city_codes;
		$profile_ids = Array();
		switch ($location_type){
			case "city":
				$city_codes = $location_id;
				break;
			case "region":
				$city_codes = $wpdb->get_col("SELECT geonameid FROM location_search where admin1_code = '".substr($location_id,3,2)."' and country_code = '".substr($location_id,0,2)."';");
				break;
			case "country":
				$city_codes = $wpdb->get_col("SELECT geonameid FROM location_search where country_code = '".$location_id."';");
				break;
		}
		if($location_type == "city"){
			$profile_ids = $wpdb->get_col("SELECT user_id FROM th_bp_xprofile_data where field_id = 19 and value = '".$city_codes."';");
		}else{
			//foreach ($city_codes as $city_code) {
				$city_codes_str = implode(",", $city_codes);
				$profiles = Array();
				$profiles = $wpdb->get_col("SELECT user_id FROM th_bp_xprofile_data where field_id = 19 and value in (".$city_codes_str.");");
				$profile_ids = array_merge($profile_ids, $profiles);
			//}
		}
		if ( !empty( $profile_ids ) ){
			$profile_ids_str = implode(",", $profile_ids);
		}
		return $profile_ids_str;

	}
	function get_profiles_filter($profile_ids_str, $xprofile_id, $xprofile_value){
		global $wpdb;
		if ( $profile_ids_str != "" ){
			$profile_ids = $wpdb->get_col("SELECT user_id FROM th_bp_xprofile_data where field_id = ".$xprofile_id." and value = '".$xprofile_value."' and user_id in (".$profile_ids_str.");");
		} else {
			$profile_ids = $wpdb->get_col("SELECT user_id FROM th_bp_xprofile_data where field_id = ".$xprofile_id." and value = '".$xprofile_value."';");
		}
		if ( !empty( $profile_ids ) ){
			$profile_ids_str2 = implode(",", $profile_ids);
		}
		return $profile_ids_str2;
	}

	function update_profile_city_id(){
		echo $_POST['profile_location_id'];
	}
	function update_shop_city_id(){
		echo $_POST['shop_location_id'];
	}

	function get_location_pref(){
		//if(is_user_logged_in()){
		$city_code;
		if(is_user_logged_in()){
			//get preference from xprofile
			$city_code = get_user_meta(bp_loggedin_user_id(),'location_pref',true);
			if (!$city_code){
				$city_code = $_COOKIE['location_pref'];
			}
		}else if(isset($_COOKIE['location_pref'])){
			//get preference from cookie
			$city_code = $_COOKIE['location_pref'];
		}else{
			$city_code = "";

		}
		return $city_code;
	}
	function clear_location_pref(){
		if (isset($_POST['clear-loc-pref'])){
			if(is_user_logged_in()){
				//save to xprofile
				update_user_meta( bp_loggedin_user_id(), 'location_pref', '' );
			}
			setcookie('location_pref', '',0);
			$_COOKIE['location_pref'] = '';
		}
	}
	add_action('bp_head','clear_location_pref');


	function save_location_pref(){

		if(isset($_POST['th_location_id'])){

			if(is_user_logged_in()){
				//save to xprofile
				update_user_meta( bp_loggedin_user_id(), 'location_pref', $_POST['th_location_id'] );
			}
			//save to cookie
			setcookie('location_pref', $_POST['th_location_id'],0);
			$_COOKIE['location_pref'] = $_POST['th_location_id'];
		}

		//print_r($_POST);
		//print_r($_COOKIE);
	}
	add_action('init','save_location_pref');
	
	function location_pref_form(){
		    $city_id = get_location_pref();
		    $city_name = get_city_name($city_id);
		//$return_text = '<form name="location_pref" action="" method="post">';
		$return_text .= '<input type="text" name="th_location" id="th_location" placeholder = "';
		  if($city_name){
		  	$return_text .= $city_name;
		  }else{
		  	$return_text .= 'Choose a city...';
		  }
		  $return_text .= '" >';
		  //$return_text .= '<input type="hidden" name="th_location_id" id="th_location_id" value = "'.$city_id.'"></form>';
		echo $return_text;
	}
	//add_action('bp_head', 'location_pref_form');
?>