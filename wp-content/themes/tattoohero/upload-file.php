<?php







function uploadpagecheck(){
	global $pagename;
	if ($pagename == 'upload2' && !is_user_logged_in()){
		wp_redirect( home_url().'/login');
		exit();
	}
}
add_action('template_redirect','uploadpagecheck');


function uploadfile(){
	if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );

	$uploadedfile = $_FILES['th_myfile'];
	$upload_overrides = array( 'test_form' => false );
	$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
	if ( $movefile ) {
	    $wp_filetype = $movefile['type'];
	    $filename = $movefile['file'];
	    $wp_upload_dir = wp_upload_dir();
	    if ($_POST['up-title']<>''){
	    	$post_title = preg_replace('/\.[^.]+$/', '', $_POST['up-title']);
	    }else{
	    	$post_title = preg_replace('/\.[^.]+$/', '', basename($filename));

	    }
	    $attachment = array(
	        'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
	        'post_mime_type' => $wp_filetype,
	        //'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
	        'post_title' => $post_title,
	        'post_content' => preg_replace('/\.[^.]+$/', '', $_POST['up-description']),
	        'post_status' => 'inherit'
	    );
	    $attach_id = wp_insert_attachment( $attachment, $filename, $_POST['up-album']);

	    if(isset($_POST['up-artist-id'])){
	    	update_post_meta($attach_id, 'Artist', $_POST['up-artist-id']);
	    }else{
	    	update_post_meta($attach_id, 'Artist', $_POST['up-artist']);
	    }
	    update_post_meta($attach_id, 'bp-media-key', bp_loggedin_user_id());
	    //update the users counts
	 //    $counts = maybe_unserialize( get_user_meta( bp_loggedin_user_id(), 'bp_media_count', true ) );
		// $counts[0]['image'] += 1;
		// update_user_meta( bp_loggedin_user_id(), 'bp_media_count', $counts );

		if ( ! is_wp_error( $attachment_id ) ) {
			wp_update_attachment_metadata( $attachment_id, wp_generate_attachment_metadata( $attachment_id, $filename) );
		} else {
			unlink( $file );
			throw new Exception( __( 'Error creating attachment for the media file, please try again', BP_MEDIA_TXT_DOMAIN ) );
		}
		// $this->id = $attachment_id;
		// $this->name = $name;
		// $this->description = $description;
		// $this->type = $type;
		// $this->owner = get_current_user_id();
		// $this->album_id = $post_id;
		// $this->group_id = $group;
		//$this->set_permalinks();
		// if ( $group == 0 ) {
		// 	update_post_meta( $attachment_id, 'bp-media-key', get_current_user_id() );
		// } else {
		// 	update_post_meta( $attachment_id, 'bp-media-key', (-$group ) );
		// }
		//do_action( 'bp_media_after_add_media', $this, false, false, 0 );
		//return $attachment_id;
		//redirect to main picture page
	  //   $permalink = get_permalink( $attach_id );

	  //   if (array_key_exists('submit', $_POST)){
			// wp_redirect($permalink);
			// exit();
	 	// }
		// $class_name = apply_filters('bp_media_transcoder', 'BPMediaHostWordpress', 'image');
  //       $bp_media_entry = new $class_name();
  //       try {
  //           //$title = isset($_POST['bp_media_title']) ? ($_POST['bp_media_title'] != "") ? $_POST['bp_media_title'] : pathinfo($_FILES['bp_media_file']['name'], PATHINFO_FILENAME)  : pathinfo($_FILES['bp_media_file']['name'], PATHINFO_FILENAME);
  //          	if ($_POST['up-title']<>''){
	 //    		$title = preg_replace('/\.[^.]+$/', '', $_POST['up-title']);
	 //    	}else{
	 //    		$title = preg_replace('/\.[^.]+$/', '', basename($filename));
	 //    	}
  //           //$album_id = isset($_POST['bp_media_album_id']) ? intval($_POST['bp_media_album_id']) : 0;
  //           $album_id = isset($_POST['up_album']) ? intval($_POST['up_album']) : 0;
  //           //$is_multiple = isset($_POST['is_multiple_upload']) ? ($_POST['is_multiple_upload'] == 'true' ? true : false) : false;
  //           $is_multiple = false;
  //           $description = isset($_POST['up_description']) ? $_POST['up_description'] : '';
  //           //$group_id = isset($_POST['bp_media_group_id']) ? intval($_POST['bp_media_group_id']) : 0;
  //           $group_id = 0;
  //           $entry = $bp_media_entry->add_media($title, $description, $album_id, $group_id, $is_multiple);
  //           //if (!isset($bp->{BP_MEDIA_SLUG}->messages['updated'][0]))
  //               //$bp->{BP_MEDIA_SLUG}->messages['updated'][0] = __('Upload Successful', BP_MEDIA_TXT_DOMAIN);
  //       } catch (Exception $e) {
  //           //$bp->{BP_MEDIA_SLUG}->messages['error'][] = $e->getMessage();
  //       }
	}


}

function filesubmitcheck(){

	//print_r($_POST);
	if (array_key_exists('up-title', $_POST)){
		//uploadfile2();
		//bp_core_add_message('Submitted!','error');

		switch ($_FILES['th_myfile']['error']){
			case 4:
				bp_core_add_message("Please select a picture to upload!", 'error');
				break;
			case 2:
			case 1:
				bp_core_add_message("We're sorry, the image can not be larger then 2MB!", 'error');
				break;
			case 3:
			case 5:
			case 6:
			case 7:
				bp_core_add_message("We're sorry, an error occured uploading your file! Please try again.", 'error');
				break;
			default:
				if(strpos($_FILES['th_myfile']['type'],"image") === false){
					bp_core_add_message("We're sorry, only image files can be uploaded!", 'error');
					break;
				}else{
					uploadfile();
					//print_r($_POST);
				}
		}
		//printf('test:'.strpos($_FILES['th_myfile']['type'],"image") );
		//print_r($_POST);
		// if (isset($_POST['up-artist-id'])){
		// 	printf('id is set/');
		// }
		// if (isset($_POST['up-artist'])){
		// 	printf('name is set/');
		// }
		//print_r($_FILES);
	}
	
}
add_action('template_redirect','filesubmitcheck');

?>