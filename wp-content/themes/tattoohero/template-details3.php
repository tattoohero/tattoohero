<?php
/* TEMPLATE NAME: PROFILE DETAILS - PAGE 3*/
$tpl_name = 'details3';

 //check if user's logged in
//check if user's logged in
if (!is_user_logged_in()) wp_redirect( '/login' );
//check if form was submitted	
else $current_url = get_permalink($post->ID);

?>

<?php get_header( ); ?>
	<div class="subpage container container-article profile-details profile-details3">
		<div class="grid_12 article-banner">
        	<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/details_image02b.jpg" />
        	<div class="grid_4 push_6 pd_slogan">
        		<img class="big" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_slogan_01.png" />
        		<img class="sub" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_txt_03.png" />
        	</div>
		</div>
        <div class="container-article-content">
            <section class="subpage grid_10">
                <div class="progressBox">
	                <div id="info" class="completed col_1of3"><a href="/profiledetails" title="quick info"></a>
		                <div class="dot"></div>
		                <div class="line"></div>
	                </div>
	                <div id="share" class="completed col_1of3"><a href="/profiledetails2" title="share and upload"></a>
	                	<div class="dot"></div>
	                	<div class="line"></div>
	                </div>
	                <div id="start" class="active col_1of3"><a href="" title="start browsing"></a>
	                </div>
                </div>
                <?php do_action( 'template_notices' ); ?>
                <?php $TH->show_errors(); 
	                	$TH->checkProfileDetailsProgress();
                ?>
                
                <div class="pre">
	                <?php //loop to get content
					if (have_posts()) : while (have_posts()) : ?>
						<?php the_post();
							the_content(); 
						?>
					<?php endwhile; endif;?>              
				</div>
				<?php $TH->get_main_profile(); ?>
				<div class="social">
					<h3><?php _e('Invite your friends...', 'tattoohero') ?></h3>
					<p><?php _e("Share the next big thing with them, they'll appreciate it", 'tattoohero') ?></p>
					<p class="highlight"><?php _e("Get 5 friends to sign up and we'll send you a small free gift!", 'tattoohero') ?></p>
					<br/>
					<p><?php _e("Connect and share to your social networks", 'tattoohero') ?>:</p>
					<?php $TH->share("Tattoo Hero"); ?>
					<!--<p><?php _e("or insert their e-mail(s) below.", 'tattoohero') ?></p>-->
				</div>
				<!--<form id="emailsForm" method="POST" action="<?php echo $current_url; ?>">
			 		
			 		<div class="emails">
				 		<div class="col_1of2">
				 			<input id="name1" type="text" name="invite1[name]"  tabindex="1" placeholder="<?php _e('Name', 'tattoohero') ?>" />
				 			<input id="email1" type="text" name="invite1[email]" tabindex="2" placeholder="<?php _e('Email', 'tattoohero') ?>" />
				 		</div>
				 		<div id="invites" class="col_1of2">
				 			<i><?php _e("Send 5 invites get a free gift!","tattoohero"); ?></i><br/>
				 			<a href="#more" id="send_more"><?php _e("Send more","tattoohero"); ?></a>
				 		</div>
				 		<div class="col_1of2">
				 			<input id="name2" type="text" name="invite2[name]"  tabindex="3" placeholder="<?php _e('Name', 'tattoohero') ?>" />
				 			<input id="email2" type="text" name="invite2[email]" tabindex="4" placeholder="<?php _e('Email', 'tattoohero') ?>" />
				 		</div>
				 		<div class="col_1of2">
				 			<input id="name3" type="text" name="invite3[name]"  tabindex="5" placeholder="<?php _e('Name', 'tattoohero') ?>" />
				 			<input id="email3" type="text" name="invite3[email]" tabindex="6" placeholder="<?php _e('Email', 'tattoohero') ?>" />
				 		</div>
				 		<div class="col_1of2">
				 			<input id="name4" type="text" name="invite4[name]"  tabindex="7" placeholder="<?php _e('Name', 'tattoohero') ?>" />
				 			<input id="email4" type="text" name="invite4[email]" tabindex="8" placeholder="<?php _e('Email', 'tattoohero') ?>" />
				 		</div>
				 		<div class="col_1of2">
				 			<input id="name5" type="text" name="invite5[name]"  tabindex="9" placeholder="<?php _e('Name', 'tattoohero') ?>" />
				 			<input id="email5" type="text" name="invite5[email]" tabindex="10" placeholder="<?php _e('Email', 'tattoohero') ?>" />
				 		</div>
			 		</div>
			 		<div class="email_submit">
			 			<div class="checkbox_input">
							<input id="gift" type="checkbox" name="gift" value="" />
							<label for="gift"><h2><?php _e("Send me a gift!", "tattoohero") ?></h2></label>
						</div>
						<input type="hidden" name="profileDetails_action" value="step3"/>
						<input type="hidden" name="profileDetails_emails" value="step3"/>
	        			<input id="submit" class="btn btn-primary" type="submit" value="<?php _e('Send invite', 'tattoohero'); ?>"/>
			 		</div>
	            </form>-->
	            <form id="profileForm" method="POST" action="<?php echo $current_url; ?>">
		        	<div class="profileButtons">
		        		<a href="/profiledetails2" class="btn btn-primary btn-large">Prev</a>
		        		<input type="hidden" name="profileDetails_action" value="step3"/>
		        		<input id="details_submit" class="btn btn-primary btn-large col_3of4" type="submit" value="<?php _e('Let\'s go', 'tattoohero'); ?>"/>
	                </div>
	            </form>
            </section>
        </div><!--.pull-up -->
    </div>
<?php get_footer(); ?>