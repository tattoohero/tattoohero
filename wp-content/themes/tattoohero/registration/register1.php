<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9"> <![endif]-->
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<meta property="og:image" content="<?=TH_IMAGES;?>/fb_thumbnail.png" />
<meta property="og:title" content="Tattoo Hero" />
<meta property="og:description"content="Submit your favourite tattoo to be a part of our upcoming launch!"/>
<meta property="og:type" content="company"/>
<meta property="og:url" content="http://tattoohero.com"/>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php global $page, $paged, $TH; wp_title( '|', true, 'right' ); bloginfo('name'); $site_description = get_bloginfo('description', 'display'); if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description"; ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" href="<?=TH_TEMPLATE_DIR;?>/style.css" type="text/css" />
<link rel="stylesheet" href="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/flexslider/flexslider.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/modernizr.js"></script>
<script src="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/oauthpopup.js"></script>
<script src="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/app.js"></script>
<script src="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/flexslider/jquery.flexslider-min.js"></script>
<?php do_action( 'bp_head' ); ?>
<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>	
	<div class="subpage container">

		<?php do_action( 'bp_before_register_page' ); ?>

			<form action="" name="signup_form" id="signup_form" method="post" enctype="multipart/form-data">

			<?php /* if ( 'registration-disabled' == bp_get_current_signup_step() ) :
				do_action( 'template_notices' );
				do_action( 'bp_before_registration_disabled' );

				_e( 'User registration is currently not allowed.', 'buddypress' );
				do_action( 'bp_after_registration_disabled' );
			endif; // registration-disabled signup setp */ ?>

			<?php if ( 'request-details' == bp_get_current_signup_step() ) : ?>

				<div class="grid_6 push_3 register">
					<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/th_logo_tag_black.png" />
					<div class="right"><?php do_action( 'bp_before_account_details_fields' ); ?></div>
					<h1><?php _e( 'Sign up for Tattoo Hero', 'buddypress' ); ?></h1>			
					<?php do_action( 'template_notices' ); ?>
					<div class="inner_container">
						<div class="col_1of2">
							<label for="signup_username"><?php _e( 'Username', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
							<?php do_action( 'bp_signup_username_errors' ); ?>
							<input type="text" name="signup_username" id="signup_username" value="<?php bp_signup_username_value(); ?>" />
						</div>
						<div class="col_1of2">
							<label for="signup_email"><?php _e( 'Email Address', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
							<?php do_action( 'bp_signup_email_errors' ); ?>
							<input type="text" name="signup_email" id="signup_email" value="<?php bp_signup_email_value(); ?>" />
						</div>
						<div class="col_1of2">
							<label for="signup_password"><?php _e( 'Choose a Password', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
							<?php do_action( 'bp_signup_password_errors' ); ?>
							<input type="password" name="signup_password" id="signup_password" value="" />
						</div>
						<div class="col_1of2">
							<label for="signup_password_confirm"><?php _e( 'Confirm Password', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
							<?php do_action( 'bp_signup_password_confirm_errors' ); ?>
							<input type="password" name="signup_password_confirm" id="signup_password_confirm" value="" />
						</div>
						<div class="col_1of1">
								<?php if ( bp_is_active( 'xprofile' ) ) : if ( bp_has_profile( 'profile_group_id=1' ) ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

								<?php $stop=false; while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
								<?php if(!$stop): if ( 'textbox' == bp_get_the_profile_field_type() ) : ?>

									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
									<input type="text" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" value="<?php bp_the_profile_field_edit_value(); ?>" />

								<?php $stop = true;  endif; endif; endwhile; ?>
							<?php endwhile; endif; endif; ?>
						</div>
						<div class="col_1of1">
							<?php do_action( 'bp_before_registration_submit_buttons' ); ?>
							<button type="submit" name="signup_submit" id="signup_submit" class="btn btn-primary"><?php _e( 'Sign up!', 'buddypress' ); ?></button>
						</div>

						<?php do_action( 'bp_after_registration_submit_buttons' ); ?>

						<?php wp_nonce_field( 'bp_new_signup' ); ?>
					</div>

				</div><!-- #basic-details-section -->

				<?php do_action( 'bp_after_account_details_fields' ); ?>

			<?php endif; // request-details signup step ?>

			<?php if ( 'completed-confirmation' == bp_get_current_signup_step() ) : ?>

				<h2><?php _e( 'Sign Up Complete!', 'buddypress' ); ?></h2>

				<?php do_action( 'template_notices' ); ?>
				
				<?php do_action( 'bp_before_registration_confirmed' ); ?>

				<?php if ( bp_registration_needs_activation() ) : ?>
					<p><?php _e( 'You have successfully created your account! To begin using this site you will need to activate your account via the email we have just sent to your address.', 'buddypress' ); ?></p>
				<?php else : ?>
					<p><?php _e( 'You have successfully created your account! Please log in using the username and password you have just created.', 'buddypress' ); ?></p>
				<?php endif; ?>

				<?php do_action( 'bp_after_registration_confirmed' ); ?>

			<?php endif; // completed-confirmation signup step ?>

			<?php do_action( 'bp_custom_signup_steps' ); ?>

			</form>
		<?php do_action( 'bp_after_register_page' ); ?>

	</div><!-- .container -->
<?php wp_footer(); ?>
</body>
</html>