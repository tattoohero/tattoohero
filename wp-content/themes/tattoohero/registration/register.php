<?php get_header( 'tattoohero' ); ?>
	<div class="subpage container registerbox"><!-- begin #content -->
		<div id="slogan">
			<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/Get_Inked_Title.png" />
		</div>
		<div class="grid_5 push_7">
	
			<?php do_action( 'bp_before_register_page' ); ?>
	
			<div class="pre">
				<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/th_logo_tag_black2.gif" />
				<div class="socialLogin"><?php do_action( 'bp_before_account_details_fields' ); ?></div>
				<h2><?php _e( 'Or', 'tattoohero' ); ?></h2>
			</div>				
			<form action="" name="signup_form" id="signup_form" class="standard-form" method="post" enctype="multipart/form-data">

			<?php if ( 'registration-disabled' == bp_get_current_signup_step() ) : ?>
				<?php do_action( 'template_notices' ); ?>
				<?php do_action( 'bp_before_registration_disabled' ); ?>

					<p><?php _e( 'User registration is currently not allowed.', 'tattoohero' ); ?></p>

				<?php do_action( 'bp_after_registration_disabled' ); ?>
			<?php endif; // registration-disabled signup setp ?>

			<?php if ( 'request-details' == bp_get_current_signup_step() ) : ?>

				<!-- <h2><?php //_e( 'Create an Account', 'tattoohero' ); ?></h2> -->
				<!--<div class="grid_6 push_3 register">-->
				<?php do_action( 'template_notices' ); ?>

				<?php //echo "<p>"; 
					//_e( 'Registering for this site is easy, just fill in the fields below and we\'ll get a new account set up for you in no time.', 'tattoohero' );
					//echo "</p>";
				?>
				
				<div class="register-section active" id="basic-details-section">
					<?php /***** Basic Account Details ******/ ?>

					<!--<label for="signup_username"><?php _e( 'Username', 'tattoohero' ); ?> <?php _e( '*', 'tattoohero' ); ?></label>-->
					<?php do_action( 'bp_signup_username_errors' ); ?>
					<input type="text" name="signup_username" id="signup_username" placeholder="<?php _e( 'Username ', 'tattoohero' ); _e( '*', 'tattoohero' ); ?>" value="<?php bp_signup_username_value(); ?>" />

					<!--<label for="signup_email"><?php _e( 'Email Address', 'tattoohero' ); ?> <?php _e( '*', 'tattoohero' ); ?></label>-->
					<?php do_action( 'bp_signup_email_errors' ); ?>
					<input type="text" name="signup_email" id="signup_email" placeholder="<?php _e( 'Email ', 'tattoohero' ); _e( '*', 'tattoohero' ); ?>" value="<?php bp_signup_email_value(); ?>" />

					<!--<label for="signup_password"><?php _e( 'Choose a Password', 'tattoohero' ); ?> <?php _e( '*', 'tattoohero' ); ?></label>-->
					<?php do_action( 'bp_signup_password_errors' ); ?>
					<input type="password" name="signup_password" id="signup_password" placeholder="<?php _e( 'Password ', 'tattoohero' ); _e( '*', 'tattoohero' ); ?>" value="" />

					<!--<label for="signup_password_confirm"><?php _e( 'Confirm Password', 'tattoohero' ); ?> <?php _e( '*', 'tattoohero' ); ?></label>-->
					<?php do_action( 'bp_signup_password_confirm_errors' ); ?>
					<input type="password" name="signup_password_confirm" id="signup_password_confirm" placeholder="<?php _e( 'Confirm Password ', 'tattoohero' );  _e( '*', 'tattoohero' ); ?>" value="" />

				
				<?php do_action( 'bp_after_account_details_fields' ); ?>

				<?php /***** Extra Profile Details ******/ ?>

				<?php if ( bp_is_active( 'xprofile' ) ) : ?>

					<?php do_action( 'bp_before_signup_profile_fields' ); ?>

						<?php /* Use the profile field loop to render input fields for the 'base' profile field group */ ?>
						<?php if ( bp_is_active( 'xprofile' ) ) : if ( bp_has_profile( 'profile_group_id=1' ) ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

					<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>						
						<?php if(bp_get_the_profile_field_is_required()):?>
							<?php if ( 'textbox' == bp_get_the_profile_field_type() ) : ?>

								<!--<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></label>-->
								<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
								<input type="text" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" placeholder="<?php if(bp_get_the_profile_field_name() == "Name") echo "Full Name"; else bp_the_profile_field_name(); echo " "; if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?>" value="<?php bp_the_profile_field_edit_value(); ?>" />

							<?php endif; ?>

							<?php if ( 'textarea' == bp_get_the_profile_field_type() ) : ?>

								<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></label>
								<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
								<textarea rows="5" cols="40" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_edit_value(); ?></textarea>

							<?php endif; ?>

							<?php if ( 'selectbox' == bp_get_the_profile_field_type() ) : ?>

								<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></label>
								<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
								<select name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>">
									<?php bp_the_profile_field_options(); ?>
								</select>

							<?php endif; ?>

							<?php if ( 'multiselectbox' == bp_get_the_profile_field_type() ) : ?>

								<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></label>
								<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
								<select name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" multiple="multiple">
									<?php bp_the_profile_field_options(); ?>
								</select>

							<?php endif; ?>

							<?php if ( 'radio' == bp_get_the_profile_field_type() ) : ?>

								<div class="radio">
									<span class="label"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></span>

									<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
									<?php bp_the_profile_field_options(); ?>

									<?php if ( !bp_get_the_profile_field_is_required() ) : ?>
										<a class="clear-value" href="javascript:clear( '<?php bp_the_profile_field_input_name(); ?>' );"><?php _e( 'Clear', 'tattoohero' ); ?></a>
									<?php endif; ?>
								</div>

							<?php endif; ?>

							<?php if ( 'checkbox' == bp_get_the_profile_field_type() ) : ?>

								<div class="checkbox">
									<span class="label"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></span>

									<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>
									<?php bp_the_profile_field_options(); ?>
								</div>

							<?php endif; ?>

							<?php if ( 'datebox' == bp_get_the_profile_field_type() ) : ?>

								<div class="datebox">
									<label for="<?php bp_the_profile_field_input_name(); ?>_day"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '*', 'tattoohero' ); ?><?php endif; ?></label>
									<?php do_action( 'bp_' . bp_get_the_profile_field_input_name() . '_errors' ); ?>

									<select name="<?php bp_the_profile_field_input_name(); ?>_day" id="<?php bp_the_profile_field_input_name(); ?>_day">
										<?php bp_the_profile_field_options( 'type=day' ); ?>
									</select>

									<select name="<?php bp_the_profile_field_input_name(); ?>_month" id="<?php bp_the_profile_field_input_name(); ?>_month">
										<?php bp_the_profile_field_options( 'type=month' ); ?>
									</select>

									<select name="<?php bp_the_profile_field_input_name(); ?>_year" id="<?php bp_the_profile_field_input_name(); ?>_year">
										<?php bp_the_profile_field_options( 'type=year' ); ?>
									</select>
								</div>

							<?php endif; ?>
							
							<?php //if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
							<?php if ( false ) : ?>
								<p class="field-visibility-settings-toggle" id="field-visibility-settings-toggle-<?php bp_the_profile_field_id() ?>">
									<?php printf( __( 'This field can be seen by: <span class="current-visibility-level">%s</span>', 'tattoohero' ), bp_get_the_profile_field_visibility_level_label() ) ?> <a href="#" class="visibility-toggle-link">Change</a>
								</p>
								
								<div class="field-visibility-settings" id="field-visibility-settings-<?php bp_the_profile_field_id() ?>">
									<fieldset>
										<legend><?php _e( 'Who can see this field?', 'tattoohero' ) ?></legend>
										<?php bp_profile_visibility_radio_buttons() ?>
									</fieldset>
									<a class="field-visibility-settings-close" href="#"><?php _e( 'Close', 'tattoohero' ) ?></a>
								</div>
							<?php endif ?>

							<?php do_action( 'bp_custom_profile_edit_fields' ); ?>

							<?php if(bp_get_the_profile_field_description() != ""): //needs improvement ?> 
								<p class="description"><?php bp_the_profile_field_description(); ?></p>
							<?php endif; 
							endif;?>
						
					<?php endwhile; ?>

							<input type="hidden" name="signup_profile_field_ids" id="signup_profile_field_ids" value="<?php bp_the_profile_group_field_ids(); ?>" />

						<?php endwhile; endif; endif; ?>					
					<?php do_action( 'bp_after_signup_profile_fields' ); ?>
					
				<?php endif; ?>

				<?php if ( bp_get_blog_signup_allowed() ) : ?>

					<?php do_action( 'bp_before_blog_details_fields' ); ?>

					<?php /***** Blog Creation Details ******/ ?>

					<div class="register-section" id="blog-details-section">

						<h4><?php _e( 'Blog Details', 'tattoohero' ); ?></h4>

						<p><input type="checkbox" name="signup_with_blog" id="signup_with_blog" value="1"<?php if ( (int) bp_get_signup_with_blog_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'Yes, I\'d like to create a new site', 'tattoohero' ); ?></p>

						<div id="blog-details"<?php if ( (int) bp_get_signup_with_blog_value() ) : ?>class="show"<?php endif; ?>>

							<label for="signup_blog_url"><?php _e( 'Blog URL', 'tattoohero' ); ?> <?php _e( '*', 'tattoohero' ); ?></label>
							<?php do_action( 'bp_signup_blog_url_errors' ); ?>

							<?php if ( is_subdomain_install() ) : ?>
								http:// <input type="text" name="signup_blog_url" id="signup_blog_url" value="<?php bp_signup_blog_url_value(); ?>" /> .<?php bp_blogs_subdomain_base(); ?>
							<?php else : ?>
								<?php echo site_url(); ?>/ <input type="text" name="signup_blog_url" id="signup_blog_url" value="<?php bp_signup_blog_url_value(); ?>" />
							<?php endif; ?>

							<label for="signup_blog_title"><?php _e( 'Site Title', 'tattoohero' ); ?> <?php _e( '*', 'tattoohero' ); ?></label>
							<?php do_action( 'bp_signup_blog_title_errors' ); ?>
							<input type="text" name="signup_blog_title" id="signup_blog_title" value="<?php bp_signup_blog_title_value(); ?>" />

							<span class="label"><?php _e( 'I would like my site to appear in search engines, and in public listings around this network.', 'tattoohero' ); ?>:</span>
							<?php do_action( 'bp_signup_blog_privacy_errors' ); ?>

							<label><input type="radio" name="signup_blog_privacy" id="signup_blog_privacy_public" value="public"<?php if ( 'public' == bp_get_signup_blog_privacy_value() || !bp_get_signup_blog_privacy_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'Yes', 'tattoohero' ); ?></label>
							<label><input type="radio" name="signup_blog_privacy" id="signup_blog_privacy_private" value="private"<?php if ( 'private' == bp_get_signup_blog_privacy_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'No', 'tattoohero' ); ?></label>

						</div>

					</div><!-- #blog-details-section -->

					<?php do_action( 'bp_after_blog_details_fields' ); ?>

				<?php endif; ?>

				<?php do_action( 'bp_before_registration_submit_buttons' ); ?>
					<input type="submit" name="signup_submit" class="btn-primary active btn-large" id="signup_submit" value="<?php _e( 'Sign Up', 'tattoohero' ); ?>" />
				</div><!-- #profile-details-section -->
					
				<?php do_action( 'bp_after_registration_submit_buttons' ); ?>

				<?php wp_nonce_field( 'bp_new_signup' ); ?>

			<?php endif; // request-details signup step ?>

			<?php if ( 'completed-confirmation' == bp_get_current_signup_step() ) : ?>
				<h2><?php _e( 'Sign Up Complete!', 'tattoohero' ); ?></h2>

				<?php do_action( 'template_notices' ); ?>
				<?php do_action( 'bp_before_registration_confirmed' ); ?>

				<?php if ( bp_registration_needs_activation() ) : ?>
					<p><?php _e( 'You have successfully created your account! To begin using this site you will need to activate your account via the email we have just sent to your address.', 'tattoohero' ); ?></p>
				<?php else : ?>
					<p><?php _e( 'You have successfully created your account! Please log in using the username and password you have just created.', 'tattoohero' ); ?></p>
				<?php endif; ?>

				<?php do_action( 'bp_after_registration_confirmed' ); ?>

			<?php endif; // completed-confirmation signup step ?>

			<?php do_action( 'bp_custom_signup_steps' ); ?>

			</form>
	
			<?php do_action( 'bp_after_register_page' ); ?>
			
			<div class="notice">*<?php _e('By Registering you agree to our ','tattoohero'); ?><a href="#terms" class="termsLink"><?php _e('Terms and Conditions','tattoohero'); ?></a><?php _e(' and our ','tattoohero'); ?><a href="#privacy" class="privacyLink"><?php _e('Privacy Policy','tattoohero'); ?></a></div>
			</div><!-- .grid_5 -->
			<div class="grid_5 push_1" id="terms">
				<div class="content">
					<?php 
		        		$legalPageId=3108; // = Legal Terms 
						$post = get_page($legalPageId); 
						$content = apply_filters('the_content', $post->post_content); 
						
						echo '<h1>'.translate('Terms and Conditions','tattoohero').'</h1>'.$content; 
					?>
				</div>	 
				<a href="#" id="close" class="btn btn-primary"><?php _e('Got it!', 'tattoohero'); ?></a>
			</div>
			<div class="grid_5 push_1" id="privacy">
				<div class="content">
					<?php 
		        		$privacyPageId=5058; // = Legal Terms 
						$post = get_page($privacyPageId); 
						$content = apply_filters('the_content', $post->post_content); 

						echo '<h1>'.translate('Privacy Policy','tattoohero').$content.'</h1>'; 
					?>
				</div>	 
				<a href="#" id="close" class="btn btn-primary"><?php _e('Got it!', 'tattoohero'); ?></a>
			</div>
		</div><!-- #content grid_8 -->

	</div>
	<?php //get_sidebar( 'tattoohero' ); ?>
	<script type="text/javascript">
		jQuery(document).ready( function() {
			if ( jQuery('div#blog-details').length && !jQuery('div#blog-details').hasClass('show') )
				jQuery('div#blog-details').toggle();

			jQuery( 'input#signup_with_blog' ).click( function() {
				jQuery('div#blog-details').fadeOut().toggle();
			});
		});
	</script>
<?php get_footer( 'tattoohero' ); ?>