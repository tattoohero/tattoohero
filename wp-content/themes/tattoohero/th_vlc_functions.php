<?php
//records a view, a like, or a collect on an image
//vlc = view, like, collect/follow
//ips = image, profile, shop
function th_bp_record_vlc_old( $ips_type, $vlc_type, $ips_id, $user_id ) {
	global $bp;
	$results = array('success'=>false);

	//check_admin_referer( 'bp_example_send_high_five' );

	$existing_user_vlc = maybe_unserialize( get_user_meta( $user_id, $ips_type."_".$vlc_type, true ) );

	/* Check to see if the user has already high-fived. That's okay, but lets not
	 * store duplicate high-fives in the database. What's the point, right?
	 */
	if ( !in_array( $ips_id, (array)$existing_user_vlc ) ) {
		//update users vlc list
		$existing_user_vlc[] = (int)$ips_id;

		//Now wrap it up and fire it back to the database overlords.
		update_user_meta( $user_id, $ips_type."_".$vlc_type, serialize( $existing_user_vlc ) );

		//update the ips's vlc list
		switch ($ips_type) {
			case 'image':
				$existing_ips_vlc = maybe_unserialize( get_post_meta( $ips_id, $vlc_type, true ) );
				if ( !in_array( $user_id, (array)$existing_ips_vlc ) ) {
					$existing_ips_vlc[] = (int)$user_id;
					update_post_meta( $ips_id, $vlc_type, serialize( $existing_ips_vlc ) );
					update_post_meta( $ips_id, $vlc_type."_count", sizeof($existing_ips_vlc));
					$results['success'] = true;
					$results['count'] = sizeof($existing_ips_vlc);
					//bp_core_add_notification( $from_user_id, $to_user_id, $bp->example->slug, 'new_high_five' );
				}
				break;
			case 'profile':
				$existing_ips_vlc = maybe_unserialize( get_user_meta( $ips_id, $vlc_type, true ) );
				if ( !in_array( $user_id, (array)$existing_ips_vlc ) ) {
					$existing_ips_vlc[] = (int)$user_id;
					update_user_meta( $ips_id, $vlc_type, serialize( $existing_ips_vlc ) );
					update_user_meta( $ips_id, $vlc_type."_count", sizeof($existing_ips_vlc));
				}
				break;
			case 'shop':
				$existing_ips_vlc = maybe_unserialize( groups_get_groupmeta( $ips_id, $vlc_type) );
				if ( !in_array( $user_id, (array)$existing_ips_vlc ) ) {
					$existing_ips_vlc[] = (int)$user_id;
					groups_update_groupmeta( $ips_id, $vlc_type, serialize( $existing_ips_vlc ) );
					groups_update_groupmeta( $ips_id, $vlc_type."_count", sizeof($existing_ips_vlc));
				}
				break;
			default:
				//bp_core_add_message(  echo ( 'The thing you are trying to "like" is not defined!'), 'error' );
				break;
		}
		//th_bp_update_vlc_count($ips_type,$ips_id, $vlc_type)
	}

	/***
	 * Now we've registered the new high-five, lets work on some notification and activity
	 * stream magic.
	 */

	/***
	 * Post a screen notification to the user's notifications menu.
	 * Remember, like activity streams we need to tell the activity stream component how to format
	 * this notification in bp_example_format_notifications() using the 'new_high_five' action.
	 */
	//bp_core_add_notification( $from_user_id, $to_user_id, $bp->example->slug, 'new_high_five' );

	/* Now record the new 'new_high_five' activity item */
	//$to_user_link = bp_core_get_userlink( $to_user_id );
	//$from_user_link = bp_core_get_userlink( $from_user_id );

	/*bp_example_record_activity( array(
		'type' => 'rejected_terms',
		'action' => apply_filters( 'bp_example_new_high_five_activity_action', sprintf( __( '%s high-fived %s!', 'bp-example' ), $from_user_link, $to_user_link ), $from_user_link, $to_user_link ),
		'item_id' => $to_user_id,
	) );*/

	/* We'll use this do_action call to send the email notification. See bp-example-notifications.php */
	//do_action( 'bp_example_send_high_five', $to_user_id, $from_user_id );

	return json_encode($results);
}

//vlc = view, like, collect/follow
//ips = image, profile, shop
function th_bp_record_vlc( $ips_type, $vlc_type, $ips_id, $user_id ) {

	global $bp;
	$newisp = false;
	$results = array('success'=>false);
	$ip = $user_id;
	$isip = filter_var($ip, FILTER_VALIDATE_IP);

	//echo ($user_id."/".$ips_id."/".$vlc_type."/".$ips_type);
	
	if(!$isip){
	//get the list of ids that the user has viewed/liked/
		$existing_user_vlc = maybe_unserialize( get_user_meta( $user_id, $ips_type."_".$vlc_type, true ) );

		if ( !in_array( $ips_id, (array)$existing_user_vlc ) ) {
			//update users vlc list
			$existing_user_vlc[] = (int)$ips_id;

			//Now wrap it up and fire it back to the database overlords.
			update_user_meta( $user_id, $ips_type."_".$vlc_type, serialize( $existing_user_vlc ) );

			$newisp = true;

		}
	}

	if($newisp or $isip){
	//update the ips's vlc list
		switch ($ips_type) {
			case 'image':
				$existing_ips_vlc = maybe_unserialize( get_post_meta( $ips_id, $vlc_type, true ) );
				if ( !in_array( $user_id, (array)$existing_ips_vlc ) ) {
					$existing_ips_vlc[] = $user_id;
					update_post_meta( $ips_id, $vlc_type, serialize( $existing_ips_vlc ) );
					update_post_meta( $ips_id, $vlc_type."_count", sizeof($existing_ips_vlc));
					$results['success'] = true;
					$results['count'] = sizeof($existing_ips_vlc);
					//bp_core_add_notification( $from_user_id, $to_user_id, $bp->example->slug, 'new_high_five' );
				}
				break;
			case 'profile':
				$existing_ips_vlc = maybe_unserialize( get_user_meta( $ips_id, $vlc_type, true ) );
				if ( !in_array( $user_id, (array)$existing_ips_vlc ) ) {
					$existing_ips_vlc[] = $user_id;
					update_user_meta( $ips_id, $vlc_type, serialize( $existing_ips_vlc ) );
					update_user_meta( $ips_id, $vlc_type."_count", sizeof($existing_ips_vlc));
				}
				break;
			case 'shop':
				$existing_ips_vlc = maybe_unserialize( groups_get_groupmeta( $ips_id, $vlc_type) );
				if ( !in_array( $user_id, (array)$existing_ips_vlc ) ) {
					$existing_ips_vlc[] = $user_id;
					groups_update_groupmeta( $ips_id, $vlc_type, serialize( $existing_ips_vlc ) );
					groups_update_groupmeta( $ips_id, $vlc_type."_count", sizeof($existing_ips_vlc));
				}
				break;
			default:
				//bp_core_add_message(  echo ( 'The thing you are trying to "like" is not defined!'), 'error' );
				break;
		}
		//th_bp_update_vlc_count($ips_type,$ips_id, $vlc_type)
	}
	
	/***
	 * Now we've registered the new high-five, lets work on some notification and activity
	 * stream magic.
	 */

	/***
	 * Post a screen notification to the user's notifications menu.
	 * Remember, like activity streams we need to tell the activity stream component how to format
	 * this notification in bp_example_format_notifications() using the 'new_high_five' action.
	 */
	//bp_core_add_notification( $from_user_id, $to_user_id, $bp->example->slug, 'new_high_five' );

	/* Now record the new 'new_high_five' activity item */
	//$to_user_link = bp_core_get_userlink( $to_user_id );
	//$from_user_link = bp_core_get_userlink( $from_user_id );

	/*bp_example_record_activity( array(
		'type' => 'rejected_terms',
		'action' => apply_filters( 'bp_example_new_high_five_activity_action', sprintf( __( '%s high-fived %s!', 'bp-example' ), $from_user_link, $to_user_link ), $from_user_link, $to_user_link ),
		'item_id' => $to_user_id,
	) );*/

	/* We'll use this do_action call to send the email notification. See bp-example-notifications.php */
	//do_action( 'bp_example_send_high_five', $to_user_id, $from_user_id );

	return json_encode($results);
}

// function test_vlc(){
// 	print_r(th_bp_record_vlc_rev( 'image', 'like', '1', getip() ));

// }
// add_action('bp_head', 'test_vlc');

function th_bp_vlc_url_save(){
	if ($_GET['vlc_type']=='like'||$_GET['vlc_type']=='collect'){
		check_admin_referer( 'bp_example_send_high_five' );
		$vlc_count = th_bp_record_vlc( $_GET['ips_type'], $_GET['vlc_type'], $_GET['ips_id'], bp_loggedin_user_id() );
	}
}
add_action( 'bp_head', 'th_bp_vlc_url_save' );

function th_bp_vlc_save($r) {
	// print_r($_GET);
	if ($r['vlc_type']=='like'){
		check_admin_referer( 'bp_example_send_high_five' );
		$vlc_count = th_bp_record_vlc( $r['ips_type'], $r['vlc_type'], $r['ips_id'], bp_loggedin_user_id() );
		// if($vlc_count > -1) {
		// 	$results['success'] = true;
		// 	$results['message'] = 'You just liked this.';
		// 	$results['count'] = $vlc_count; //current_count
		// 	$results['type'] = $r['type'];
		// }
		//return json_encode(true);		
		//header('location:'.$_SERVER['HTTP_REFERER']);
	}
	
	/*if ( bp_is_example_component() && bp_is_current_action( 'screen-one' ) && bp_is_action_variable( 'th-like', 0 ) ) {
		// The logged in user has clicked on the 'send high five' link

		if ( bp_is_my_profile() ) {
			// Don't let users high five themselves
			bp_core_add_message( __( 'Cant like yourself! :)', 'bp-example' ), 'error' );
		} else {
			//if ( bp_example_send_highfive( bp_displayed_user_id(), bp_loggedin_user_id() ) )
			if ( True)
				bp_core_add_message( __( 'Like sent!', 'bp-example' ) );
			else
				bp_core_add_message( __( 'High-five could not be sent.', 'bp-example' ), 'error' );
		}

		bp_core_redirect( bp_displayed_user_domain() . bp_get_example_slug() . '/screen-one' );
	}*/
	
}


function bp_th_get_views_for_user( $user_id ) {
	global $bp;

	if ( !$user_id ) {
		return false;
	}

	return get_user_meta( $user_id, 'view_count', true );
}

function bp_th_record_a_view() {
	if ( bp_is_current_component('activity') && bp_is_current_action('just-me') ){
		if (is_user_logged_in()){
			//printf('user is logged in');
			if ( !bp_is_my_profile() ) {
				th_bp_record_vlc( 'profile', 'view', bp_displayed_user_id(), bp_loggedin_user_id());
			}
		} else {
			//printf('user is not logged in');
			th_bp_record_vlc( 'profile', 'view', bp_displayed_user_id(), getip());
		}
		
	} else if (bp_is_current_component('groups') && bp_is_current_action('home') ){
		if (is_user_logged_in()){
			th_bp_record_vlc( 'shop', 'view', bp_get_current_group_id(), bp_loggedin_user_id());
		}else{
			th_bp_record_vlc( 'shop', 'view', bp_get_current_group_id(), getip());
		}
	}
	//views for images are recorded each time the single.php page is loaded

}
add_action( 'bp_actions', 'bp_th_record_a_view' );

function vlc_view($type){
	global $post;
	if ($type == 'image'){
	?>
	<div class="vlc">
        <div class="views uppercase"><?php echo get_post_meta($post->ID,'like_count',true)." Likes"?></div>
        <div class="likes uppercase"><?php echo get_post_meta($post->ID,'view_count',true)." Views" ?></div>
        <div class="collects uppercase"><?php //echo get_post_meta($post->ID,'collect_count',true)." Collects" ?></div>
    </div>
    <?php 
	} elseif ($type == 'profile'){
	?>
	<div class="vlc">
        <div class="views uppercase"><?php echo get_user_meta(bp_displayed_user_id(),'like_count',true)." Likes"?></div>
        <div class="likes uppercase"><?php echo get_user_meta(bp_displayed_user_id(),'view_count',true)." Views" ?></div>
        <div class="collects uppercase"><?php //echo get_user_meta(bp_displayed_user_id(),'collect_count',true)." Collects" ?></div>
    </div>
    <?php
	} elseif ($type == 'shop') {
	?>
	<div class="vlc">
        <div class="views uppercase"><?php echo groups_get_groupmeta(bp_get_group_id(),'like_count',true)." Likes"?></div>
        <div class="likes uppercase"><?php echo groups_get_groupmeta(bp_get_group_id(),'view_count',true)." Views" ?></div>
        <div class="collects uppercase"><?php //echo groups_get_groupmeta(bp_get_group_id(),'collect_count',true)." Collects" ?></div>
   
    </div>
    <?php
	}
}
function vlc_buttons($type){
	global $post;
	if ($type == 'image'){
	?>
	<div class="vlc">
        <?php printf('<a href="%s" class="btn btn-primary btn-like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$post->ID, 'bp_example_send_high_five' ) )?>
        <?php //printf('<a href="%s" class="btn btn-primary btn-collect">Collect</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=collect&ips_type=image&ips_id='.$post->ID, 'bp_example_send_high_five' ) )?>
    </div>
    <?php 
	} elseif ($type == 'profile'){
	?>
	<div class="vlc">
       <?php if (bp_displayed_user_id() != bp_loggedin_user_id()){?>
        	<?php printf('<a href="%s" class="btn btn-primary btn-like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=profile&ips_id='.bp_displayed_user_id(), 'bp_example_send_high_five' ) )?>
        	<?php //printf('<a href="%s" class="btn btn-primary btn-collect">Collect</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=collect&ips_type=profile&ips_id='.bp_displayed_user_id(), 'bp_example_send_high_five' ) )?>
    	<?php } ?>
    </div>
    <?php
	} elseif ($type == 'shop') {
	?>
	<div class="vlc">
       
        <?php printf('<a href="%s" class="btn btn-primary btn-like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=shop&ips_id='.bp_get_group_id(), 'bp_example_send_high_five' ) )?>
        <?php //printf('<a href="%s" class="btn btn-primary btn-collect">Collect</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=collect&ips_type=shop&ips_id='.bp_get_group_id(), 'bp_example_send_high_five' ) )?>
    	
    </div>
    <?php
	}
}

function getip() {
	if (isSet($_SERVER)) {
		if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif (isSet($_SERVER["HTTP_CLIENT_IP"])) {
			$realip = $_SERVER["HTTP_CLIENT_IP"];
		} else {
			$realip = $_SERVER["REMOTE_ADDR"];
		}
	} else {
		if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
			$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
		} elseif ( getenv( 'HTTP_CLIENT_IP' ) ) {
			$realip = getenv( 'HTTP_CLIENT_IP' );
		} else {
			$realip = getenv( 'REMOTE_ADDR' );
		}
	}
	return $realip;
}
?>