<?php
/* TEMPLATE NAME: PROFILE DETAILS - PAGE 1.5 */ 
$tpl_name = 'details1-5';

//session_start();

//check if user's logged in
if (!is_user_logged_in()) wp_redirect( '/login' );
else if(!th_isArtist(bp_loggedin_user_id())) wp_redirect('/profiledetails2');
else $current_url = get_permalink($post->ID);
?>

<?php get_header(); ?>
	<div class="subpage container container-article profile-details">
		<div class="grid_12 article-banner">
        	<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/details_image01.jpg" />
        	<div class="grid_4 push_6 pd_slogan">
        		<img class="big" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_slogan_01.png" />
        		<img class="sub" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_txt_01.png" />
        	</div>
		</div>
        <div class="container-article-content">
            <section class="subpage grid_10">
                <div class="progressBox">
	                <div id="info" class="active col_1of3"><a href="/profiledetails" title="quick info"></a>
		                <div class="dot"></div>
		                <div class="line"></div>
	                </div>
	                <div id="share" class="col_1of3"><a href="/profiledetails2" title="share and upload"></a>
	                	<div class="dot"></div>
	                	<div class="line"></div>
	                </div>
	                <div id="start" class="col_1of3"><a href="" title="start browsing"></a>
	                </div>
                </div>
                <?php do_action( 'template_notices' ); ?>
                <?php $TH->show_errors(); 
	                $TH->checkProfileDetailsProgress();
                ?>
                	
                <div class="pre">
	                <?php //loop to get content
					if (have_posts()) : while (have_posts()) : ?>
					<?php the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif;?> 
				</div>
                <form id="shopForm" method="POST" action="<?php echo $current_url; ?>">
                	<div id="fields">
                		<div id="items">
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group-name"><?php _e('Name (required)', 'tattoohero') ?></label>
			                		<input id="name" type="text" name="group-name" tabindex="1" placeholder="<?php _e('Shop Name', 'tattoohero') ?>" value="<?php echo $_SESSION['shopName']; ?>" required />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('The shop\'s name.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group-desc"><?php _e('Description (required)', 'tattoohero') ?></label>
			                		<input id="desc" type="textarea" name="group-desc" tabindex="2" placeholder="<?php _e('Shop Description', 'tattoohero') ?>"  value="<?php if(isset($_POST['group-desc'])) echo $_POST['group-desc']; ?>" required />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('What kind of a shop are you? Walk-in shop, Custom shop, etc. Tattoo Styles, shop motto, atmosphere.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="street"><?php _e('Street & Number', 'tattoohero') ?></label>
			                		<input id="street" type="text" name="group_plus_map_field" tabindex="3" placeholder="<?php _e('666 Johnson Ave.', 'tattoohero') ?>"  value="<?php if(isset($_POST['group_plus_map_field'])) echo $_POST['group_plus_map_field']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('Street and Number.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group-city"><?php _e('City', 'tattoohero') ?></label>
			                		<input id="city" type="text" name="group-city" tabindex="4" placeholder="<?php _e('City', 'tattoohero') ?>" value="<?php if(isset($_POST['group-city'])) echo $_POST['group-city']; ?>" />
			                		<input id="city-id" type="hidden" name="group-city-id" value="<?php if(isset($_POST['group-city-id'])) echo $_POST['group-city-id']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('The city where the shop\'s located.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textfield_field"><?php _e('Website', 'tattoohero') ?></label>
			                		<input id="web" type="text" name="group_plus_textfield_field" tabindex="4" placeholder="<?php _e('Website address', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textfield_field'])) echo $_POST['group_plus_textfield_field']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('Got a website you\'d like to associate with your shop?', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textfield_field2"><?php _e('Email', 'tattoohero') ?></label>
			                		<input id="email" type="text" name="group_plus_textfield_field2" tabindex="5" placeholder="<?php _e('Email', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textfield_field2'])) echo $_POST['65lko67']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('Is there an email the shop can be reached at?', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textarea_field"><?php _e('Phone', 'tattoohero') ?></label>
			                		<input id="phone" type="text" name="group_plus_textarea_field" tabindex="6" placeholder="<?php _e('Telehone number, i.e. (123) 456-7890', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textarea_field'])) echo $_POST['group_plus_textarea_field']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('How can the shop be reached via phone, i.e. (123) 456-7890', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textarea_field2"><?php _e('Hours of Operation', 'tattoohero') ?></label>
			                		<textarea id="hours" rows="6" cols="40" name="group_plus_textarea_field2" tabindex="7" placeholder="<?php _e('Mon - Sun: 10:00AM - 8:00PM', 'tattoohero') ?>"><?php if(isset($_POST['group_plus_textarea_field2'])) echo $_POST['group_plus_textarea_field2']; ?></textarea>
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('The opening hours, i.e. Mon: 10:00 - 21:00, ... or Mon: 10:00AM - 9:00PM', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textfield_field3"><?php _e('Facebook', 'tattoohero') ?></label>
			                		<input id="phone" type="text" name="group_plus_textfield_field3" tabindex="8" placeholder="<?php _e('Facebook', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textfield_field3'])) echo $_POST['group_plus_textfield_field3']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('The shop\'s facebook page.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textfield_field4"><?php _e('Twitter', 'tattoohero') ?></label>
			                		<input id="phone" type="text" name="group_plus_textfield_field4" tabindex="9" placeholder="<?php _e('Twitter', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textfield_field4'])) echo $_POST['group_plus_textfield_field4']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('The shop\'s twitter page.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textfield_field5"><?php _e('Instagram', 'tattoohero') ?></label>
			                		<input id="phone" type="text" name="group_plus_textfield_field5" tabindex="10" placeholder="<?php _e('Instagram', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textfield_field5'])) echo $_POST['group_plus_textfield_field5']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('The shop\'s instagram page.', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                		<div class="editfield">
		                		<div class="field_content col_1of2">
		                			<label for="group_plus_textarea_field3"><?php _e('Artists', 'tattoohero') ?></label>
			                		<input id="phone" type="text" name="group_plus_textarea_field3" tabindex="11" placeholder="<?php _e('Artists', 'tattoohero') ?>" value="<?php if(isset($_POST['group_plus_textarea_field3'])) echo $_POST['group_plus_textarea_field3']; ?>" />
		                		</div>
		                		<div class="description col_1of2">
			                		<?php _e('Artists in the shop, i.e. John James, Mike Myers', 'tattoohero') ?>	
		                		</div>
	                		</div>
	                	</div>
                	</div>
	                <div class="profileButtons">
	                	<a href="/profiledetails" class="btn btn-primary btn-large"  tabindex="13">Prev</a>
	                	<input type="hidden" name="profileDetails_action" value="step1.5"/>
		                <input type="hidden" name="userID" value="<?php echo bp_loggedin_user_id(); ?>"/>
		                <input type="hidden" name="invite_status" value="mods"/>
						<input id="details_submit" class="btn btn-primary btn-large col_3of4" tabindex="12" type="submit" value="<?php _e('Next', 'tattoohero'); ?>"/>
	                </div>
	            </form>
			</section>
        </div><!--.pull-up -->
    </div>
<?php get_footer(); ?>