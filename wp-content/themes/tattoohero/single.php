<?php require_once( ABSPATH . '/wp-content/plugins/buddypress-media/app/main/includes/BPMediaHostWordpress.php' ); ?>


<?php get_header(); ?>

<?php if(get_post_type($post) == 'attachment') : ?>
    <div class="subpage container single-image">
        <?php if (have_posts()) : the_post(); global $post; ?>
        <?php 
            if (is_user_logged_in()){
                th_bp_record_vlc( 'image', 'view', $post->ID, bp_loggedin_user_id()); 
            }else{
                th_bp_record_vlc( 'image', 'view', $post->ID, getip());
            }

        ?>
        <section class="subpage grid_8">
            <img src="<?=$post->guid;?>" />
            <hr />
            <?php if (bp_loggedin_user_id() == '5'){
                echo '<a href="'.add_query_arg(array('a' => 'delete', 'postid' => $post->ID)).'">Delete Image</a>';

            }?>
            <div class="clr"></div>
            <div class="comments">
                <?php comments_template(); ?>
            </div>
            <?php //global $bp_media_current_entry; ?>
            
            <?php //$bp_media_current_entry = new BPMediaHostWordpress($post->ID); ?>
            <?php //$bp_media_current_entry->show_comment_form(); ?>

        </section>
        <aside id="sidebar" class="grid_4">
            <section class="widget image-details">
                <h2 class="uppercase"><?php the_title(); ?></h2>
                <div class="excerpt">
                    <p><?php //the_excerpt(); ?></p>
                </div>
                <?php vlc_view('image'); ?>
                <?php vlc_buttons('image'); ?>
                <div class="socials"><?php echo synved_social_share_markup(); ?></div>
                <div class="meta">
                    <div class="title">Description:<div>
                    <div class="tags"><?php the_excerpt(); ?></div>
                </div>
                <hr />
                <div class="user-data">
                    <?php 
                    $user = get_user_by('id',$post->post_author); 
                    ?>
                    <div class="avatar left">
                        <a href="<?=TH_HOMEPAGE;?>/members/<?=$user->user_login;?>">
                            <?=bp_core_fetch_avatar ( array( 'item_id' => $user->ID, 'type' => 'full' ) ) ?>
                        </a>
                    </div>
                    <div class="left">
                        <div class="title uppercase">Posted By: </div>
                        <div class="username">
                            <a href="<?=TH_HOMEPAGE;?>/members/<?=$user->user_login;?>"><?=xprofile_get_field_data( "Name" , $user->ID )?></a>
                        </div>
                        <p class="userloc uppercase">
                            <?php 
                                $city = xprofile_get_field_data( "City" , $user->ID );
                                $province = xprofile_get_field_data( "Province/State" , $user->ID );
                            ?>
                            <?php if($city): ?>
                                <span class="user-city"><?=$city;?><?php if($province) { echo trim(','); } ?> </span>
                            <?php endif; ?>
                            <?php if($province): ?>
                                <span class="user-province"> <?=$province;?></span>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
                <div class="clr"></div>
            </section>
            <section class="widget artist-details">
                <?php 
                    $artist_id = get_post_meta($post->ID,'artist',true); 
                    $artist = get_user_by('id',$artist_id);
                ?>
                <div class="artist-data">

                    <?php if($artist){ ?>
                    <div class="avatar left">
                        <a href="<?=TH_HOMEPAGE;?>/members/<?=$artist->user_login;?>">
                            <?=bp_core_fetch_avatar ( array( 'item_id' => $artist->ID, 'type' => 'full' ) ) ?>
                        </a>
                    </div>
                    <div class="left">
                        
                        <div class="username">
                            <div class="title uppercase">By Artist: </div>
                            <a href="<?=TH_HOMEPAGE;?>/members/<?=$artist->user_login;?>"><?=xprofile_get_field_data( "Name" , $artist->ID )?></a>
                        </div>
                        <div class="meta">
                            <!-- <div class="shop">Tattoo Shop Inc.</div> -->
                            <p class="userloc uppercase">
                                <?php 
                                    $city = xprofile_get_field_data( "City" , $artist->ID );
                                    $province = xprofile_get_field_data( "Province/State" , $artist->ID );
                                ?>
                                <?php if($city): ?>
                                    <span class="user-city"><?=$city;?><?php if($province) { echo trim(','); } ?> </span>
                                <?php endif; ?>
                                <?php if($province): ?>
                                    <span class="user-province"> <?=$province;?></span>
                                <?php endif; ?>
                            </p>
                            <!-- <div class="user-likes">1,200 Likes</div> -->
                            
                        </div>
                        <!--shop name data-->
                        <?php 
                        $artist_shops = BP_Groups_Member::get_is_admin_of($artist_id); 
                        foreach($artist_shops[groups] as $artist_shop){
                            ?>
                            <div class="title uppercase">Shop: </div>
                            <a href="<?php echo ('/shops/'.$artist_shop->slug); ?>"><?php echo $artist_shop->name; ?></a>
                            <?php
                        }  
                        ?>

                    </div>
                    <?php }else if($artist_id){ ?>
                        <div class="title uppercase">By Artist: </div>
                        <div class="username">
                            <?php echo $artist_id ?>
                        </div>
                    <?php } ?>
                </div>
                
            </section>
        </aside>
        <div class="clr"></div>
        <section class="widget image-suggestions">

            <?php 
            if($artist){
                ?>
                <a href="/members/<?=$artist->user_login;?>/media">More tattoos from <?=xprofile_get_field_data( "Name" , $artist->ID )?></a>
                <?php
                th_user_gallery($artist_id); 

                //echo '<a href="/members/
            }else{
                ?>
                <a href="/members/<?=$user->user_login;?>/media" style = "align: right;">More Tattoos from <?=xprofile_get_field_data( "Name" , $user->id )?></a>
                <?php
                th_user_gallery($user->id);

            }
            ?>
            <a href="/browse?o=popular" style = "align: right;">More Popular Tattoos</a>
            <?php th_popular_gallery(); ?>
        </section>
        <?php endif; ?>
    </div>

<?php elseif (get_post_type($post) == 'th_blog') : ?>
<?php do_action( 'bp_before_blog_single_post' ); ?>
	<div class="subpage container container-article">

    <?php if (have_posts()) : the_post(); global $post; ?>
        <?php 
            if (is_user_logged_in()){
                th_bp_record_vlc( 'image', 'view', $post->ID, bp_loggedin_user_id()); 
            }else{
                th_bp_record_vlc( 'image', 'view', $post->ID, getip());
            }

        ?>

        <div class="grid_12 article-banner">
            <?php the_post_thumbnail('full'); ?>
        </div>
        <div class="container-article-content">
            <section class="subpage grid_8">
                <div class="th_logo_article"></div>
                <h1 class="article-title"><?php the_title(); ?></h1>
            	<?php the_content(); ?>
                <?php comments_template(); ?>
            </section>
            <?php endif; ?>

            <aside id="sidebar" class="grid_4">
                <?=$TH->get_blog_posts_sidebar($post->ID); ?>
            	<?php //get_sidebar(); ?>

            </aside>
        </div><!--.pull-up -->

    </div>
    
<?php elseif (get_post_type($post) == 'post') : ?>
<?php do_action( 'bp_before_blog_single_post' ); ?>
	<?php if (have_posts()) : the_post(); global $post; ?>
    	
        <div class="grid_12 article-banner">
            <?php the_post_thumbnail('full'); ?>
        </div>
        <div class="container-article-content">
            <section class="subpage grid_8">
                <div class="th_logo_article"></div>
                <h1 class="article-title"><?php the_title(); ?></h1>
                <h3><?php the_category(); ?></h3>
            	<?php the_content(); ?>
                <?php comments_template(); ?>
            </section>
            <?php endif; ?>

            <aside id="sidebar" class="grid_4">
                <?=$TH->get_blog_posts_sidebar($post->ID); ?>
            	<?php //get_sidebar(); ?>

            </aside>
        </div><!--.pull-up -->

<?php endif; ?>

<?php get_footer(); ?>