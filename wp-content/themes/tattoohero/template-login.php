<?php
session_start();
 /* TEMPLATE NAME: LOGIN */ ?>

<?php $l = md5("login");
//$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	if ($_POST[$l]) {
		$valid_login = $TH->get_login($_POST['u'],$_POST['p']);
		if($valid_login['success']) { header('Location: '.TH_HOMEPAGE); exit(); }
		else { $msg = '<div class="alert alert-error" style="font-size: 13px">'. $valid_login['message'].'</div>'; }
	}
	if ("logout" == $_REQUEST["x"]) { wp_logout(); }// header('Location: '.$url); exit(); }

	if (!is_user_logged_in()) { //header('Location: '.TH_HOMEPAGE); }
	//else {
	
	//print_r('check_profile_form()');
	
?>

<?php get_header( 'buddypress' ); ?>
	<div class="subpage container login"><!-- begin #content -->
		<div class="grid_8" id="slogan">
			<img id="sloganText" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/Get_Inked_Title.png" />
		</div>
		<div class="grid_5 push_7">
			<div class="pre">
				<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/th_logo_tag_black2.gif" />
				
				<div class="socialLogin"><?php do_action( 'bp_before_account_details_fields' ); ?></div>
				<h2><?php _e( 'OR', 'buddypress' ); ?></h2>
			</div>
			<form method="post" class="clearfix">
				<?php do_action( 'template_notices' ); ?>
				<?php do_action( 'bp_before' ); ?>
				
				<?=$msg;?>
				<div class="login-section">
				    <input type="hidden" name="<?=$l;?>" value="<?=md5(date("is"));?>" />
					<input type="text" name="u" placeholder="Username" value="<?=$_POST['u'];?>" />
				    <input type="password" name="p" placeholder="Password" value="<?=$_POST['p'];?>" />
				    <a href="<?=wp_lostpassword_url();?>" class="left forgot-login">Forgot your log in?</a>
				    <button type="submit" class="btn btn-primary btn-large right">Let's go</button>
				</div>
			</form>
			<div class="align-center">
			  <h2>Not registered? No problemo<br /><small>It takes 1 minute... Seriously!</small></h2>
			  <a href="<?=TH_HOMEPAGE;?>/register" class="btn btn-primary btn-large">Register</a>
			</div>
		</div><!-- .grid_5 -->
	</div><!-- end #content -->

<?php get_footer( 'buddypress' ); ?>

<?php } ?>