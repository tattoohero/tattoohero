<?php
session_start();
/* TEMPLATE NAME: PROFILE DETAILS BACKUP */ 
 
//check if user's logged in
if (is_user_logged_in()) {
	$the_user_id = $bp->loggedin_user->userdata->ID;
	$the_user_login = $bp->loggedin_user->userdata->user_login;
	
	//$userData = $TH->get_user_info($the_user_id);
	$userData =  $bp->loggedin_user->userdata;
	
	//$roles = $TH->get_current_user_role();
	
	
	//check if form was submitted
	
	
	$current_url = get_permalink($post->ID);

}
else{
	header('Location: '.TH_HOMEPAGE.'/login'); exit();
}
 //get login type (registered, facebook, twitter) to determine what data we already have and fill form fields
 
 
 //if fb we need what?
 
 
 //else if twitter we need what?
 
 //else
 
 
 //which user role was selected from the dropdown? --> decides next step's options
 
 //collect error messages
 $errors = th_errors()->get_error_codes();
 
 ?>

<?php get_header(); ?>
	<div class="subpage container container-article profile-details">
		<div class="grid_12 article-banner">
        	<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/details_image01.jpg" />
        	<div class="grid_4 push_6 pd_slogan">
        		<img class="big" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_slogan_01.png" />
        		<img class="sub" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_txt_01.png" />
        	</div>
		</div>
        <div class="container-article-content">
            <section class="subpage grid_10">
                <div class="progressBox">
	                <div id="info" class="active col_1of3"><a href="" title="quick info"></a>
		                <div class="dot"></div>
		                <div class="line"></div>
	                </div>
	                <div id="share" class="col_1of3"><a href="/profileDetails2" title="share and upload"></a>
	                	<div class="dot"></div>
	                	<div class="line"></div>
	                </div>
	                <div id="start" class="col_1of3"><a href="" title="start browsing"></a>
	                </div>
                </div>
                
                <?php do_action( 'bp_before_profile_edit_content' ); ?>
                
                <form id="profileForm" method="POST" action="<?php echo $current_url; ?>">
	                <div id="fields">
	                	<h3>Does this seem right?</h3>
	                	<p>Check out the info we gathered from you below. Feel free to get creative where you want!</p>
	                	
	                	<?php $TH->show_errors(); ?>
	                	<?php do_action( 'template_notices' ); ?>

	                	<div id="items">
			                <?php 
			                if ( bp_has_profile('profile_group_id=' . bp_get_current_profile_group_id().'&user_id='.bp_loggedin_user_id().'&hide_empty_fields=0') ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>
			                
						    <?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
							
							<div<?php bp_field_css_class( 'editfield' ); ?>>
								<div class="field_content col_2of4">
										
								<?php if ( 'textbox' == bp_get_the_profile_field_type() ) : ?>
							
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
										<input type="text" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" value="<?php $idVal = bp_get_the_profile_field_edit_value(); echo empty($_POST[$idVal])? bp_the_profile_field_edit_value() : $_POST[$idVal]; ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>/>
								</div>
								<div class="description col_2of4"><?php /* Special cases here for descriptions */
	
									switch(bp_get_the_profile_field_name()){
										case "Name":
											_e('This is your display name.', 'tattoohero');
											break;
										case "City":
											_e('This is the place you are currently located.', 'tattoohero');
											break;
										case "Country":
											_e('This is the country you are currently in.', 'tattoohero');
											break;
										case "Interests":
											_e('Tell us what you are in to.', 'tattoohero');
											break;
									}
								?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'textarea' == bp_get_the_profile_field_type() ) : ?>
								
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<textarea rows="5" cols="40" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>><?php $idVal = bp_get_the_profile_field_edit_value(); echo empty($_POST[$idVal])? bp_the_profile_field_edit_value() : $_POST[$idVal]; ?></textarea>
								</div>
								<div class="description col_2of4"><?php /* Special cases here for descriptions */
	
									switch(bp_get_the_profile_field_name()){
										case "About Me":
											echo "Let others peek inside your brain.";
											break;
									}
								?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'selectbox' == bp_get_the_profile_field_type() ) : ?>
							
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<select name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
										<?php bp_the_profile_field_options(); ?>
									</select>
								</div>
								<div class="description col_2of4" id="member"><?php /* Special cases here for descriptions */
	
									switch(bp_get_the_profile_field_name()){
										case "Member Type":
											echo '<div id="member-type">
								                	<h3>TATTOO COLLECTOR</h3>
									                <div>You love looking at tattoos & you got some ink on you as well.</div>
									              </div>';
									        break;
									} ?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'multiselectbox' == bp_get_the_profile_field_type() ) : ?>
							
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<select name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" multiple="multiple" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
										<?php bp_the_profile_field_options(); ?>
							
									</select>
							
									<?php if ( !bp_get_the_profile_field_is_required() ) : ?>
										<a class="clear-value" href="javascript:clear( '<?php bp_the_profile_field_input_name(); ?>' );"><?php _e( 'Clear', 'buddypress' ); ?></a>
									<?php endif; ?>
								</div>
								<div class="description col_2of4"><?php /* Special cases here for descriptions */
	
									switch(bp_get_the_profile_field_name()){
										case "Member Type ":
											echo '<div id="member-type">
								                	<h3>TATTOO COLLECTOR</h3>
									                <div>You love looking at tattoos & you got some ink on you as well.</div>
									              </div>';
									        break;
									} ?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'radio' == bp_get_the_profile_field_type() ) : ?>
							
									<div class="radio">
										<span class="label"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></span>
							
										<?php bp_the_profile_field_options(); ?>
							
										<?php if ( !bp_get_the_profile_field_is_required() ) : ?>
							
											<a class="clear-value" href="javascript:clear( '<?php bp_the_profile_field_input_name(); ?>' );"><?php _e( 'Clear', 'buddypress' ); ?></a>
							
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
							
								<?php if ( 'checkbox' == bp_get_the_profile_field_type() ) : ?>
							
									<div class="checkbox">
										<span class="label"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></span>
							
										<?php bp_the_profile_field_options(); ?>
									</div>
								</div>
								<?php endif; ?>
							
								<?php if ( 'datebox' == bp_get_the_profile_field_type() ) : ?>
							
									<div class="datebox">
										<label for="<?php bp_the_profile_field_input_name(); ?>_day"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
							
										<select name="<?php bp_the_profile_field_input_name(); ?>_day" id="<?php bp_the_profile_field_input_name(); ?>_day" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
											<?php bp_the_profile_field_options( 'type=day' ); ?>
							
										</select>
							
										<select name="<?php bp_the_profile_field_input_name(); ?>_month" id="<?php bp_the_profile_field_input_name(); ?>_month" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
											<?php bp_the_profile_field_options( 'type=month' ); ?>
							
										</select>
							
										<select name="<?php bp_the_profile_field_input_name(); ?>_year" id="<?php bp_the_profile_field_input_name(); ?>_year" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
											<?php bp_the_profile_field_options( 'type=year' ); ?>
							
										</select>
									</div>
								</div>
								<?php endif; ?>
							
								<?php if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
								<p class="field-visibility-settings-toggle" id="field-visibility-settings-toggle-<?php bp_the_profile_field_id() ?>">
									<?php printf( __( 'This field can be seen by: <span class="current-visibility-level">%s</span>', 'buddypress' ), bp_get_the_profile_field_visibility_level_label() ) ?> <a href="#" class="visibility-toggle-link"><?php _e( 'Change', 'buddypress' ); ?></a>
								</p>
						
								<div class="field-visibility-settings" id="field-visibility-settings-<?php bp_the_profile_field_id() ?>">
									<fieldset>
										<legend><?php _e( 'Who can see this field?', 'buddypress' ) ?></legend>
										<?php bp_profile_visibility_radio_buttons() ?>
									</fieldset>
									<a class="field-visibility-settings-close" href="#"><?php _e( 'Close', 'buddypress' ) ?></a>
								</div>
								<!--<?php //else : ?>
									<div class="field-visibility-settings-notoggle" id="field-visibility-settings-toggle-<?php //bp_the_profile_field_id() ?>">
										<?php //printf( __( 'This field can be seen by: <span class="current-visibility-level">%s</span>', 'buddypress' ), bp_get_the_profile_field_visibility_level_label() ) ?>
									</div>-->
								
								<?php endif; ?>
							</div>
							
							<?php endwhile; endwhile; endif; ?>
		                </div>
	                </div>
	                <div id="legal">
	                	<div id="pre">
		                	<div class="col_3of4">Here's the legal mumbo-jumbo.</div>
		                	<div class="col_1of4" style="display: none;">In layman's terms.</div>
	                	</div>
	                	<div id="text" class="col_3of4">
	                		<?php 
		                		$legalPageId=3108; // = Legal Terms 
								$post = get_page($legalPageId); 
								$content = apply_filters('the_content', $post->post_content); 
								echo '<h1>Terms and Conditions</h1>'.$content; 
							?> 
						</div>
						<div id="context" class="col_1of4" style="display: none;">
		                	<span class="indent"></span> explain this legal nonsense
	                	</div>
	                	<div class="checkbox col_3of4">
		                	<div class="checkbox_input<?php if(!empty($errors) && in_array('Accept Terms', $errors)) echo ' errors'; ?>">
								<input id="terms" type="checkbox" name="terms_and_conditions" />
								<label for="terms"><h2><?php _e( 'Accept Terms &amp; Conditions', 'tattoohero'); ?></h2></label>
							</div>
						</div>
	                </div>
	                <div class="col_3of4">
		                <input type="hidden" name="field_ids" id="field_ids" value="<?php bp_the_profile_group_field_ids(); ?>" />
		                <input type="hidden" name="profileDetails_action" value="step1"/>
		                <input type="hidden" name="userID" value="<?php echo bp_loggedin_user_id(); ?>"/>
						<input type="hidden" name="_redirect" value="<?php  ?>"/>
						<input type="hidden" name="profileDetails_nonce" value="<?php echo wp_create_nonce('details-nonce'); ?>"/>
						<input id="details_submit" class="btn btn-primary btn-large col_3of4" type="submit" value="<?php _e('Next', 'tattoohero'); ?>"/>
	                </div>
				</form>
			</section>
        </div><!--.pull-up -->
    </div>
<?php get_footer(); ?>