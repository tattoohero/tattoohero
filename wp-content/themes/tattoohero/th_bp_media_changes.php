<?php
require_once( ABSPATH . '/wp-content/plugins/buddypress-media/app/main/includes/BPMediaHostWordpress.php' );
/*
* This file makes the adjustments to the plugin, BP Media, that were needed.
* Record of Change format: Change, File, function, hook added, code removed.
*/


/*
* Change: Add Image to the edit screen.
* File: app/main/profile/BPMediaScreen.php
* Function: edit_screen_content
* Hook Added: th_edit_screen_image
* Code Removed: N/A
*/
function add_image_to_edit_screen(){
	global $bp_media_current_entry;
?>
	<img src = "<?php echo($bp_media_current_entry->get_attachment_url())?>" width = "600">
<?php
}
add_action('th_edit_screen_image', 'add_image_to_edit_screen');

/*
* Change: Add Artist field to the edit screen.
* File: app/main/profile/BPMediaScreen.php
* Function: edit_screen_content
* Hook Added: th_edit_screen_artist_field
* Code Removed: N/A
*/

function add_artist_field_to_edit_screen(){
	global $bp_media_current_entry, $bp_media_default_excerpts;
	?>
	<label for="th-upload-input-artist">
            <?php echo('Tattoo Artist'); ?>
    </label>
    <input id="th-upload-input-artist" type="text" name="th_artist" class="settings-input"
            maxlength="<?php echo max(array($bp_media_default_excerpts['single_entry_title'], $bp_media_default_excerpts['activity_entry_title'])) ?>" value="<?php echo get_post_meta($bp_media_current_entry->get_id(),'artist', true) ?>" />
    <?php
}
add_action('th_edit_screen_artist_field','add_artist_field_to_edit_screen');


function th_album_title($albumID){
	//$bp_media_current_entry = new BPMediaHostWordpress($albumID);
	
	$editAlbum = new BPMediaAlbum($albumID);
	
	$title = $editAlbum->get_title();
	
	echo $title;
}
//add_action('get_album_title','album_title');



//Change: function to process the description field of a image

function process_description(BPMediaHostWordpress $media) {
	//fb($media);
	//print_r('process_description');
	update_post_meta($media->get_id(),'artist',$_POST['th_artist']);
	/*$defaults = array(
		'name' => $media->name,
		'description' => $media->description,
		'owner' => $media->owner
	);*/
	//$args = wp_parse_args( $args, $defaults );
	$post = get_post( $media->get_id(), ARRAY_A );
	//$post[ 'post_title' ] = esc_html( $args[ 'name' ] );
	$post[ 'post_content' ] = esc_html($media->get_content());
	//$post[ 'post_author' ] = $args[ 'owner' ];
	$result = wp_update_post( $post );
	
	return $result;
}
add_action('bp_media_after_update_media','process_description');


/*
* Change: Add Artist field to the edit screen.
* File: app/main/profile/BPMediaScreen.php
* Function: screen_content
* Hook Added: th_profile_image_gallery
* Code Removed: BPMediaUploadScreen::upload_screen_content(); (x2)
*/
function th_profile_display_gallery(){
	//the following function is in the th_bp_media_query.php file
	th_gallery_profile();
}
add_action('th_profile_image_gallery','th_profile_display_gallery');



function th_edit_album($albumID){
	//global $bp_media_current_entry, $bp_media_default_excerpts;
	print_r('the_edit_album');
	
	$bp_media_current_entry = new BPMediaHostWordpress($albumID);
	//BPMediaFunction::update_media();
	$editAlbum = new BPMediaScreen('album', 'album');
	$editAlbum->edit_screen_content();
	/*
	// this is: $editAlbum->edit_screen_content();
	
	
	do_action('th_edit_screen_image');
	
    ?>
    <form method="post" class="standard-form" id="bp-media-upload-form">
        <label for="bp-media-upload-input-title">
            <?php printf(__('%s Title', BP_MEDIA_TXT_DOMAIN), ucfirst(album)); ?>
        </label>
        <input id="bp-media-upload-input-title" type="text" name="bp_media_title" class="settings-input"
               maxlength="<?php echo max(array($bp_media_default_excerpts['single_entry_title'], $bp_media_default_excerpts['activity_entry_title'])) ?>"
               value="<?php echo $bp_media_current_entry->get_title(); ?>" />
        <?php 
        	do_action('th_edit_screen_artist_field'); 
        ?>
        <label for="bp-media-upload-input-description">
            <?php printf(__('%s Description', BP_MEDIA_TXT_DOMAIN), ucfirst($this->media_type)); ?>
        </label>
        <textarea id="bp-media-upload-input-description" name="bp_media_description" class="settings-input"
                  maxlength="<?php echo max(array($bp_media_default_excerpts['single_entry_description'], $bp_media_default_excerpts['activity_entry_description'])) ?>"
                  ><?php echo $bp_media_current_entry->get_content(); ?></textarea>
                  <?php do_action('bp_media_add_media_fields', $this->media_type); ?>
        <div class="submit">
            <input type="submit" class="auto" value="<?php _e('Update', BP_MEDIA_TXT_DOMAIN); ?>" />
            <a href="<?php echo $bp_media_current_entry->get_url(); ?>" class="button" title="<?php _e('Back to Media File', BP_MEDIA_TXT_DOMAIN); ?>">
                <?php _e('Back to Media', BP_MEDIA_TXT_DOMAIN); ?>
            </a>
        </div>
    </form>
    <?php*/
}
add_action('th_edit_single_album','th_edit_album');


/*
* Change: reformat the profile gallery
* File: app/main/profile/BPMediaAlbumScreen.php
* Function: entry_screen_content
* Hook Added: th_image_single_album
* Code Removed: 6 lines after the hook
*/
function th_display_single_album($albumID){
	if(bp_loggedin_user_id() == bp_displayed_user_id()) th_edit_new_images_form($albumID);
	else 												th_gallery_album($albumID);	
}
add_action('th_image_single_album','th_display_single_album');


/*
* Change: remove upload buttons on album screen
* File: app/main/profile/BPMediaAlbumScreen.php
* Function: entry_screen_content()
* Hook Added: N/A
* Code Removed: BPMediaUploadScreen::upload_screen_content(); (x2)
*/

/*
* Change: show upload form on custom pages
* File: app/main/profile/BPMediaUploadScreen.php
* Function: upload_enqueue and upload_screen_content
* Hook Added: th_upload_form
* Code Removed: $this->template->upload_form_multiple()
*/
//!th_upload_form
function th_display_upload_form(){
	//print_r('th_display_upload_form()');
	/*while( list( $field, $value ) = each( $_POST )) {
	   echo "<p>" . $field . " = " . $value . "</p>\n";
	}*/
	
	global $bp_media_current_album;
	$media = new BPMediaUploadScreen('image','image');
	
	$media->upload_enqueue();
	$media->upload_screen_content();
	
	//do_action('th_edit_single_album', $bp_media_current_album->get_id());
}
//add_action('th_upload_form','th_display_upload_form');


function th_single_image_redirect(){
	global $bp, $bp_media_current_entry;
	//echo $bp->action_variables[0];
	if($bp->action_variables[0] == constant('BP_MEDIA_IMAGES_VIEW_SLUG')){
		$postid = $bp_media_current_entry->get_id();
		$permalink = get_permalink($postid);
		wp_redirect($permalink);
	}
}
add_action('bp_head', 'th_single_image_redirect');
?>


