<?php
/* TEMPLATE NAME: PROFILE DETAILS - PAGE 1 */ 
$tpl_name = 'details';


//check if user's logged in
if (!is_user_logged_in()) wp_redirect( '/login' ); 
else $current_url = get_permalink($post->ID);

$bp->displayed_user->id = $bp->loggedin_user->id;

?>

<?php get_header(); ?>
	<div class="subpage container container-article profile-details">
		<div class="grid_12 article-banner">
        	<img src="<?=TH_TEMPLATE_DIR_IMAGES;?>/details_image01.jpg" />
        	<div class="grid_4 push_6 pd_slogan">
        		<img class="big" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_slogan_01.png" />
        		<img class="sub" src="<?=TH_TEMPLATE_DIR_IMAGES;?>/pd/pd_txt_01.png" />
        	</div>
		</div>
        <div class="container-article-content">
            <section class="subpage grid_10">
                <div class="progressBox">
	                <div id="info" class="active col_1of3"><a href="" title="quick info"></a>
		                <div class="dot"></div>
		                <div class="line"></div>
	                </div>
	                <div id="share" class="col_1of3"><a href="/profileDetails2" title="share and upload"></a>
	                	<div class="dot"></div>
	                	<div class="line"></div>
	                </div>
	                <div id="start" class="col_1of3"><a href="/profileDetails3" title="start browsing"></a>
	                </div>
                </div>
                <?php 
                	//global $bp;
                	//$bp->no_status_set = true;
                	
                	upload_new_avatar();
                	do_action( 'template_notices' ); 
                	$TH->show_errors(); 
                ?>
	                	
                <div class="pre">
	                <?php //loop to get content
					if (have_posts()) : while (have_posts()) : ?>
					<?php the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif;?> 
				</div>
                
                <div class="avatar">
                	<h4><?php _e( 'Change Avatar', 'buddypress' ); ?></h4>

					<?php if ( !(int)bp_get_option( 'bp-disable-avatar-uploads' ) ) : ?>
					
						<p><?php _e( 'Your avatar will be used on your profile and throughout the site. If there is a <a href="http://gravatar.com">Gravatar</a> associated with your account email we will use that, or you can upload an image from your computer.', 'buddypress'); ?></p>
					
						<form action="/profiledetails" method="post" id="avatar-upload-form" class="standard-form" enctype="multipart/form-data">
					
							<?php if ( 'upload-image' == bp_get_avatar_admin_step() ) : ?>
					
								<?php wp_nonce_field( 'bp_avatar_upload' ); ?>
								<p><?php _e( 'Click below to select a JPG, GIF or PNG format photo from your computer and then click \'Upload Image\' to proceed.', 'buddypress' ); ?></p>
								<p id="avatar-upload">
									<input type="file" name="file" id="file" />
									<input type="submit" name="upload" id="upload" value="<?php _e( 'Upload Image', 'buddypress' ); ?>" />
									<input type="hidden" name="action" id="action" value="bp_avatar_upload" />
								</p>
							<?php endif; ?>
					
							<?php if ( 'crop-image' == bp_get_avatar_admin_step() ) : ?>
					
								<h5><?php _e( 'Crop Your New Avatar', 'buddypress' ); ?></h5>
					
								<img src="<?php bp_avatar_to_crop(); ?>" id="avatar-to-crop" class="avatar" alt="<?php _e( 'Avatar to crop', 'buddypress' ); ?>" />
					
								<div id="avatar-crop-pane">
									<img src="<?php bp_avatar_to_crop(); ?>" id="avatar-crop-preview" class="avatar" alt="<?php _e( 'Avatar preview', 'buddypress' ); ?>" />
								</div>
					
								<input type="submit" name="avatar-crop-submit" id="avatar-crop-submit" value="<?php _e( 'Crop Image', 'buddypress' ); ?>" />
					
								<input type="hidden" name="image_src" id="image_src" value="<?php bp_avatar_to_crop_src(); ?>" />
								<input type="hidden" id="x" name="x" />
								<input type="hidden" id="y" name="y" />
								<input type="hidden" id="w" name="wi" />
								<input type="hidden" id="h" name="h" />
					
								<?php wp_nonce_field( 'bp_avatar_cropstore' ); ?>
					
							<?php endif; ?>
					
						</form>
					
					<?php else : ?>
					
						<p><?php _e( 'Your avatar will be used on your profile and throughout the site. To change your avatar, please create an account with <a href="http://gravatar.com">Gravatar</a> using the same email address as you used to register with this site.', 'buddypress' ); ?></p>
					
					<?php endif; ?>
					
				</div>
                
                <?php do_action( 'bp_before_profile_edit_content' ); ?>
                
               
                <form id="profileForm" method="POST" action="/profiledetails">
	                <div id="fields">
	                	<div id="items">
			                <?php 
			                
			                if ( bp_has_profile('profile_group_id=' . bp_get_current_profile_group_id().'&user_id='.bp_loggedin_user_id().'&hide_empty_fields=0') ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>
			                
						    <?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
							
							<div<?php bp_field_css_class( 'editfield' ); ?>>

							<?php 
								//for hidden fields make the first part of the field name 'hidden' 
								//as well as city, province, country --> we are transitioning to 'location' combining these fields
							?>
							<?php if ( 'hidden' == 	 substr(bp_get_the_profile_field_name(), 0, 6) || 
										'field_3' == bp_get_the_profile_field_input_name() || //city
										'field_4' == bp_get_the_profile_field_input_name() || //province/state
										'field_5' == bp_get_the_profile_field_input_name()){ //country ?>
			
								<input type="hidden" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" value="<?php bp_the_profile_field_edit_value(); ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>/>
			
							<?php } else {  ?>

								<div class="field_content col_2of4">
										
								<?php if ( 'textbox' == bp_get_the_profile_field_type() ) : ?>
							
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> 
									<?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
										<input type="text" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" value="<?php $idVal = bp_get_the_profile_field_edit_value(); echo empty($_POST[$idVal])? bp_the_profile_field_edit_value() : $_POST[$idVal]; ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?> placeholder="<?php if('field_22' == bp_get_the_profile_field_input_name()) echo "location (minimum 3 letters)..."; ?>"/>
								</div>
								<div class="description col_2of4">
								<?php /* Special cases here for descriptions */
									switch(bp_get_the_profile_field_name()){
										case "Name":
											_e('This is your display name.', 'tattoohero');
											break;
										case "City":
											_e('This is the place you are currently located.', 'tattoohero');
											break;
										case "Country":
											_e('This is the country you are currently in.', 'tattoohero');
											break;
										case "Interests":
											_e('Tell us what you are in to.', 'tattoohero');
											break;
										case "Website":
											_e('Is there a site you would like to share with the community.', 'tattoohero');
											break;
										default:
											bp_the_profile_field_description();
											break;
									}
								?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'textarea' == bp_get_the_profile_field_type() ) : ?>								
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<textarea rows="5" cols="40" name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>><?php $idVal = bp_get_the_profile_field_edit_value(); echo empty($_POST[$idVal])? bp_the_profile_field_edit_value() : $_POST[$idVal]; ?></textarea>
								</div>
								<div class="description col_2of4"><?php /* Special cases here for descriptions */
									switch(bp_get_the_profile_field_name()){
										case "About Me":
											echo "Let others peek inside your brain.";
											break;
										default:
											bp_the_profile_field_description();
											break;
									}
								?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'selectbox' == bp_get_the_profile_field_type() ) : ?>
							
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<select name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
										<?php bp_the_profile_field_options(); ?>
									</select>
								</div>
								<div class="description col_2of4" id="member"><?php /* Special cases here for descriptions */
	
									switch(bp_get_the_profile_field_name()){
										case 'Member Type': ?>
										<div id="member-type">
											<div id="member" class="selected">
												<h3><?php _e('Member','tattoohero') ?></h3>
												<div><?php _e('You are interested in getting your first tattoo or just want to know what\'s out there.','tattoohero') ?></div>
											</div>
											<div id="artist" class="hide">
												<h3><?php _e('Artist','tattoohero') ?></h3>
												<div><?php _e('You work at a shop (not apprenticing).','tattoohero') ?></div>
											</div>
											<div id="apprentice" class="hide">
												<h3><?php _e('Apprentice','tattoohero') ?></h3>
												<div><?php _e('You work in a shop and are supervised by professional artist.','tattoohero') ?></div>
											</div>
											<div id="collector" class="hide">
												<h3><?php _e('Tattoo Collector','tattoohero') ?></h3>
												<div><?php _e('You love looking at tattoos & you got some ink on you as well.','tattoohero') ?></div>
											</div>
										</div>
										<?php break;
									} ?>
								</div>
								<div class="editfield shop hide" id="shopField">
									<div class="field_content col_2of4">
										<label for="up-shop0"><?php _e('Shop Name', 'tattoohero'); echo " "; _e('(required)', 'tattoohero'); ?></label>
										<!--<input id="up-shop" type="text" name="shopName" />-->
										<?php 
											//shopselectfield(); 
											//echo autocompleteshops(0);
											shopSelection();	
										?>
										<input id="up-shop0" type="text" name="0[shop]" value="<?php if(isset($_POST[0]['shop'])) echo $_POST[0]['shop']; ?>" placeholder="<?php _e('Shop Name', 'tattoohero'); ?>" class="hide">
										<input type="hidden" id="up-shop-id0" name="0[shop_id]" value="" />
										
									</div>
									<div class="description col_2of4"><?php _e('Please enter your shop\'s name, if we already know about it you should be able to select it from the dropdown list', 'tattoohero'); ?></div>
								</div>
								<?php endif; ?>
							
								<?php if ( 'multiselectbox' == bp_get_the_profile_field_type() ) : ?>
							
									<label for="<?php bp_the_profile_field_input_name(); ?>"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
									<select name="<?php bp_the_profile_field_input_name(); ?>" id="<?php bp_the_profile_field_input_name(); ?>" multiple="multiple" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
										<?php bp_the_profile_field_options(); ?>
							
									</select>
							
									<?php if ( !bp_get_the_profile_field_is_required() ) : ?>
										<a class="clear-value" href="javascript:clear( '<?php bp_the_profile_field_input_name(); ?>' );"><?php _e( 'Clear', 'buddypress' ); ?></a>
									<?php endif; ?>
								</div>
								<div class="description col_2of4"><?php /* Special cases here for descriptions */
									switch(bp_get_the_profile_field_name()){
										case "Member Type ":
											echo '<div id="member-type">
								                	<h3>TATTOO COLLECTOR</h3>
									                <div>You love looking at tattoos & you got some ink on you as well.</div>
									              </div>';
									        break;
									    default:
									    	bp_the_profile_field_description();
									    	break;
									} ?>
								</div>
								<?php endif; ?>
							
								<?php if ( 'radio' == bp_get_the_profile_field_type() ) : ?>
							
									<div class="radio">
										<span class="label"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></span>
							
										<?php bp_the_profile_field_options(); ?>
							
										<?php if ( !bp_get_the_profile_field_is_required() ) : ?>
							
											<a class="clear-value" href="javascript:clear( '<?php bp_the_profile_field_input_name(); ?>' );"><?php _e( 'Clear', 'buddypress' ); ?></a>
							
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
							
								<?php if ( 'checkbox' == bp_get_the_profile_field_type() ) : ?>
							
									<div class="checkbox">
										<span class="label"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></span>
							
										<?php bp_the_profile_field_options(); ?>
									</div>
								</div>
								<?php endif; ?>
							
								<?php if ( 'datebox' == bp_get_the_profile_field_type() ) : ?>
							
									<div class="datebox">
										<label for="<?php bp_the_profile_field_input_name(); ?>_day"><?php bp_the_profile_field_name(); ?> <?php if ( bp_get_the_profile_field_is_required() ) : ?><?php _e( '(required)', 'buddypress' ); ?><?php endif; ?></label>
							
										<select name="<?php bp_the_profile_field_input_name(); ?>_day" id="<?php bp_the_profile_field_input_name(); ?>_day" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
											<?php bp_the_profile_field_options( 'type=day' ); ?>
							
										</select>
							
										<select name="<?php bp_the_profile_field_input_name(); ?>_month" id="<?php bp_the_profile_field_input_name(); ?>_month" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
											<?php bp_the_profile_field_options( 'type=month' ); ?>
							
										</select>
							
										<select name="<?php bp_the_profile_field_input_name(); ?>_year" id="<?php bp_the_profile_field_input_name(); ?>_year" <?php if ( bp_get_the_profile_field_is_required() ) : ?>aria-required="true"<?php endif; ?>>
							
											<?php bp_the_profile_field_options( 'type=year' ); ?>
							
										</select>
									</div>
								</div>
								<?php endif; ?>
							<?php }; //end of else: hidden field ?>
								
							</div>
							
							<?php endwhile; endwhile; endif; ?>
		                </div>
	                </div>
	                <div class="profileButtons">
	                	<input type="hidden" name="field_ids" id="field_ids" value="<?php bp_the_profile_group_field_ids(); ?>" />
		                <input type="hidden" name="profileDetails_action" value="step1"/>
		                <input type="hidden" name="userID" value="<?php echo bp_loggedin_user_id(); ?>"/>
						<input type="hidden" name="profileDetails_nonce" value="<?php echo wp_create_nonce('details-nonce'); ?>"/>
						<input id="details_submit" class="btn btn-primary btn-large col_3of4" type="submit" value="<?php _e('Next', 'tattoohero'); ?>"/>
	                </div>
	            </form>
			</section>
        </div><!--.pull-up -->
    </div>
<?php get_footer(); ?>