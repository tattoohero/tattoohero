<?php

//function bp_group_meta_init() {
	function custom_field($meta_key='') {
		//get current group id and load meta_key value if passed. If not pass it blank
		return groups_get_groupmeta( bp_get_group_id(), $meta_key) ;
	}
	//code if using seperate files require( dirname( __FILE__ ) . '/buddypress-group-meta.php' );
	// This function is our custom field's form that is called in create a group and when editing group details
	 function th_group_header_fields_markup() {
	 	global $bp, $wpdb;
	 	?>
	 	<label for="group-city">City</label>
	 	<input id="group-city" type="text" name="group-city" value="<?php echo custom_field('group-city'); ?>" /> 
	 	<br>
	 	<input id="group-city-id" type="hidden" name="group-city-id" value="<?php echo custom_field('group-city-id'); ?>" /> 
	 	<br>
	 	<?php 
	 }
	// This saves the custom group meta – props to Boone for the function
	// Where $plain_fields = array.. you may add additional fields, eg
	 // $plain_fields = array(
	 //     'field-one',
	 //     'field-two'
	 // );
	function th_group_header_fields_save( $group_id ) {
		global $bp, $wpdb;
		$plain_fields = array(
			'group-city',
			'group-city-id'
		);
		foreach( $plain_fields as $field ) {
			$key = $field;
			if ( isset( $_POST[$key] ) ) {
			$value = $_POST[$key];
				groups_update_groupmeta( $group_id, $field, $value );
			}
		}
	}
	add_filter( 'groups_custom_group_fields_editable', 'th_group_header_fields_markup' );
	add_action( 'groups_group_details_edited', 'th_group_header_fields_save' );
	add_action( 'groups_created_group',  'th_group_header_fields_save' );
	 
	// Show the custom field in the group header
	function th_show_field_in_header() {
		echo "<p> My favorite color is:" . custom_field('group-city') . "</p>";
	}
	//add_action('bp_group_header_meta' , 'th_show_field_in_header') ;
//}
//add_action( 'bp_include', 'bp_group_meta_init' );
/* If you have code that does not need BuddyPress to run, then add it here. */
?>