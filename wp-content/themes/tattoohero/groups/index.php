<?php

/**
 * BuddyPress - Groups Directory
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

if ($_GET['shopq']){
	wp_redirect(home_url()."/shops/?s=".$_GET['shopq']);
}

get_header( 'buddypress' ); ?>

	<?php do_action( 'bp_before_directory_groups_page' ); ?>

	<div class="subpage container shops">

		<?php do_action( 'bp_before_directory_groups' ); ?>

		<form action="" method="post" id="groups-directory-form" class="dir-form">

			<h1 class="search-heading align-center"><?php _e( 'Shops Directory', 'buddypress' ); ?><?php if ( is_user_logged_in() && bp_user_can_create_groups() ) : ?><?php /* &nbsp;<a class="button" href="<?php echo trailingslashit( bp_get_root_domain() . '/' . bp_get_groups_root_slug() . '/create' ); ?>"><?php _e( 'Create a Group', 'buddypress' ); ?></a> */ ?><?php endif; ?></h1>

			<?php do_action( 'bp_before_directory_groups_content' ); ?>

			<div class="search grid_6 push_3">
				<div id="group-dir-search" class="dir-search" role="search">

					<?php bp_directory_groups_search_form(); ?>
		<div class="clr"></div>

				</div><!-- #group-dir-search -->
			</div><!-- .search -->
			<div class="clr"></div>

			<?php do_action( 'template_notices' ); ?>
<!-- 
			<div class="item-list-tabs grid_12" id="subnav" role="navigation">
				<ul>
					<li class="selected" id="groups-all"> --><a href="<?php //echo trailingslashit( bp_get_root_domain() . '/' . bp_get_groups_root_slug() ); ?>"><?php //printf( __( 'All Shops <span></span>', 'buddypress' ), bp_get_total_group_count() ); ?></a></li>

					<?php if ( is_user_logged_in() && bp_get_total_group_count_for_user( bp_loggedin_user_id() ) ) : ?>

						<!-- <li id="groups-personal"> --><a href="<?php //echo trailingslashit( bp_loggedin_user_domain() . bp_get_groups_slug() . '/my-groups' ); ?>"><?php //printf( __( 'My Shops <span>%s</span>', 'buddypress' ), bp_get_total_group_count_for_user( bp_loggedin_user_id() ) ); ?></a></li>

					<?php endif; ?>

					<?php do_action( 'bp_groups_directory_group_filter' ); ?>

				<!-- </ul> -->
			<!-- </div> --><!-- .item-list-tabs -->

			<div class="item-list-tabs" role="navigation">
				<ul>

					<?php do_action( 'bp_groups_directory_group_types' ); ?>
				</ul>
			</div>

			<div id="groups-dir-list" class="groups dir-list">
				<?php 
						$location_pref = get_location_pref();
						$meta_filter = new BP_Groups_Meta_Filter( 'groupcityid', $location_pref ); 
						if ($location_pref != ""){
							echo '<h2>Shops in '.get_city_name($location_pref).'</h2>';
						
							if(count($meta_filter->get_group_ids()) <= 0){
								$message = '<p> There are no Shops registered for your area yet.  If you would like us to research your area or if you have a recommendation please email us at <a href="mailto:hello@tattoohero.com?subject=A suggestion from one of Tattoo Hero\'s beloved users&Body=Please help me find an awesome Tattoo Artist in '.get_city_name($location_pref).'. I would also like to recommend (artist name/shop name) to you guys!">hello@tattoohero.com</a>.</p>';
								$message .= '<h2>Shops (All)</h2>';
								$meta_filter->remove_filters();
								$meta_filter = new BP_Groups_Meta_Filter( 'groupcityid', $location_pref );
							}
						} else {
							echo '<h2>Shops (All)</h2>';
						}
						echo $message;
				?>

				<?php locate_template( array( 'groups/groups-loop.php' ), true ); ?>
				<?php $meta_filter->remove_filters(); ?>
			</div><!-- #groups-dir-list -->

			<?php do_action( 'bp_directory_groups_content' ); ?>

			<?php wp_nonce_field( 'directory_groups', '_wpnonce-groups-filter' ); ?>

			<?php do_action( 'bp_after_directory_groups_content' ); ?>

		</form><!-- #groups-directory-form -->

		<?php do_action( 'bp_after_directory_groups' ); ?>

		</div><!-- .shops -->

	<?php do_action( 'bp_after_directory_groups_page' ); ?>

<?php get_footer( 'buddypress' ); ?>