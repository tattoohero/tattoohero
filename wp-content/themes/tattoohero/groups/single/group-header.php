<?php do_action( 'bp_before_group_header' ); ?>
<section class="user-header grid_12 alpha omega" role="complementary">	

<div class="member-options-nav" role="navigation">
	<ul>
		<li class="btn-profilemenu">
			<span class="arrowfont">&#xe00b;</span>&nbsp;&nbsp;Shop Menu
			<ul class="dropdown">
				<?php bp_get_options_nav(); ?>
				<?php do_action( 'bp_group_options_nav' ); ?>
			</ul>
		</li>
	</ul>
</div>

<div class="shop-avatar grid_2 alpha">
	<a href="<?php bp_group_permalink(); ?>" title="<?php bp_group_name(); ?>">

		<?php bp_group_avatar(); ?>

	</a>
</div><!-- .user-avatar -->

<div class="shop-header-content grid_8 omega prefix_half">

	<div id="item-header-content">
		<h2><a href="<?php bp_group_permalink(); ?>" title="<?php bp_group_name(); ?>"><?php bp_group_name(); ?></a></h2>

		<?php do_action( 'bp_before_group_header_meta' ); ?>

		<div id="item-meta">

			<?php bp_group_description(); ?>
			<?php vlc_view('shop'); ?>
			<?php vlc_buttons('shop'); ?>
			<div id="item-buttons">

				<?php do_action( 'bp_group_header_actions' ); ?>

			</div><!-- #item-buttons -->

			<?php do_action( 'bp_group_header_meta' ); ?>

		</div>
	</div><!-- #item-header-content -->

</div><!-- .member-header-content -->
<div class="clr"></div>
<div id="item-actions">

	<?php if ( bp_group_is_visible() ) : ?>

		<h3><?php echo ( bp_group_name().' Team'); ?></h3>

		<?php bp_group_list_admins();

		do_action( 'bp_after_group_menu_admins' );

		if ( bp_group_has_moderators() ) :
			do_action( 'bp_before_group_menu_mods' ); ?>

			<h3><?php _e( 'Group Mods' , 'buddypress' ); ?></h3>

			<?php bp_group_list_mods();

			do_action( 'bp_after_group_menu_mods' );

		endif;

	endif; ?>

</div><!-- #item-actions -->
<div class="clr"></div>
</section><!-- .shop-header -->

<?php
do_action( 'bp_after_group_header' );
do_action( 'template_notices' );
?>