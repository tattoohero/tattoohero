<?php
 
/**
 * This is a quick and dirty class to work around the fact that bp_has_groups() does not have a
 * meta_query parameter (or even an 'in' parameter). Usage:
 *
 * 1) Just before you fire up your bp_has_groups() loop, instantiate the BP_Groups_Meta_Filter
 *    class, with parameters $key and $value
 * 2) Do your groups loop as normal
 * 3) When you've closed the bp_has_groups() loop (endif;), call the method remove_filters() just
 *    to be safe.
 *
 * EXAMPLE
 * Here's how you would run a bp_has_groups() loop that would only show groups that had the meta
 * key/value: 'favorite_gum' => 'Juicy Fruit'
 *
 *     $meta_filter = new BP_Groups_Meta_Filter( 'favorite_gum', 'Juicy Fruit' );
 *     
 *     // Note that you can pass whatever arguments you want to bp_has_groups(), as usual
 *     if ( bp_has_groups() ) :
 *         while ( bp_groups() ) :
 *             bp_the_group();
 *             // Do your template stuff here
 *         endwhile;
 *     endif;
 *
 *     // Make sure that other loops on the page are clean
 *     $meta_filter->remove_filters();
 */
class BP_Groups_Meta_Filter {
	protected $key;
	protected $value;
	protected $group_ids = array();
 
	function __construct( $key, $value ) {
		$this->key   = $key;
		$this->value = $value;
 
		$this->setup_group_ids();
 
		add_filter( 'bp_groups_get_paged_groups_sql', array( &$this, 'filter_sql' ) );
		add_filter( 'bp_groups_get_total_groups_sql', array( &$this, 'filter_sql' ) );
	}
 
	function setup_group_ids() {
		global $wpdb, $bp;
 
		$sql = $wpdb->prepare( "SELECT group_id FROM {$bp->groups->table_name_groupmeta} WHERE meta_key = %s AND meta_value = %s", $this->key, $this->value );
 
		$this->group_ids = wp_parse_id_list( $wpdb->get_col( $sql ) );
	}
 
	function get_group_ids() {
		return $this->group_ids;
 
	}
 
	function filter_sql( $sql ) {
		$group_ids = $this->get_group_ids();
		if ( empty( $group_ids ) ) {
			return $sql;
		}
 
		$sql_a = explode( 'WHERE', $sql );
 
		$new_sql = $sql_a[0] . 'WHERE g.id IN (' . implode( ',', $group_ids ) . ') AND ' . $sql_a[1];
 
		return $new_sql;
	}
 
	function remove_filters() {
		remove_filter( 'bp_groups_get_paged_groups_sql', array( &$this, 'filter_sql' ) );
		remove_filter( 'bp_groups_get_total_groups_sql', array( &$this, 'filter_sql' ) );
	}
}
 
?>