

<?php
function th_gallery_album($albumID, $size){
	//print_r('th_gallery_album()');
	$num = $size || 6;
	$images = array();
	$images = th_get_media(array('posts_per_page'=>$num, 'post_parent'=>$albumID));
	
	if(!empty($images)) th_display_gallery($images);
	//$page = get_url_var();
}

function th_header_gallery(){

	$images = array();
	$images = th_get_media(array('posts_per_page'=>6, 'author'=>bp_displayed_user_id()));
	th_display_head_gallery($images);
	//$page = get_url_var();
}
//add_action('bp_after_member_activity_content', 'th_header_gallery');

function th_user_gallery($user_id){
	wp_reset_postdata();
	$images = array();
	$images = th_get_media(array('posts_per_page'=>5, 'author'=>$user_id));
	//print_r($images);
	th_display_suggestion_gallery($images);
	//$page = get_url_var();
}
function th_popular_gallery(){

	$images = array();
	$images = th_get_media(array('meta_query' => $meta_query,'posts_per_page'=>15, 'meta_key' => 'like_count', 'orderby'=>'rand'));
	th_display_suggestion_gallery($images);
	//$page = get_url_var();
}

function th_gallery_profile(){
	$meta_query = array(
		array(
			'key' => 'bp_media_privacy',
			'value' => 0,
			'compare' => '>='
		)
	);
	$images = array();
	if (bp_current_component() == "groups") {
		$images = th_get_media(array('posts_per_page'=>6, 'meta_value' => "-".bp_get_group_id()));
	} else {
		$images = th_get_media(array('meta_query' => $meta_query, 'posts_per_page'=>6, 'author'=>bp_displayed_user_id()));
	}
	//$images = th_get_media(array('posts_per_page'=>6, 'author'=>bp_displayed_user_id()));
	th_display_gallery($images);
	$page = get_url_var();
}

function th_gallery_all(){
	$message = "";
	$empty_message = '<br>There are no tattoos posted for your area yet. If you would like us to research your area or if you have a recommendation please email us at <a href="mailto:hello@tattoohero.com?subject=A suggestion from one of Tattoo Hero\'s beloved users&Body=Please help me find an awesome Tattoo Artist in '.get_city_name($location_pref).'. I would also like to recommend (artist name/shop name) to you guys!">hello@tattoohero.com</a>.';
	$empty_message_close = "<h2>Tattoos from users that are close by </h2>";
	$empty_message_all = "<h2>Tattoos (All)<h2>";
	$location_pref = get_location_pref();
	if ($location_pref != ""){
			$profiles_text = get_profiles_in($location_pref,"city");
			if($profiles_text == ""){
				$message = $empty_message.$empty_message_close;
				$profiles_text =  get_profiles_in(get_region_id($location_pref), "region");
				if($profiles_text == ""){
					$profiles_text = get_profiles_in(get_country_id($location_pref), "country");
				}
			}
	}
	$meta_query = array(
		array(
			'key' => 'bp_media_privacy',
			'value' => 0,
			'compare' => '='
		)
	);
	$images = array();
	
	$order = $_GET['o'];

	$search = $_GET['q'];

	if ($order == 'popular' ){
		$images = th_get_media(array('author' => $profiles_text,'meta_query' => $meta_query,'posts_per_page'=>12, 'meta_key' => 'like_count', 'orderby'=>'meta_value_num'));
		if(count($images)<=0){
			$message = $empty_message.$empty_message_all;
			$images = th_get_media(array('meta_query' => $meta_query,'posts_per_page'=>12, 'meta_key' => 'like_count', 'orderby'=>'meta_value_num'));
		}
		$popActive = 'active';
	} else if ($order == 'most_viewed'){
		$images = th_get_media(array('author' => $profiles_text,'meta_query' => $meta_query,'posts_per_page'=>12, 'meta_key' => 'view_count', 'orderby'=>'meta_value_num'));
		if(count($images)<=0){
			$message = $empty_message.$empty_message_all;
			$images = th_get_media(array('meta_query' => $meta_query,'posts_per_page'=>12, 'meta_key' => 'view_count', 'orderby'=>'meta_value_num'));
		}
		$viewActive = 'active';
	} else if ($order == 'recent'){
		$images = th_get_media(array('author' => $profiles_text,'meta_query' => $meta_query, 'posts_per_page'=>12));
		if(count($images)<=0){
			$message = $empty_message.$empty_message_all;
			$images = th_get_media(array('meta_query' => $meta_query,'posts_per_page'=>12));
		}
		$browseActive = 'active';
	} else if (isset($search)) {
		$images = th_get_media(array('author' => $profiles_text,'posts_per_page'=>12, 's'=>$search));
		if(count($images)<=0){
			$message = $empty_message.$empty_message_all;
			$images = th_get_media(array('posts_per_page'=>12, 's'=>$search));
		}
	} else {
		$images = th_get_media(array('author' => $profiles_text, 'meta_query' => $meta_query,'orderby' => 'rand', 'posts_per_page'=>12));
		if(count($images)<=0){
			$message = $empty_message.$empty_message_all;
			$images = th_get_media(array('meta_query' => $meta_query,'orderby' => 'rand', 'posts_per_page'=>12));
		}
	}
	
	
	?>
	<section class="browse grid_12">
		<header>
			<h2>Browse Tattoos</h2>
		</header>

		<nav class="browse-nav">
			<ul>
				<li><a href="<?php print(home_url()."/browse/?o=recent"); ?>" class="<?php print($browseActive)?>">Newest</a></li>
				
				<li><a href="<?php print(home_url()."/browse/?o=most_viewed"); ?>" class="<?php print($viewActive)?>">Most Viewed</a></li>
				<li><a href="<?php print(home_url()."/browse/?o=popular"); ?>" class="<?php print($popActive)?>">Popular</a></li>
			</ul>
		</nav>

		<div class="search">
			<div id="tattoo-search" class="tattoo-search" role="search">
				<form action="<?php print(home_url()."/browse/"); ?>" method="post" class="search">
		    		<input type="text" name="q" size="27"/>
		    		<button class="btn" type="submit">Search Tattoos</button>
				</form>
			</div>
		</div><!-- .search -->
		<div class="clr"></div>

		<?php if (isset($search)){?>
			<p>Search results for: "<?php print($search)?>" </p>
		<?php } else if($location_pref != ""){
			echo '<h2>Tattoos from '.get_city_name($location_pref).' </h2>';
		}
			echo $message;
		?>

		
			<?php th_display_gallery($images); ?>
			

	</section>
	<?php
}
add_action('th_browse', 'th_gallery_all');

function th_gallery_front(){
	$meta_query = array(
		array(
			'key' => 'bp_media_privacy',
			'value' => 0,
			'compare' => '='
		)
	);
	$images = array();
	
	$images = th_get_media(array('meta_query' => $meta_query,'posts_per_page'=>12));
	$browseActive = 'active';
	
	$search = $_GET['q'];
	
	?>
	<section class="browse grid_12">
		<header>
			<h2>Tattoos</h2>
		</header>

		<nav class="browse-nav">
			<ul>
				<li><a href="<?php print(home_url()."/browse/2?o=recent"); ?>" class="active">Newest</a></li>				
				<li><a href="<?php print(home_url()."/browse/?o=most_viewed"); ?>" >Most Viewed</a></li>
				<li><a href="<?php print(home_url()."/browse/?o=popular"); ?>" >Popular</a></li>
			</ul>
		</nav>

		<div class="search">
			<div id="tattoo-search" class="tattoo-search" role="search">
				<form action="<?php print(home_url()."/browse/"); ?>" method="post" class="search">
		    		<input type="text" name="q" size="27"/>
		    		<button class="btn" type="submit">Search Tattoos</button>
				</form>
			</div>
		</div><!-- .search -->
		<div class="clr"></div>
		<?php th_display_gallery($images); ?>
	</section>
	<?php
}
add_action('th_browse_front', 'th_gallery_front');


function search_redirect(){
		if (isset($_POST['q'])){
		$url = "?q=".$_POST['q'];
		//$url = home_url();
		wp_redirect( home_url().'/browse?q='.$_POST['q']);
		//bp_core_redirect(get_option('siteurl')."/register");
		exit();
		
	}
}
add_action('bp_head','search_redirect');


function th_display_gallery($images){
	//print_r('th_display_gallery($images)');
	
		/*   JS is implemented through functions.php->th_load_scripts() */
	?>
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
 	<!--<script src="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/vlc.js"></script>-->
 	<?php $i = 0; ?>
	
	<div class="inner_container verticalSpacer">
		<ul class="browse-list">
		<?php
		foreach ($images as $image) :
			$k++;
			
			$artist = get_user_by('id',$image['artist_id']); ?>
			<li class="col_1of3 media-<?=$image['post_id'];?>">
				<a href="<?=$image['permalink'];?>">
					<?= wp_get_attachment_image($image['post_id'], array('class'=>"attachment-thumbnail lazy" )); ?>
				</a>
				<div class="details">
					<div class="title"><a href="<?=$image['permalink'];?>"><?=$image['title']?></a></div>
					<div class="artist"><a href="<?=TH_HOMEPAGE;?>/members/<?=$artist->user_login;?>"><?=$artist->display_name;?></a></div>
					<div class="clr"></div>
					<div class="actionbar-image">
						<div class="view left"><?=($image['views'])? $image['views'] : 0;?> <span class="icon icon-eye"></span></div>
						<div class="likes left"><span class="like-count"><?=($image['likes'])? $image['likes'] : 0;?></span> <span class="icon icon-heart <?=($image['current_user_likes'])? 'liked' : '';?>"></span></div>
						<div class="right">
						<?php if(is_user_logged_in()){if ($image['current_user_likes'] != 'liked') { ?>
						<?php 
							echo '<a href="#" data-params="'.wp_nonce_url( $_SERVER['REQUEST_URI'], 'bp_example_send_high_five' ).'&vlc_type=like&ips_type=image&ips_id='.$image['post_id'].'" data-class="media-'.$image['post_id'].'" data-url="'.TH_HOMEPAGE.'" class="set-like btn btn-mini" title="Like">Like</a>';
							//----------wp_nonce_url breaks character coding---------------//	
							//printf( '<a href="#" data-params="%s" data-class="media-'.$image['post_id'].'" data-url="'.TH_HOMEPAGE.'" class="set-like btn btn-mini" title="Like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$image['post_id'], 'bp_example_send_high_five' ) ); 
							?>
						<?php }} ?>
						<?php if (current_user_can('level_10')) { ?>
							<a href="<?php echo add_query_arg(array('a' => 'hide', 'postid' => $image['post_id'])); ?>" class="btn btn-mini" name="hide">Hide</a>
						<?php } ?>
						</div>
						<div class="clr"></div>
					</div><!-- .actionbar-image -->
				</div>
			</li>
			<?php if(($k % 3) == 0) { echo '<div class="clr"></div>'; } endforeach;?>
		</ul>
		<div class="clr"></div>
		<?php 
		//print_r($tpl_name);
		if(!is_front_page()):
			if(pagination('prev', 'next') != ''): ?>
		<div class="browse-pagination"><?php pagination('prev', 'next'); ?></div>
		<?php endif;
		else: ?>
		<div class="more"><a class="btn" href="<?php echo TH_HOMEPAGE.'/browse/2?o=recent'; ?>">Show more</a></div>
		<? endif; ?>
	</div><!-- .inner-container -->
<?php
}


function th_display_head_gallery($images){	
	//$i = 0;
	?>
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
 	<script src="<?=TH_TEMPLATE_DIR_SCRIPTS;?>/vlc.js"></script> 
 	<?php
	$i = 0;
	?>

	<div class="inner_container verticalSpacer">
			<ul class="browse-list">
	<?php
	foreach ($images as $image) :
		$k++;
	?>
		
			<li class="col_1of3 media-<?=$image['post_id'];?>">
				<a href="<?=$image['permalink'];?>">
					<?= wp_get_attachment_image($image['post_id']); ?>
				</a>
				<div class="details">
					<div class="clr"></div>
					<div class="actionbar-image">
						<div class="view left"><?=($image['views'])? $image['views'] : 0;?> <span class="icon icon-eye"></span></div>
						<div class="likes left"><span class="like-count"><?=($image['likes'])? $image['likes'] : 0;?></span> <span class="icon icon-heart <?=($image['current_user_likes'])? 'liked' : '';?>"></span></div>
						<div class="right">
						<?php if ($image['current_user_likes'] != 'liked') { ?>
						<?php 
							echo '<a href="#" data-params="'.wp_nonce_url( $_SERVER['REQUEST_URI'], 'bp_example_send_high_five' ).'&vlc_type=like&ips_type=image&ips_id='.$image['post_id'].'" data-class="media-'.$image['post_id'].'" data-url="'.TH_HOMEPAGE.'" class="set-like btn btn-mini" title="Like">Like</a>';
							//----------wp_nonce_url breaks character coding---------------//	
							//printf( '<a href="#" data-params="%s" data-class="media-'.$image['post_id'].'" data-url="'.TH_HOMEPAGE.'" class="set-like btn btn-mini" title="Like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$image['post_id'], 'bp_example_send_high_five' ) ); 
							?>
						<?php } ?>
						<?php if (current_user_can('level_10')) { ?>
							<a href="<?php echo add_query_arg(array('a' => 'hide', 'postid' => $image['post_id'])); ?>" name="hide">Hide</a>
						<?php } ?>
						</div>
						<div class="clr"></div>
					</div><!-- .actionbar-image -->
				</div>
			</li>
			<?php if(($k % 3) == 0) { echo '<div class="clr"></div>'; } endforeach;?>
			</ul>
			<div class="clr"></div>
			<?php if($k > 0) { ?>
			<div class="more"><a class="btn" href="<?php echo $_SERVER["REQUEST_URI"].'media/'; ?>">Show more</a></div>
			<?php } ?>
		</div><!-- .inner-container -->
	<?php


}
function th_display_suggestion_gallery($images){
	$k = 0;
	?>

	<div class="inner_container verticalSpacer">
			<ul class="browse-list">
	<?php
	foreach ($images as $image) :
		$k++;
	?>
		
			<li class="col_1of5 media-<?=$image['post_id'];?>">
				<a href="<?=$image['permalink'];?>">
					<?= wp_get_attachment_image($image['post_id']); ?>
				</a>
			</li>
			<?php if(($k % 5) == 0) { echo '<div class="clr"></div>'; } endforeach;?>
			</ul>
			<div class="clr"></div>
		</div><!-- .inner-container -->
	<?php

}


function browse_screen(){
	?>
	<section class="browse grid_12">
		<header>
			<h2>Browse Tattoos</h2>
		</header>

		<nav class="browse-nav">
			<ul>
				<li><a href="#" class="active">Most Viewed</a></li>
				<li><a href="#">Most Popular artist/shop</a></li>
				<li><a href="#">Recent Tattoos</a></li>
			</ul>
		</nav>
		<div class="inner_container">
			<ul class="browse-list">
	<?php
	$args = array(
		'owner_id' =>false,
		'id' => false,
		'post_type' => 'attachment',
		'post_status' => 'any',
		'post_mime_type' => false,
		'meta_query' => false,
		'posts_per_page' => -1
	);
	$media_query = new WP_Query($args);
	$i=0;
	while ($media_query->have_posts()) {
		$i++;
        $media_query->the_post();
       
        ?>
      		<li class="col_1of3 media-<?php the_ID();?>">
						<a href="<?=TH_HOMEPAGE;?>/?p=<?=the_ID();?>">
							<?= wp_get_attachment_image($media_query->post->ID,'thumbnail'); ?>
						</a>
						<div class="details">
							<div class="title"><a href="#"><?php echo(the_title())?></a></div>
							<div class="artist"><a href="#"><?php echo(the_title())?></a></div>
							<p><?php printf( '<a href="%s" title="Like">Like2</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$media_query->post->ID, 'bp_example_send_high_five' ) ) ?></p>
							<p><?php printf( '<a href="#" data-params="%s" data-url="'.TH_HOMEPAGE.'" class="set-like" title="Like">Like</a>', wp_nonce_url( $_SERVER['REQUEST_URI'].'?vlc_type=like&ips_type=image&ips_id='.$media_query->post->ID, 'bp_example_send_high_five' ) ) ?></p>
							<div class="clr"></div>
							<div class="views">11,382 views</div>
						</div>
				</li>
		<?php if(($i % 3) == 0) { echo '<div class="clr"></div>'; }
		
        /*<p><?php echo(the_title().'/'.the_attachment_link().'/'.the_meta().'/'.wp_link) ?></p> */
    }
    ?>
    		</ul>
		</div>
	</section>
    <?php

	/*$args = array();

	$args['owner_id'] = false;	// Do *not* change this
	$args['id'] = false;		// Do *not* change this
	$args['page']=1;		// Do *not* change this
	$args['max']=false;		// Do *not* change this
	$args['privacy']='public';	// Do *not* change this
	$args['priv_override']=false;	// Do *not* change this
	$args['groupby']=false;	// Do *not* change this
	$args['post_type']='attachment';

	$args['per_page']=24;
	$args['ordersort']='DESC';
	$args['orderkey']='id';	// You can also use 'random' to shuffle images, but this will slow down your site
	$images_per_row = 5;*/
}

// add_action('th_browse','browse_screen');
/*
 * th_get_media
 */
function th_get_media($args = array()) {
	global $media_query, $th_query_details;
	
	$current_user_likes_array = 	maybe_unserialize(get_user_meta(bp_loggedin_user_id(), 'image_like', true));
	$current_user_collects_array = 	maybe_unserialize(get_user_meta(bp_loggedin_user_id(), 'image_collect', true));
	$temp = 						array();
	$th_query_details = 			array();
	
	$x=0;
	
	$defaults = array(
		'owner_id' 	=>false,
		'id' 		=>false,
		'author'	=>false,

		'paged'		=> get_page_no(),
		'posts_per_page' => -1,

		'post_type' => 'attachment',
		'post_status' => 'any',
		'post_mime_type' => false,
		'meta_key' 	=> 'bp-media-key'
	);

	$args = 			wp_parse_args($args, $defaults);
	$media_query = 		new WP_Query($args);
	$th_query_details['paged'] = $args['paged'];
	$th_query_details['max_num_pages'] = $media_query->max_num_pages;

	while ($media_query->have_posts()) {
        $media_query->the_post();

    	$artist_id = 	get_post_meta($media_query->post->ID,'artist',true); 
    	$likes = 		get_post_meta($media_query->post->ID, 'like_count', true);
    	$views = 		get_post_meta($media_query->post->ID, 'view_count', true);
    	$collects = 	get_post_meta($media_query->post->ID, 'collect_count', true);

  		$temp[$x]['post_id'] = 		$media_query->post->ID;
		$temp[$x]['src'] = 			$media_query->post->guid;
		$temp[$x]['title'] = 		$media_query->post->post_title;
		$temp[$x]['description'] = 	$media_query->post->post_content;
		$temp[$x]['author_id'] = 	$media_query->post->post_author;
		$temp[$x]['artist_id'] = 	$artist_id;
		$temp[$x]['likes'] = 		$likes;// user_meta likes
		$temp[$x]['views'] = 		$views; // user_meta views 
		$temp[$x]['collects'] = 	$collects; // user_meta collects 
		$temp[$x]['current_user_likes'] = 	 in_array( $media_query->post->ID, (array)$current_user_likes_array );
		$temp[$x]['current_user_collects'] = in_array( $media_query->post->ID, (array)$current_user_collects_array );
		$temp[$x]['permalink'] = 	get_permalink();
		
		$x++;
	}
	
	wp_reset_postdata();
		// foreach ($media_query->post as $key=>$value){
		// 	echo "$key, ";
		// }
// 	if($args['format'] == 'json') {
// 		return json_encode($temp);
// 	}
	return $temp;
}
function pagination($prev = '«', $next = '»') {
    global $media_query, $wp_rewrite, $th_query_details;
    $th_query_details['paged'] > 1 ? $current = $th_query_details['paged'] : $current = 1;
    $pagination = array(
        //'base' => $_SERVER['REQUEST_URI']."?"."page=%#%",
        'base' => add_query_arg( 'page', '%#%' ),
        'format' => '',
        'total' => $th_query_details['max_num_pages'],
        'current' => $current,
        'prev_text' => __($prev),
        'next_text' => __($next),
        'type' => 'plain'
	);
    if( $wp_rewrite->using_permalinks() )
        //$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

    if( !empty($th_query_details->query_vars['s']) )
        $pagination['add_args'] = array( 's' => get_query_var( 's' ) );
    //$pagination['base'] = $pagination['base']."?".get_url_variables();

    echo paginate_links( $pagination );
};
function get_page_no()  
{  
    $strURL = $_SERVER['REQUEST_URI'];  
    $arrVals = split("/",$strURL);  
    if($arrVals[count($arrVals)-2]<>'attachment'){
    	$page = split("\?", $arrVals[count($arrVals)-1]);
    }else{
    	$page[0] = 0;
    }
    
    if ($page[0] != '') {
    	return $page[0];
    } else {
    	return $_GET['page'];
    }
    //return $_GET['page'];

}  
function get_url_variables($part){
	$strURL = $_SERVER['REQUEST_URI'];  
    $strVals = split("\?",$strURL);
    return $strVals[1];
}

?>