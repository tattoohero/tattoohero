$_ = jQuery.noConflict();

var th = {
	init: function() {
		// silence
		th.sitecalls();
		th.flexslider(); //main feature slider
		th.registration(); //handles terms&conditions pop up
		th.likes();
		th.uploadHandler();
		th.imageLazyLoading();
		//th.albumOptions();
		th.profileOptions();//in profiledetails.php
		th.socialLoginOverride();
		th.scripts_url = window.location +'/scripts';
	},
	sitecalls: function() {
		$_('#fb-login').on('click',function(){
			th.fbLogin();
		});
		$_(document).scroll(function(){
			if ($_(document).scrollTop() > 40 ) {
				$_('#header').addClass('pinned');
				$_('body').addClass('pinned');
			} else {
				$_('#header').removeClass('pinned');
				$_('body').removeClass('pinned');
			}
		});
		/*$_('.btn-navbar').on('click',function(e){
			$_('.main-nav').toggleClass('open');
			e.preventDefault();
		});
		*/
		setTimeout(function(){ $_('.shop .logo').show().addClass('animated fadeInDown'); },500);
		setTimeout(function(){ $_('.shop .newsletter_signup_group').show().addClass('animated fadeInUp'); }, 1500);
		setTimeout(function(){ $_('.shop .social-bar').show().addClass('animated fadeInUp'); }, 1500);

		$_('#message').addClass('grid_12 alert alert-info omega alpha');
		
		
		/* select dropdown arrow  */
		
		$shopField = 	$_('div.shop');
		$newShopField = $_('<div class="editfield shop hide"><div class="field_content"><label for="up-shop0">Shop Name</label><input id="up-shop0" type="text" name="0[shop]" placeholder="Shop Name"><input type="hidden" id="up-shop-id0" name="0[shop_id]" value="" /></div><div class="description">Please enter your shop\'s name, if we already know about it you should be able to select it from the dropdown list</div></div>');
		$memberType=	$_('#member-type');	
			
		//Member Type
		$_('#field_9').chosen({ 
			disable_search_threshold: 10,
			width: "100%"
		})
		.on('change', function(evt, params){
			//console.log(evt);
			//console.log(params);
			if(params.selected == "Tattoo Artist" || params.selected == "Tattoo Artist (Apprentice)"){
				//console.log(params.selected);
				if($_('.field_member-type').children().length < 3){ //do we have a form field yet?
					$shopField = $newShopField;
					
					$_('.field_member-type').append($shopField);
				}
				
				if(!$shopField.is(':visible')){
					$shopField.slideToggle(function(){ $_(this).css('overflow', 'visible')});
					//$shopField.find('#up-shop0').prop('required', 'true');
				}
			}
			else if($shopField.children().length && $shopField.is(':visible')) {
				$shopField.slideToggle();
				//$shopField.find('#up-shop0').removeAttr('required');
			}
			
			//update description info based on member type selected
			$childIndex = $_('#field_9').find(':selected').index() - 1;
			$memberType.children('div.selected').fadeOut( function(){ $memberType.children(':eq(' + $childIndex + ')').addClass('selected').fadeIn() }).removeClass('selected');
		})
		.on('chosen:ready', function(evt, params){ //page refresh handling
			$memberTypeSel = $_(this).find(':selected').val();
			
			if(($memberTypeSel == "Tattoo Artist" || $memberTypeSel == "Tattoo Artist (Apprentice)") 
				&& !$shopField.is(':visible')){
				$shopField.show(function(){ $_(this).css('overflow', 'visible')});
			}
		}).trigger('chosen:ready');
		
		
		//Shop selection
		//$addBtn = $_('<div class="chosen-add btn btn-primary">add</div>');
		//$addBtn.hide();
				
		$selShop = $_('select.selectShop');
		
		var addClick = false;
		$selShop.chosen({
			disable_search_threshold: 0,
			no_results_text: "We couldn't find that shop. If it is new click 'ADD'.",
			search_contains: true,
			placeholder_text_single: "Choose a shop",
			width: "100%"
		}).on('chosen:no_results', function(evt, params){
			//console.log('no_res');
			$search = $_(params.chosen.container[0]).find('div.chosen-search');
			
			//did we already include an 'ADD' button?
			if($search.children().length == 1){
				$addBtn = $_('<div class="chosen-add btn btn-primary">add</div>');
				$addBtn.hide();
				$search.append($addBtn);
				$addBtn = $search.find('.chosen-add');
				$addBtn.on('click', buttonClick); //click function handler below
			}
			
			$search.find('input[type="text"]').stop().animate({width: '75%'}, 500, function(){ $addBtn.fadeIn()});
			
			
		}).on('chosen:no_results_clear', function(evt, params){
			if(addClick) {
				addClick = false;
				return;
			}
			
			//console.log('no_res_clear');
			$addBtn.stop().fadeOut(function(){ 
				$search.stop().find('input[type="text"]').animate({width: '100%'}); 
			});
		});		
		
		//add new shop button click
		buttonClick = function(){
			addClick = true;
			$searchInput = $search.find('input[type="text"]');
			$_(this).hide(); //hide the add button 
			$searchInput.stop().animate({width: '100%', 'max-width': '100%'}, 100, function(){
				$selShop.prepend('<option selected>'+$searchInput.val()+'</option>');
//				$selShop.val('');				
				$searchInput.next().remove();
				$selShop.trigger('chosen:updated');	
				$selShop.find(':selected').trigger('click');//('chosen:close');
//				$selShop.trigger('chosen:close');	
				
			}); 
		}
		
		
		//standard
		$_('select').chosen();
		
		/* end of select dropdown arrow */
	},
	flexslider: function(){
		if($_('.flexslider').length){
			console.log('flexslider found');
			$_('.flexslider').flexslider({
		    controlNav: "thumbnails",
				start: function(slider){
					$_('.contest_form input').focus(function(){slider.pause()});
				}
			});
		}
	},
	likes: function () {
		$_('.set-like').on('click',function(e){
			//console.log('like clicked');
			e.preventDefault();
			var a = $_(this).data('params').split('?');
			var params = a[1];
			
			var that = $_(this);
			var el = $_(this).data('class');
			//check if user logged in
			/*console.log(a[0]);
			console.log(params);*/
			
			$_.ajax({
				url: 'http://tattoohero.com/wp-content/themes/tattoohero/th_ajax.php',
				data: params,
				dataType: 'json',
				type: 'GET',
				success: function(data) {
					if(data.success) {
						that.remove();
						$_('.'+el+' .like-count').html(data.count);
						$_('.'+el+' .icon-heart').addClass('liked');
					}
				},
				error: function(e) {
					console.log('like clicked, but ajax call failed.');
				}
			});
		});
	},
	profileOptions: function(){
		var loc = window.location.pathname;		
		//console.log('selectionDescription() ');
		if(loc == "/profileDetails" || loc == "/profiledetails" ){
		
			//MOVED TO APP.JS -> SITECALLS
		}
		else if(loc == "/profileDetails3" || loc == "/profiledetails3" ){
		
			/*	---------------------------------------------------------------- */
			/*	------------------------ Invite Emails ------------------------- */
			/*	---------------------------------------------------------------- */
			$sendMore 	= $_('#send_more');
			$emails		= $_('.emails');
			$numEmailBoxes = $emails.children('.col_1of2').length;
			$tabIndex	= 10;
			
			$sendMore.click(function(e){
				//console.log($numEmailBoxes);
				$emailBox = $_('<div class="col_1of2 hide"><input id="name" type="text" name="invite[name]"  tabindex="1" placeholder="Name" /><input id="email" type="text" name="invite[email]" tabindex="2" placeholder="Email" /></div>');
				
				e.stopImmediatePropagation();
				e.preventDefault();
				
				$tabIndex 		+= 1;
				
				$emailBox.attr('id',$numEmailBoxes)
						 .find('input#name').attr('id', 'name'+$numEmailBoxes).attr('name', 'invite'+$numEmailBoxes+'[name]').attr('tabindex', $tabIndex);
				$tabIndex += 1;
				$emailBox.find('input#email').attr('id', 'email'+$numEmailBoxes).attr('name', 'invite'+$numEmailBoxes+'[email]').attr('tabindex', $tabIndex);
				$emails.append($emailBox);
				$emails.find('#'+$numEmailBoxes).slideToggle(function(){ $_(this).removeClass('hide'); });	
				
				$numEmailBoxes 	+= 1;
			});
		}
	},
	registration: function () {
		$regForm = jQuery('#signup_form');
		
		//handle error boxes
		$regForm.find('input').click(function(){
			jQuery(this).prev('.error').fadeOut(400, function(){ jQuery(this).remove()});
		});
		
		//terms and conditions & privacy box
		$termsBox = 		$_('div#terms');
		$termsVisible =  	false;
		$termsClose = 		$termsBox.find('#close');
		
		$privacyBox = 		$_('div#privacy');
		$privacyVisible =	false;
		$privacyClose = 	$privacyBox.find('#close');
		
		$pageSpacer = 		$_('div.spacer'); //lets us figure out if in mobile mode or not
		
		jQuery('div.notice a.termsLink').click(function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			
			if(!$termsVisible){
				if($privacyVisible){
					$pageSpacer.css('background-image') == 'none' ? $privacyBox.slideToggle(1000) : $privacyBox.fadeOut(300); 
					$privacyVisible = false;
				}
				$pageSpacer.css('background-image') == 'none' ? $termsBox.slideToggle(1000, function(){ scrollAfter("terms") }) : $termsBox.fadeIn(500, function(){ scrollAfter("terms") });  
				$termsVisible = true; 
			}
		});
		
		jQuery('div.notice a.privacyLink').click(function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			
			if(!$privacyVisible){
				if($termsVisible){
					$pageSpacer.css('background-image') == 'none' ? $termsBox.slideToggle(1000) : $termsBox.fadeOut(300); 
					$termsVisible = false;
				}
				$pageSpacer.css('background-image') == 'none' ? $privacyBox.slideToggle(1000, function(){ scrollAfter("privacy") }) : $privacyBox.fadeIn(500, function(){ scrollAfter("privacy") });  
				$privacyVisible = true; 
			}
		});
		
		
		$termsClose.click(function(e){ 
			e.stopImmediatePropagation(); 
			e.preventDefault();
			
			//check if in mobile mode or not
			$pageSpacer.css('background-image') == 'none' ? $termsBox.slideToggle(2000) : $termsBox.fadeOut(300);
			
			$termsVisible = false;
		});
		
		
		$privacyClose.click(function(e){ 
			e.stopImmediatePropagation(); 
			e.preventDefault();
			
			//check if in mobile mode or not
			$pageSpacer.css('background-image') == 'none' ? $privacyBox.slideToggle(2000) : $privacyBox.fadeOut(300);
			
			$privacyVisible = false;
		});
		
		scrollAfter = function($type){
			if($type = 'terms')
				 jQuery("html, body").animate({ scrollTop: $termsBox.offset().top }, "slow");
			else jQuery("html, body").animate({ scrollTop: $privacyBox.offset().top }, "slow");

			//jQuery("html, body").animate({ scrollTop: $termsBox.offset().top }, "slow");
		}
	},
	socialLoginOverride: function(){
		/*$_('#social_login_frame').children('.provider').each(function(){
			$_(this).find('div').hide();
		});
		*/
		//iframe = document.getElementsByTagName('iframe');

	    //WaitForIFrame();
	    $_('#social_login_frame').find('iframe').load(function(){
		   alert('loaded!');
	    });
	    
	
			
	},
	uploadHandler: function(){
		//format edit/delete button on user->albums screen
		//$_('.album-edit').children('a').addClass('btn');
		
		//unbind existing handler
		$_('#bp-media-upload-ui').unbind('dragover').unbind('dragleave');
		
		//rebind handler
		$_('#drag-drop-area').bind('dragover', function(e){
            $_(this).addClass('hover');return 0;
        });
        $_('#drag-drop-area').bind('dragleave drop', function(e){
            $_(this).removeClass('hover');return 0;
        });
        
        $_('.newAlbum').hide();
        $_('#btn-create-new').addClass('btn-primary');	
	},	
	imageLazyLoading: function(){
		$_('img.lazy').lazyload({ threshold: -10, effect: "fadeIn"});/*.each(function(){
		 $currentImg = $_(this);
		 maxHeight = parseInt($currentImg.parent().css('max-height'));
		 width = parseInt($currentImg.css('width'));
		 ratio = $currentImg.attr('height')/$currentImg.attr('width');
		 height = width * ratio;
		 
		 if(height < maxHeight){
			 height = maxHeight;
			 width = maxHeight/ratio;
		 }
		 
		 $currentImg.lazyload({ threshold : -10, effect: "fadeIn"});
		});*/
	},
	albumOptions: function(){
		$_('.album').each(function($) {
			//this does not work with multiple albums???????
			/*$artistOptions 	= $('div.artist_options#<?php echo $randID; ?>');
			$hideBox  		= $artistOptions.find('div.hide');
			$selArtist		= $hideBox.find('input#selectableArtist');
			$singleArtist	= $artistOptions.find("#singleArtist");
			$sameArtist		= $('#same_artist_label<?php echo $randID; ?>');
			*/
				
			/* Activate artist options */
			$(this).find('#same_artist_label').click(function(){
				$(this).next().slideToggle();
			});
			
			//Artist options: selecting the artist name
			function artistSelect(item, options){
				if($(item).is(':checked')){
					name = $(item).val();
					if (name.length > 0){
						$('div.artist_options').parent().find(".artists").each(function(){
							$item = $(this);
							$item.find('input[type=text]').val(name);
							if(typeof options != 'undefined'){
								$item.find('input[type=hidden]').val( options.value );
								$item.find('.artistDescription').html( options.desc );
							}
						});
					}
				}
			}; 
			
			$('div.artist_options').find('div.hide').children(':radio').each(function(){
				$(this).click(function(event, data){ //jquery CANNOT be in no conflict mode
					($(this).attr('id') == "selectableArtist") ? artistSelect(this, data) : artistSelect(this);
				})
			});
			
			//text field click
			$('div.artist_options').find("#singleArtist").click(function(){
				//activte radio button
				$('div.artist_options').find('input#selectableArtist').prop("checked", true);
			})
			//catch key input
			.keyup(function(e){
				// get keycode of current keypress event
				var code = (e.keyCode ? e.keyCode : e.which);
	    
				// do nothing if it's an arrow key
			    if(code == 37 || code == 38 || code == 39 || code == 40) {
			        return;
			    }
				artistName = $( this ).val();
				if (artistName.length >= 3)
					$('div.artist_options').find('input#selectableArtist').val(artistName).trigger('click');
			}).keyup();
			
			
			$('div.artist_options').find("#singleArtist").autocomplete({
				minLength: 0,
				source: projects,
				focus: function( event, ui ) {
					$( this ).val( ui.item.label );
					$('div.artist_options').find('input#selectableArtist').prop("checked", true);
					
					return false;
				},
				select: function( event, ui ) {
					$('div.artist_options').find('input#selectableArtist').val( ui.item.label )
						.trigger('click', {value : ui.item.value, desc : ui.item.desc});
					return false;
				}
			})
			
			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			  	return $( "<li>" )
				    .append( "<a>" + item.label + "<br>" + item.desc + "</a>")
				    .appendTo( ul );
			};		
		});
	}	
}


function WaitForIFrame() {
    if (iframe.readyState != "complete") {
        setTimeout("WaitForIFrame();", 200);
    } else {
        done();
    }
}

function done() {
	$_('.oneall_social_login').find('iframe').children('.provider').hide();
    //some code after iframe has been loaded
    alert('done!!!!');
}


var _gaq 		= _gaq || [];
var pluginUrl 	= '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
_gaq.push(['_setAccount', 'UA-36983999-2']);
_gaq.push(['_trackPageview']);

(function(d) {
	var ga = d.createElement('script'); 
		ga.type  = 'text/javascript'; 
		ga.async = true;
		ga.src   = ('https:' == d.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

	var s = d.getElementsByTagName('script')[0]; 
		s.parentNode.insertBefore(ga, s);

	// Load the SDK Asynchronously

	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js		= d.createElement('script'); 
	js.id  = id; 
	js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	
	ref.parentNode.insertBefore(js, ref);
}(document));

// Additional JS functions here
window.fbAsyncInit = function() {
	FB.init({
		appId      : '337608409678319', // App ID
		channelUrl : '//www.thdev.ca/wp-content/themes/tattoohero/channel.html', // Channel File
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
	});
	
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
		 th.fbStartForm();
		} else if (response.status === 'not_authorized') {
		} else {}
	});
};	


/* ===================================================
 * bootstrap-transition.js v2.3.2
 * http://twitter.github.com/bootstrap/javascript.html#transitions
 * ===================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


  /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
   * ======================================================= */

  $(function () {

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }
    })()

  })

}(window.jQuery);


//!Bootstrap-button

/* ============================================================
 * bootstrap-button.js v2.3.2
 * http://twitter.github.com/bootstrap/javascript.html#buttons
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* BUTTON PUBLIC CLASS DEFINITION
  * ============================== */

  var Button = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.button.defaults, options)
  }

  Button.prototype.setState = function (state) {
    var d = 'disabled'
      , $el = this.$element
      , data = $el.data()
      , val = $el.is('input') ? 'val' : 'html'

    state = state + 'Text'
    data.resetText || $el.data('resetText', $el[val]())

    $el[val](data[state] || this.options[state])

    // push to event loop to allow forms to submit
    setTimeout(function () {
      state == 'loadingText' ?
        $el.addClass(d).attr(d, d) :
        $el.removeClass(d).removeAttr(d)
    }, 0)
  }

  Button.prototype.toggle = function () {
    var $parent = this.$element.closest('[data-toggle="buttons-radio"]')

    $parent && $parent
      .find('.active')
      .removeClass('active')

    this.$element.toggleClass('active')
  }


 /* BUTTON PLUGIN DEFINITION
  * ======================== */

  var old = $.fn.button

  $.fn.button = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('button')
        , options = typeof option == 'object' && option
      if (!data) $this.data('button', (data = new Button(this, options)))
      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.button.defaults = {
    loadingText: 'loading...'
  }

  $.fn.button.Constructor = Button


 /* BUTTON NO CONFLICT
  * ================== */

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


 /* BUTTON DATA-API
  * =============== */

  $(document).on('click.button.data-api', '[data-toggle^=button]', function (e) {
    var $btn = $(e.target)
    if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
    $btn.button('toggle')
    console.log('btn clicked')
  })

}(window.jQuery);


//!Bootstrap-collapse

/* =============================================================
 * bootstrap-collapse.js v2.3.2
 * http://twitter.github.com/bootstrap/javascript.html#collapse
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* COLLAPSE PUBLIC CLASS DEFINITION
  * ================================ */

  var Collapse = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.collapse.defaults, options)

    if (this.options.parent) {
      this.$parent = $(this.options.parent)
    }

    this.options.toggle && this.toggle()
  }

  Collapse.prototype = {

    constructor: Collapse

  , dimension: function () {
      var hasWidth = this.$element.hasClass('width')
      return hasWidth ? 'width' : 'height'
    }

  , show: function () {
      var dimension
        , scroll
        , actives
        , hasData

      if (this.transitioning || this.$element.hasClass('in')) return

      dimension = this.dimension()
      scroll = $.camelCase(['scroll', dimension].join('-'))
      actives = this.$parent && this.$parent.find('> .accordion-group > .in')

      if (actives && actives.length) {
        hasData = actives.data('collapse')
        if (hasData && hasData.transitioning) return
        actives.collapse('hide')
        hasData || actives.data('collapse', null)
      }

      this.$element[dimension](0)
      this.transition('addClass', $.Event('show'), 'shown')
      $.support.transition && this.$element[dimension](this.$element[0][scroll])
    }

  , hide: function () {
      var dimension
      if (this.transitioning || !this.$element.hasClass('in')) return
      dimension = this.dimension()
      this.reset(this.$element[dimension]())
      this.transition('removeClass', $.Event('hide'), 'hidden')
      this.$element[dimension](0)
    }

  , reset: function (size) {
      var dimension = this.dimension()

      this.$element
        .removeClass('collapse')
        [dimension](size || 'auto')
        [0].offsetWidth

      this.$element[size !== null ? 'addClass' : 'removeClass']('collapse')

      return this
    }

  , transition: function (method, startEvent, completeEvent) {
      var that = this
        , complete = function () {
            if (startEvent.type == 'show') that.reset()
            that.transitioning = 0
            that.$element.trigger(completeEvent)
          }

      this.$element.trigger(startEvent)

      if (startEvent.isDefaultPrevented()) return

      this.transitioning = 1

      this.$element[method]('in')

      $.support.transition && this.$element.hasClass('collapse') ?
        this.$element.one($.support.transition.end, complete) :
        complete()
    }

  , toggle: function () {
      this[this.$element.hasClass('in') ? 'hide' : 'show']()
    }

  }


 /* COLLAPSE PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.collapse

  $.fn.collapse = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('collapse')
        , options = $.extend({}, $.fn.collapse.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.collapse.defaults = {
    toggle: true
  }

  $.fn.collapse.Constructor = Collapse


 /* COLLAPSE NO CONFLICT
  * ==================== */

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


 /* COLLAPSE DATA-API
  * ================= */

  $(document).on('click.collapse.data-api', '[data-toggle=collapse]', function (e) {
    var $this = $(this), href
      , target = $this.attr('data-target')
        || e.preventDefault()
        || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
      , option = $(target).data('collapse') ? 'toggle' : $this.data()
    $this[$(target).hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
    $(target).collapse(option)
  })

}(window.jQuery);


//!Bootstrap-dropdown

/* ============================================================
 * bootstrap-dropdown.js v2.3.2
 * http://twitter.github.com/bootstrap/javascript.html#dropdowns
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle=dropdown]'
    , Dropdown = function (element) {
        var $el = $(element).on('click.dropdown.data-api', this.toggle)
        $('html').on('click.dropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  Dropdown.prototype = {

    constructor: Dropdown

  , toggle: function (e) {
      var $this = $(this)
        , $parent
        , isActive

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      clearMenus()

      if (!isActive) {
        if ('ontouchstart' in document.documentElement) {
          // if mobile we we use a backdrop because click events don't delegate
          $('<div class="dropdown-backdrop"/>').insertBefore($(this)).on('click', clearMenus)
        }
        $parent.toggleClass('open')
      }

      $this.focus()

      return false
    }

  , keydown: function (e) {
      var $this
        , $items
        , $active
        , $parent
        , isActive
        , index

      if (!/(38|40|27)/.test(e.keyCode)) return

      $this = $(this)

      e.preventDefault()
      e.stopPropagation()

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      if (!isActive || (isActive && e.keyCode == 27)) {
        if (e.which == 27) $parent.find(toggle).focus()
        return $this.click()
      }

      $items = $('[role=menu] li:not(.divider):visible a', $parent)

      if (!$items.length) return

      index = $items.index($items.filter(':focus'))

      if (e.keyCode == 38 && index > 0) index--                                        // up
      if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
      if (!~index) index = 0

      $items
        .eq(index)
        .focus()
    }

  }

  function clearMenus() {
    $('.dropdown-backdrop').remove()
    $(toggle).each(function () {
      getParent($(this)).removeClass('open')
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = selector && $(selector)

    if (!$parent || !$parent.length) $parent = $this.parent()

    return $parent
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  var old = $.fn.dropdown

  $.fn.dropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('dropdown')
      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


 /* DROPDOWN NO CONFLICT
  * ==================== */

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(document)
    .on('click.dropdown.data-api', clearMenus)
    .on('click.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.dropdown.data-api'  , toggle, Dropdown.prototype.toggle)
    .on('keydown.dropdown.data-api', toggle + ', [role=menu]' , Dropdown.prototype.keydown)

}(window.jQuery);


$_(document).ready(function(){
	th.init();
});