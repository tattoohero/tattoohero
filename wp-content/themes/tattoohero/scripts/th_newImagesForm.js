function ajax_th_new_images_form(album){
	var data = {
        action: 'th_new_images_form',
        albumId: album
    };
    
    
    jQuery.post(thAjax.ajaxurl,data,function(response){
    	var responseTest = parseInt(response);
        if(responseTest == 0){
            alert('Sorry something went wrong.');
        }
        else {
        	//check if album container exists (in case something went wrong and it was removed)
        	var $albumContainer = jQuery(".albumContainer");
   
        	$resAlbum = jQuery("#result"+album);
        	
        	//does an album of this name already exist? Override it with updates
        	if($resAlbum.length > 0) $resAlbum.html(response); 
        	else{ 
        		//create album container
        		var $newAlbum = jQuery("<div>",{ id: "result"+album, class: "album"});
        		
        		$albumContainer.prepend($newAlbum); 
        		
        		$newAlbum.html(response);  
        		
        		//updared $resAlbum reference
        		$resAlbum = $newAlbum; 
        	}
        	
        	if(jQuery('.loaderBox').length) 
        		jQuery('.loaderBox').css({'height': $albumContainer.outerHeight(), 'width':jQuery('.loaderBox').outerWidth()});
        	
        	//$resAlbum = jQuery("#result".album);
        	jQuery('html, body').animate({ scrollTop: $albumContainer.offset().top - 75}, 500, scrollCompleted(album, $resAlbum));
        }
    });
}

function scrollCompleted($ID, $album){
	$aID = '#'+$ID;
	//console.log($aID);
	
	$nextButton = jQuery('#details_submit');
	$nextButton.click(function(){
		jQuery($aID).find('input[type=submit]').hide().trigger('click');
	});	
	
	//$albumContHeight = jQuery('.albumContainer').outerHeight();
    
	
	//animate loading box, then remove ajax loader graphic
	jQuery('.ajaxLoader').delay(200).fadeOut(400, function(){
		//jQuery('.loaderBox').animate({height: jQuery('.albumContainer').outerHeight() }, 1000, function(){
			jQuery('.loaderBox').delay(100).fadeOut(200);
			$album.delay(200).animate({opacity: '1'});
			$_('img.lazy').lazyload({ threshold : -10, effect: "fadeIn"});/*.each(function(){
			 $currentImg = $_(this);
			 maxHeight = $currentImg.parent().css('max-height');
			 width = parseInt($currentImg.css('width'));
			 ratio = $currentImg.attr('height')/$currentImg.attr('width');
			 height = width * ratio;
			 
			 //console.log("height: "+height+", width: "+width);
			 
			 if(height < width && height < maxHeight){
				 height = maxHeight;
				 width = maxHeight/ratio;
			 }
			 
			 $currentImg.lazyload({ event: 'scrollstop', threshold : -10, effect: "fadeIn"});
			});*/
			
			/* Activate artist options */
			/*$_('#same_artist_label').click(function(){
				$_(this).next().slideToggle();
			});*/
		//});
	});
}