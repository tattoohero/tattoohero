<?php

function th_load_scripts(){
	wp_register_script('jquery-ui', 'http://code.jquery.com/ui/1.10.4/jquery-ui.js');
	wp_register_script( 'vlc', get_template_directory_uri() . '/scripts/vlc.js', array('jquery'));
	wp_register_script( 'location', get_template_directory_uri() . '/scripts/location.js', array('jquery','jquery-ui'));

	wp_enqueue_script('jquery-ui');
	wp_enqueue_script('location');
	wp_enqueue_script('vlc');
}
add_action('wp_enqueue_scripts', 'th_load_scripts');
?>