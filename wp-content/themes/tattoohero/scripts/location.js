

var l = jQuery.noConflict();
//console.log(window.location.protocol + '://' + window.location.host + '/wp-content/themes/tattoohero/th_ajax.php');
//var field_id = "#th_location";
//var field_id = "#field_3";
var location_search = {
	list: function(search_field, id_field) {
		$search = l(search_field);
		$searchVal = l(search_field).val();
			
	    $search.autocomplete({
	        //minLength: 0,
	        source: function( request, response ) {
	        	l.ajax({
					//url: 'http://steve.tattoohero.com/wp-content/themes/tattoohero/th_ajax.php',
					url: window.location.protocol + '//' + window.location.host + '/wp-content/themes/tattoohero/th_ajax.php',
					//url: thAjax.ajaxurl, 
					data: 'th_action=location_search&search_string=' + l(search_field).val(),
					dataType: 'json',
					type: 'GET',
					success: function(result) {
						//console.log(result[0]);
						 response(l.map( result, function(item ) {
							return {
								label: item[0],
								value: item[1]
							}
						}));
					},
					error: function(e, status, error) {
						console.log('location failed:' + e.responseText + error);
					}
				});
	        },
	        focus: function( event, ui ) {
	            l( search_field ).val( ui.item.label );
	            return false;
	        },
	        select: function( event, ui ) {
	           l(id_field).val( ui.item.value );
	           l(search_field).val(ui.item.label);
	           l(this).parent().submit();
	        //   // l( "#up-artist-id<?php echo ($id); ?>" ).val( ui.item.value );
	        //   // l( "#up-artist-description<?php echo ($id); ?>" ).html( ui.item.desc );
			
				return false;
			},
		 	minLength: 3,
		 	open: function(){ l('ul.ui-autocomplete').width(l(search_field).outerWidth()); } 
	    })

    	// .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	    //   return l( "<li>" )
	    //     .append( "<a>" + item.label + "<br>" + item.desc + "</a>")
	    //     .appendTo( ul );
    	// };
	
	}

};
l(document).ready(function(){
	console.log('start location script....');
	location_search.list("#field_20","#field_19"); //buddypress profile fields
	location_search.list("#th_location","#th_location_id");
	location_search.list("#group-field-one", "#shop_location_id");
	location_search.list("#group-city","#group-city-id");
	location_search.list("#city","#city-id");
	console.log('location script loaded');
});