<?php 
if (!function_exists('add_action')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit("This page can't be loaded outside of WordPress!"); }
define('TH_HOMEPAGE', get_option("home"));
define('TH_HOMEPAGE_FR', "http://www.congrespcc.ca");
define('TH_TEMPLATE_DIR', get_bloginfo('template_directory'));
define('TH_TEMPLATE_DIR_SCRIPTS', TH_TEMPLATE_DIR.'/scripts');
define('TH_TEMPLATE_DIR_IMAGES', TH_TEMPLATE_DIR.'/images');


class Main_Nav_Menu extends Walker_Nav_Menu {
	private $curItem;

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) { 
	    if ( !$element )
	            return;
	
	    $id_field = $this->db_fields['id'];
	
	    //display this element
	    if ( is_array( $args[0] ) )
	            $args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
	   
	    elseif ( is_object($args[0]) )
        	$args[0]->has_children =  ! empty( $children_elements[$element->$id_field] );
	    
	    $cb_args = array_merge( array(&$output, $element, $depth), $args);
	   
	    call_user_func_array(array(&$this, 'start_el'), $cb_args);
	
	    $id = $element->$id_field;
	
	    // descend only when the depth is right and there are childrens for this element
	    if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {
	
	            foreach( $children_elements[ $id ] as $child ){
	
	                    if ( !isset($newlevel) ) {
	                            $newlevel = true;
	                            //start the child delimiter
	                            $cb_args = array_merge( array(&$output, $depth), $args);
	                            call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
	                    }
	                    $this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
	            }
	            unset( $children_elements[ $id ] );
	    }
	
	    if ( isset($newlevel) && $newlevel ){
	            //end the child delimiter
	            $cb_args = array_merge( array(&$output, $depth), $args);
	            call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
	    }
	
	    //end this element
	    $cb_args = array_merge( array(&$output, $element, $depth), $args);
	    call_user_func_array(array(&$this, 'end_el'), $cb_args);
	}
	
	function start_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		$output .= $indent.'<ul class="sub-menu dropdown-menu-'.$this->curItem->ID.'">';
	}
  
	function start_el(&$output, $item, $depth, $args)
	{
		$this->curItem = $item;
		$has_children = (is_object($args) && $args->has_children) || (is_array($args) &&  $args['has_children']);
	    $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;
	
	    $class_names = join(
	        ' '
	    ,   apply_filters(
	            'nav_menu_css_class'
	        ,   array_filter( $classes ), $item
	        )
	    );
	
	    ! empty ( $class_names )
	        and $class_names = ' class="'. esc_attr( $class_names ).'"';
	
	    
	    $output .= "<li id='menu-item-$item->ID' $class_names >";
	    
	    $attributes  = '';
	
	    ! empty( $item->attr_title )
	        and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
	    ! empty( $item->target )
	        and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
	    ! empty( $item->xfn )
	        and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
	    !empty( $item->url )
	        and $attributes .= ' href="';//   . esc_attr( $item->url        ) .'"';#"';//
	    
	    $attributes .= ($has_children) ? '#" data-toggle="collapse" data-target=".dropdown-menu-'.$item->ID.'"' : esc_attr( $item->url ).'"';//data-toggle="dropdown" data-target="#"';  
	    
	    // insert description for top level elements only
	    // you may change this
	    $description = ( ! empty ( $item->description ) and 0 == $depth )
	        ? '<small class="nav_desc">' . esc_attr( $item->description ) . '</small>' : '';
	
	    $title = apply_filters( 'the_title', $item->title, $item->ID );
	
	    $item_output = $args->before
	        . "<a $attributes>"
	        . $args->link_before
	        . $title
	        . '</a>';
	        
	        if($has_children) $item_output .= '<b class="caret"></b>';
	        
	        $item_output .= $args->link_after
	        . $description
	        . $args->after;

	    // Since $output is called by reference we don't need to return anything.
	    $output .= apply_filters(
	        'walker_nav_menu_start_el'
	    ,   $item_output
	    ,   $item
	    ,   $depth
	    ,   $args
	    );
	}
}

class Footer_Nav_Menu extends Walker_Nav_Menu {
	private $curItem;

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) { 
	    if ( !$element )
	            return;
	
	    $id_field = $this->db_fields['id'];
	
	    //display this element
	    if ( is_array( $args[0] ) )
	            $args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
	   
	    elseif ( is_object($args[0]) )
        	$args[0]->has_children =  ! empty( $children_elements[$element->$id_field] );
	    
	    $cb_args = array_merge( array(&$output, $element, $depth), $args);
	   
	    call_user_func_array(array(&$this, 'start_el'), $cb_args);
	
	    $id = $element->$id_field;
	
	    // descend only when the depth is right and there are childrens for this element
	    if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {
	
	            foreach( $children_elements[ $id ] as $child ){
	
	                    if ( !isset($newlevel) ) {
	                            $newlevel = true;
	                            //start the child delimiter
	                            $cb_args = array_merge( array(&$output, $depth), $args);
	                            call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
	                    }
	                    $this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
	            }
	            unset( $children_elements[ $id ] );
	    }
	
	    if ( isset($newlevel) && $newlevel ){
	            //end the child delimiter
	            $cb_args = array_merge( array(&$output, $depth), $args);
	            call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
	    }
	
	    //end this element
	    $cb_args = array_merge( array(&$output, $element, $depth), $args);
	    call_user_func_array(array(&$this, 'end_el'), $cb_args);
	}
	
	function start_lvl(&$output, $depth=0, $args) {
		$has_children = (is_object($args) && $args->has_children) || (is_array($args) &&  $args['has_children']);
		if( 0 == $depth && !$has_children ) return;
		$indent = str_repeat("\t", $depth);
		$output .= $indent.'<ul class="sub-menu dropdown-menu-'.$this->curItem->ID.'">';
	}
  
	function start_el(&$output, $item, $depth, $args)
	{
		$this->curItem = $item;
		$has_children = (is_object($args) && $args->has_children) || (is_array($args) &&  $args['has_children']);
	    
	    if($depth == 0 && $has_children) return;
		
	    
	    $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;
	
	    $class_names = join(
	        ' '
	    ,   apply_filters(
	            'nav_menu_css_class'
	        ,   array_filter( $classes ), $item
	        )
	    );
	
	    ! empty ( $class_names )
	        and $class_names = ' class="'. esc_attr( $class_names ).'"';
	
	    $output .= "<";
	    
	    $output .= ($has_children) ? "div" : (($depth != 0) ? "li" : "p");
	    
		$output .= " id='menu-item-$item->ID' $class_names >";
	    
	    $attributes  = '';
	
	    ! empty( $item->attr_title )
	        and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
	    ! empty( $item->target )
	        and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
	    ! empty( $item->xfn )
	        and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
	    !empty( $item->url )
	        and $attributes .= ' href="';//   . esc_attr( $item->url        ) .'"';#"';//
	    
	    $attributes .= ($has_children) ? '#" data-toggle="collapse" data-target=".dropdown-menu-'.$item->ID.'"' : esc_attr( $item->url ).'"';//data-toggle="dropdown" data-target="#"';  
	    
	    // insert description for top level elements only
	    // you may change this
	    $description = ( ! empty ( $item->description ) and 0 == $depth )
	        ? '<small class="nav_desc">' . esc_attr( $item->description ) . '</small>' : '';
	
	    $title = apply_filters( 'the_title', $item->title, $item->ID );
	
	    $item_output = $args->before;
	    
	        $item_output .= "<a $attributes>"
	        . $args->link_before
	        . $title
	        . '</a>';
	    
	        $item_output .= $args->link_after
	        . $description
	        . $args->after;

	    // Since $output is called by reference we don't need to return anything.
	    $output .= apply_filters(
	        'walker_nav_menu_start_el'
	    ,   $item_output
	    ,   $item
	    ,   $depth
	    ,   $args
	    );
	}
	
	function end_lvl(&$output, $depth=0, $args) {
	    $output .= "</ul>";
	}
}

//add categorization to media
function wptp_add_categories_to_attachments() { 
	register_taxonomy_for_object_type( 'category', 'attachment' ); 
} 
add_action( 'init' , 'wptp_add_categories_to_attachments' ); 


//!LOAD SCRIPTS AND STYLES
function th_load_scripts(){
    wp_enqueue_script( 'modernizr',		TH_TEMPLATE_DIR_SCRIPTS . '/modernizr.js', array('jquery'), 0, 1);
    wp_enqueue_script( 'oauthpopup', 	TH_TEMPLATE_DIR_SCRIPTS . '/oauthpopup.js', array('jquery'), 0, 1);
    wp_enqueue_script( 'scrollstop', 	TH_TEMPLATE_DIR_SCRIPTS . '/jquery.scrollstop.js', array('jquery'), 0, 1);
    
    /* -- Included in app.js -- */
    //wp_enqueue_script( 'bootstrap', 	TH_TEMPLATE_DIR_SCRIPTS .  '/bootstrap.min.js', array('jquery'), 0, 1);
    //wp_enqueue_script( 'bootstrap-button', TH_TEMPLATE_DIR_SCRIPTS .  '/bootstrap-button.js', array('bootstrap'), 0, 1);
    
    wp_register_script('jquery-ui', 	'http://code.jquery.com/ui/1.10.4/jquery-ui.js', array('jquery'),0,1); //dependecy and enqueued through app.js
    wp_register_script( 'lazyload', 	TH_TEMPLATE_DIR_SCRIPTS . '/jquery.lazyload.min.js', array('jquery'), 0, 1);
    wp_register_script( 'chosen', 		TH_TEMPLATE_DIR_SCRIPTS . '/chosen.jquery.js', array('jquery'), 0, 1);
    
	/* Views/Likes/Comments - TODO: should be limited to the pages that require it */
    wp_register_script( 'vlc', 			TH_TEMPLATE_DIR_SCRIPTS . '/vlc.js', array('jquery'), 0, 1);
    wp_register_script( 'location', 	TH_TEMPLATE_DIR_SCRIPTS . '/location.js', array('jquery', 'jquery-ui' ), 0 , 1);
    
    wp_enqueue_script( 'newImagesForm', TH_TEMPLATE_DIR_SCRIPTS . '/th_newImagesForm.js', array('jquery', 'jquery-ui' ), 0, 1);
	wp_localize_script('newImagesForm', 'thAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
	 
	 
	$dependencies = array( 'jquery', 'jquery-ui', 'lazyload', 'chosen', 'location', 'vlc');
	
	if ( is_home() || is_front_page() ){
		wp_register_script('flexslider',TH_TEMPLATE_DIR_SCRIPTS . '/flexslider/jquery.flexslider-min.js', array('jquery'), 0, 1);
		
		array_push($dependencies, 'flexslider');
	}
	
    wp_enqueue_script( 'app',			TH_TEMPLATE_DIR_SCRIPTS . '/app.js', $dependencies, 0, 1);
    
    /* STYLES */
    wp_enqueue_style( 'th_core', TH_TEMPLATE_DIR . '/style.css', false);
	wp_enqueue_style( 'jquery_ui_css', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', false);
	wp_enqueue_style( 'flexslider_css', TH_TEMPLATE_DIR_SCRIPTS . '/flexslider/flexslider.css',  false);
}
add_action('wp_enqueue_scripts', 'th_load_scripts');


/*function ajaxNewImagesEnquerer(){
	wp_register_script( "newImagesForm", TH_TEMPLATE_DIR_SCRIPTS.'/th_newImagesForm.js', array('jquery') );
	wp_localize_script( 'newImagesForm', 'thAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
	//wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'newImagesForm' );
}*/
//add_action( 'init', 'ajaxNewImagesEnquerer' );


/**
 * bfg_bp_core_fetch_avatar()
 *
 * Modifieds the BuddyPress avatar with WP Social Login avatar if default avatar is rendered
 *
 *
 * @param string $bp_avatar Avatar image returned by BuddyPress
 * @param array $args Determine the output of this function
 * @return string Formatted HTML <img> element, or raw avatar URL based on $html arg
 */

function bfg_bp_core_fetch_avatar($bp_avatar, $args = '') {

 	$params = wp_parse_args( $args, array(
		'item_id'    => false,
		'object'     => $def_object, // user/group/blog/custom type (if you use filters)
		'type'       => $def_type,   // thumb or full
		'avatar_dir' => false,       // Specify a custom avatar directory for your object
		'width'      => false,       // Custom width (int)
		'height'     => false,       // Custom height (int)
		'class'      => $def_class,  // Custom <img> class (string)
		'css_id'     => false,       // Custom <img> ID (string)
		'alt'        => '',    	     // Custom <img> alt (string)
		'email'      => false,       // Pass the user email (for gravatar) to prevent querying the DB for it
		'no_grav'    => false,       // If there is no avatar found, return false instead of a grav?
		'html'       => true,        // Wrap the return img URL in <img />
		'title'      => ''           // Custom <img> title (string)
	) );
	extract( $params, EXTR_SKIP );
	//modify only user's avatars and
	if(!empty ($bp_avatar) && $object == 'user' && get_option('wsl_settings_users_avatars')) {

		if(empty($email) && !empty($item_id)) {
			$email = get_userdata($item_id)->user_email;
		}

		if($item_id || $email) {

			if(is_default_gravatar($email)) {
				$user_thumbnail = get_user_meta ($item_id, 'wsl_user_image', true);

			  if ($user_thumbnail !== false && strlen(trim($user_thumbnail)) > 0) {
          $user_thumbnail = preg_replace ('#src=([\'"])([^\\1]+)\\1#Ui', "src=\\1" . $user_thumbnail . "\\1", $bp_avatar);
          if (!is_ssl()) {
						$user_thumbnail = str_replace('https', 'http', $user_thumbnail);
					}
					return $user_thumbnail;
        }
			}

		}
	}

	return $bp_avatar;
}
//add_filter( 'bp_core_fetch_avatar', 'bfg_bp_core_fetch_avatar', 15, 2 );



//given user e-mail tests if gravatar exits or a default image is rendered
//returns true if default image is going to be used, false if a valid gravatar image exists
function is_default_gravatar($email) {
	// Craft a potential url and test its headers
	$hash = md5(strtolower(trim($email)));
	$uri = 'http://www.gravatar.com/avatar/' . $hash . '?d=404';
	$headers = @get_headers($uri);
	return !preg_match("|200|", $headers[0]);

}

function upload_new_avatar(){
	global $bp;
	
	
	if (!isset( $bp->avatar_admin )) $bp->avatar_admin = new stdClass();
	
	//reset every time so we still see that option if nothing else was done	
	$bp->avatar_admin->step = 'upload-image';
	
	if ( !empty( $_FILES )) {
		// Check the nonce
		check_admin_referer( 'bp_avatar_upload' );

		// Pass the file to the avatar upload handler
		if ( bp_core_avatar_handle_upload( $_FILES, 'xprofile_avatar_upload_dir' ) ) {
			$bp->avatar_admin->step = 'crop-image';

			// Make sure we include the jQuery jCrop file for image cropping
			wp_enqueue_style( 'jcrop');
			wp_enqueue_script( 'jcrop', array( 'jquery' ));
	
			add_action( 'wp_footer', 'bp_core_add_cropper_inline_js' );
			add_action( 'wp_footer', 'bp_core_add_cropper_inline_css' );			
		}		
	}

	// If the image cropping is done, crop the image and save a full/thumb version
	
	if ( isset( $_POST['avatar-crop-submit'] ) ) {
		// Check the nonce
		check_admin_referer( 'bp_avatar_cropstore' );
	
		if ( !bp_core_avatar_handle_crop( array( 'item_id' => bp_displayed_user_id(), 'original_file' => $_POST['image_src'], 'crop_x' => $_POST['x'], 'crop_y' => $_POST['y'], 'crop_w' => $_POST['wi'], 'crop_h' => $_POST['h'] ) ) ){
			bp_core_add_message( __( 'There was a problem cropping your avatar, please try uploading it again', 'buddypress' ), 'error' );
		}else {
			bp_core_add_message( __( 'Your new avatar was uploaded successfully!', 'buddypress' ) );
			//reset upload button in case user wants to change avatar
		}
	}	

	/*called in profile-details.php*/
	//locate_template( array( 'members/single/profile/change-avatar.php' ), false );
}




// remove WordPress Social Login's get_avatar filter so that we can add our own
//remove_filter( 'get_avatar', 'wsl_user_custom_avatar' );
function my_user_custom_avatar($avatar, $id_or_email, $size, $default, $alt) {
    global $comment;

    if( get_option ('wsl_settings_users_avatars') && !empty ($avatar)) {
        //Check if we are in a comment
        if (!is_null ($comment) && !empty ($comment->user_id)) {
                $user_id = $comment->user_id;
        } elseif(!empty ($id_or_email)) {
            if ( is_numeric($id_or_email) ) {
	            $user_id = (int) $id_or_email;
            } elseif ( is_string( $id_or_email ) && ( $user = get_user_by( 'email', $id_or_email ) ) ) {
	            $user_id = $user->ID;
            } elseif ( is_object( $id_or_email ) && ! empty( $id_or_email->user_id ) ) {
	            $user_id = (int) $id_or_email->user_id;
            }
        }
        // Get the thumbnail provided by WordPress Social Login
        if ($user_id) {
            if (($user_thumbnail = get_user_meta ($user_id, 'wsl_user_image', true)) !== false) {
                if (strlen (trim ($user_thumbnail)) > 0) {
                    $user_thumbnail = preg_replace ('#src=([\'"])([^\\1]+)\\1#Ui', "src=\\1" . $user_thumbnail . "\\1", $avatar);
                    return $user_thumbnail;
                }
            }
        }
    }
    // No avatar found.  Return unfiltered.
    return $avatar;
}

//add_filter ( 'get_avatar', 'my_user_custom_avatar', 10, 5);





//redirect on activation
function bp_autologin_on_activation($user_id,$key,$user) {
	global $bp, $wpdb;
		
    //simulate Bp activation
	/* Check for an uploaded avatar and move that to the correct user folder, just do what bp does */
	if ( is_multisite() )
		$hashed_key = wp_hash( $key );
	else
		$hashed_key = wp_hash( $user_id );

	/* Check if the avatar folder exists. If it does, move rename it, move it and delete the signup avatar dir */
	if ( file_exists( BP_AVATAR_UPLOAD_PATH . '/avatars/signups/' . $hashed_key ) )
		@rename( BP_AVATAR_UPLOAD_PATH . '/avatars/signups/' . $hashed_key, BP_AVATAR_UPLOAD_PATH . '/avatars/' . $user_id );

	bp_core_add_message( __( 'Your account is now active!', 'buddypress' ) );

	$bp->activation_complete = true;

	//now login and redirect
	wp_set_auth_cookie($user_id,true,false);
	//bp_core_redirect(apply_filters ("bp_loggedin_register_page_redirect_to",bp_core_get_user_domain($user_id),'profiledetails'));
	bp_core_redirect( TH_HOMEPAGE . '/profiledetails' );       
}
add_action("bp_core_activated_user","bp_autologin_on_activation",40,3);


/* buddypress captcha fix */
function bp_reg_captcha() {
  global $bp;
  if( function_exists( 'cptch_display_captcha_custom' ) ) {
	echo '<label>Human Verification (required)</label>';
	if (!empty($bp->signup->errors['captcha_response_field'])) {
	  echo '<div class="error">';
	  echo "Please complete the verification.";
	  echo '</div>';
	}
	echo "<input type='hidden' name='cntctfrm_contact_action' value='true' />";
	echo "<div class=\"captcha\">";
	echo cptch_display_captcha_custom();
	echo "</div>";
  }
}

function bp_reg_captcha_validate($errors) {
	global $bp;
	if(function_exists( 'cptch_check_custom_form' ) && cptch_check_custom_form() !== true ) {
	  $bp->signup->errors['captcha_response_field'] = "Error.";
	}
	return;
}

add_action( 'bp_before_registration_submit_buttons', 'bp_reg_captcha' );
add_action( 'bp_signup_validate', 'bp_reg_captcha_validate' );

//!ADD USER PROFILE DETAILS STEP META
function th_init_user_details_step( $signup ) {
	update_usermeta( $signup['user_id'], 'profileDetailsStep', 0 );
	return $signup;
}
add_filter( 'bp_core_activate_account', 'bp_user_activate_field' );


//!PROFILE-DETAILS CHECK
function check_profile_form(){
	global $bp, $TH;
	
	if(isset($_POST['profileDetails_action'])){
		
		//redirecting if logged out
		if(!is_user_logged_in()) return wp_login_url();
		
		//check what step we were one (in case we browse back a step we still want to have the most recent one)
		$currentProgress = $TH->checkProfileDetailsProgress();
		
		if($_POST['profileDetails_action'] == 'step1') {
			
			//shop field
			$shopFieldRequired = false;
			$shop = "";
				
			if(wp_verify_nonce($_POST['profileDetails_nonce'], 'details-nonce')) {
				
				// No errors
				$errors = false;
				
				//check fields
				if ( isset( $_POST['field_ids'] ) ) {
					
					//this should never happen!
					if ( empty( $_POST['field_ids'] ) ){
						$errors = true;
						th_errors()->add('Form', __('REALLY BAD! There seems to be no content at all. Make sure to fill the required boxes before moving on.', 'tattoohero'));
						return;
					}
					
					// Explode the posted field IDs into an array so we know which
					// fields have been submitted
					$posted_field_ids = explode( ',', $_POST['field_ids'] );
					$is_required      = array();
			
					// Loop through the posted fields formatting any datebox values
					// then validate the field
					foreach ( (array) $posted_field_ids as $field_id ) {
						if ( !isset( $_POST['field_' . $field_id] ) ) {
			
							if ( !empty( $_POST['field_' . $field_id . '_day'] ) && !empty( $_POST['field_' . $field_id . '_month'] ) && !empty( $_POST['field_' . $field_id . '_year'] ) ) {
								// Concatenate the values
								$date_value =   $_POST['field_' . $field_id . '_day'] . ' ' . $_POST['field_' . $field_id . '_month'] . ' ' . $_POST['field_' . $field_id . '_year'];
			
								// Turn the concatenated value into a timestamp
								$_POST['field_' . $field_id] = date( 'Y-m-d H:i:s', strtotime( $date_value ) );
							}
						}
			
						$is_required[$field_id] = xprofile_check_is_required_field( $field_id );
						if ( $is_required[$field_id] && empty( $_POST['field_' . $field_id] ) ) { 
							$errors = true; 
							
							$field = new BP_XProfile_Field( $field_id );
							$field_name = $field->name;
							
							th_errors()->add($field_name, $value);				
						}
						
						//check for shop requirement
						//th_errors()->add($field_id, $_POST['field_'.$field_id]);		
						//$errors = true;		
						if($field_id == '9' && ($_POST['field_'.$field_id] == "Tattoo Artist" || $_POST['field_'.$field_id] == "Tattoo Artist (Apprentice)")){
							$shopFieldRequired = true;
							//is shop field empty?
							//printArray($_POST);
							if(empty($_POST['shop'])){
							 	th_errors()->add('shop name', 'empty');		
							 	$errors = true;		
							 }
							 else $shop = $_POST['shop'];
						}
					}
				}
	
				//terms and conditions -- DEPRECATED
				/*if(!$_POST['terms_and_conditions'] == 'checked') { 
					$errors = true; 
					th_errors()->add('Accept Terms', __('We are still bound on legal grounds so you need to agree to our legal terms', 'tattoohero'), 'terms');
				}*/				
					
				//ALL GOOD, MOVING ON: No errors
				if ( empty( $errors ) ) {
					// Reset the errors var
					$errors = false;
					
					// Now we've checked for required fields, lets save the values.
					foreach ( (array) $posted_field_ids as $field_id ) {
	
						$field = new BP_XProfile_Field( $field_id );
						$field_name = $field->name;
						th_errors()->add('ERROR', $field_name, $value);
							
						// Certain types of fields (checkboxes, multiselects) may come through empty. Save them as an empty array so that they don't get overwritten by the default on the next edit.
						if ( empty( $_POST['field_' . $field_id] ) )	$value = array();
						else $value = $_POST['field_' . $field_id];
						
						if ( !xprofile_set_field_data( $field_id, $_POST['userID'], $value, $is_required[$field_id] ) )
							$errors = true;
						else do_action( 'xprofile_profile_field_data_updated', $field_id, $value );
		
						// Save the visibility level
						$visibility_level = !empty( $_POST['field_' . $field_id . '_visibility'] ) ? $_POST['field_' . $field_id . '_visibility'] : 'public';
						xprofile_set_field_visibility_level( $field_id, $_POST['userID'], $visibility_level );
					}
		
					// Set the feedback messages
					if ( !empty( $errors ) ){
						bp_core_add_message( __( 'There was a problem cropping your avatar, please try uploading it again', 'buddypress' ), 'error' );
						th_errors()->add('ERROR', __('There was a problem updating some of your profile information, please try again.', 'tattoohero'),'');
					}
					else {
						bp_core_add_message( __( 'Changes saved.', 'tattoohero' ) );
						
						//REDIRECT
						if($shopFieldRequired && !is_numeric($shop)){
							//save store name for later use
							$_SESSION['shopName'] = $shop; 
							bp_core_redirect( trailingslashit( bp_displayed_user_domain() . '/profiledetails1-5' ) );
						}
						else{
							//add member to shop
							if($shopFieldRequired) {
								global $bp;
								
								$group = groups_get_group( array( 'group_id' => $shop) );
								//print_r($group);
								
								if ( ! groups_is_user_member( bp_loggedin_user_id(), $group->id ) ) {
									if ( 'public' == $group->status ) {
										if ( ! groups_join_group( $group->id ) ) {
											th_errors()->add('ERROR', __('There was a problem with adding you to the selected shop', 'tattoohero'),'');
											$errors = true;
										} 
									} elseif ( 'private' == $group->status ) {
										
										if ( ! groups_send_membership_request( bp_loggedin_user_id(), $group->id ) ) {
											//_e( 'Error requesting membership', 'buddypress' );
											th_errors()->add('ERROR', __('There was a problem with adding you to the selected shop', 'tattoohero'),'shopField');
											$errors = true;
										} 
									}
							
								}
							}
								
							//now redirect
							if(empty($errors)) {
								if( $currentProgress < 1) $TH->setProfileDetailsProgress(1);
								bp_core_redirect( trailingslashit( bp_displayed_user_domain() . '/profiledetails2' ) );
							}
						}
					}
				}
			}
		}
		else if($_POST['profileDetails_action'] == 'step1.5') {
			//Create Shop and REDIRECT
			//print_r(create_shop());
			if(create_shop())
				bp_core_redirect( trailingslashit( bp_displayed_user_domain() . '/profiledetails2' ) );
		}
		else if($_POST['profileDetails_action'] == 'step2') {
			if( $currentProgress < 2) $TH->setProfileDetailsProgress(2);
			
			//REDIRECT
			bp_core_redirect( trailingslashit( bp_displayed_user_domain() . '/profiledetails3' ) );
		}
		else if($_POST['profileDetails_action'] == 'step3') {
		
			if( $currentProgress < 3) $TH->setProfileDetailsProgress(3);
			// Invite Emails
			//print_r("step3 activated \n");
			
			/*echo '<pre>'; 
			print_r($_POST);
			echo '</pre>';
			*/

			$num 	= 0;
			$emailCount = 0;			
			$names 	= array();
			$emails = array();
			
			$errors = array();
			$allEmpty = true;
			
			$gift = false;
			
			if(isset($_POST['profileDetails_Emails'])){
				foreach($_POST as $item => $value){
					//echo $item."</br>";
					$nameRef 	= 'name'.($num+1);
					$emailRef 	= 'email'.($num+1);
					//echo $value.'</br>';
					
					//gift button set?
					if($item == 'gift') $gift = true;
					
					//get email data
					if (strpos($item,'invite') !== false) {
						//basic check
						if(!empty($value['name']) && empty($value['email'])){
							$allEmpty = false;
							$errors[$num] = true;
							th_errors()->add('ERROR', __('Name entered, but no email', 'tattoohero'), $emailRef);
						}
						
						if(empty($value['name']) && !empty($value['email'])){
							$allEmpty = false;
							$errors[$num] = true;
							th_errors()->add('ERROR', __('Email entered, but no name', 'tattoohero'), $nameRef);
						}
						
						//advanced check
						if(!$errors[$num] && !empty($value['name']) && !empty($value['email'])){
							$allEmpty = false; //as soon as at least one field is not empty
							
							if(strlen($value['name']) >= 2){
								/*echo "invite found: </br>";
								echo "name: ".$value['name']."</br>";
								echo "email: ".$value['email']."</br>";
								*/
								if(strlen($value['email']) > 8){
									if(check_email_address($value['email'])){
										//echo 'email is good</br>';
										// ALL GOOD, STORE INFO
										$names[$emailCount]  = $value['name'];
										$emails[$emailCount] = $value['email'];
										$emailCount++;
										
									}
									else {
										$errors[$num] = true;
										th_errors()->add('ERROR', __('Bad email address.', 'tattoohero'), $emailRef);
									}
								}
								else {
									$errors[$num] = true;
									th_errors()->add('ERROR', __('The email you entered seems too short. Remove it if you made a mistake.', 'tattoohero'),$emailRef);
								}
							}
						}
						
						//only for invite fields we increase num
						$num++;
					}
				}
			
				if($allEmpty)
					th_errors()->add('ERROR', __('You haven\'t entered anything.', 'tattoohero'),'name1');
				else if(!in_array('true',$errors)){ //No errors, moving on		
					//store in database
					global $wpdb;
					
					$user_id = bp_loggedin_user_id();
					for($i = 0; $i < $emailCount; $i++){
						if ($i == 0 && $gift) $wpdb->query($wpdb->prepare("INSERT INTO th_invite_emails (ID, invite_name, invite_email, gift) VALUES (%s,%s,%s,%s)", $user_id, $names[$i], $emails[$i], 1));
						else $wpdb->query($wpdb->prepare("INSERT INTO th_invite_emails (ID, invite_name, invite_email) VALUES (%s,%s,%s)", $user_id, $names[$i], $emails[$i]));
					}
					
					//make sure that you can only get a gift with 5 proper emails
					if($gift && $emailCount < 5){
						$gift = false;
						bp_core_add_message( __( 'Saved everything, but you haven\'t entered enough emails to receive a free gift.', 'buddypress' ), 'alert' );	
						//th_errors()->add('ALERT', __('Saved everything, but you haven\'t entered enough emails to receive a free gift.', 'tattoohero'),'name1');
					}
					else bp_core_add_message( __( 'Added your emails.', 'buddypress' ), 'alert' );
				}
			}
			
			//REDIRECT
			bp_core_redirect( TH_HOMEPAGE );
			
		}
	}
	
	/*$currPage = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
	
	if($currPage == 'profileDetails3' || $currPage == 'profiledetails3' ){
		
	}*/
}
add_action('init', 'check_profile_form');

//!REDIRECT INCOMPLETE PROFILE
function profileIncompleteRedirect(){
	global $TH;
	//auto redirect for logged in users with unfinished profile Details
	$detailsProgress = $TH->checkProfileDetailsProgress();
	
	if($detailsProgress && $detailsProgress < 3){
	 	$location = $_SERVER['REQUEST_URI'];
	 	
		if($location != "/profiledetails" && $location != "/profiledetails1-5" && $location != "/profiledetails2" && $location != "/profiledetails3"){
			bp_core_add_message( __( 'You are logged in, but please finish completing all the steps', 'buddypress' ), 'alert' );
			
			$location = '/profiledetails'. (( $detailsProgress > 1) ? $detailsProgress : '');
			bp_core_redirect( bp_displayed_user_domain() . $location  );
		}
	}
}

function create_shop(){
	global $bp;
	
	// Set the ID of the new group, if it has already been created in a previous step
	if ( isset( $_COOKIE['bp_new_group_id'] ) ) {
	    $bp->groups->new_group_id = $_COOKIE['bp_new_group_id'];
	    $bp->groups->current_group = groups_get_group( array( 'group_id' => $bp->groups->new_group_id ) );
	}
	
	$new_group_id = isset( $bp->groups->new_group_id ) ? $bp->groups->new_group_id : 0;
	
	//required fields filed out?	
	if ( empty( $_POST['group-name'] ) || empty( $_POST['group-desc'] ) || !strlen( trim( $_POST['group-name'] ) ) || !strlen( trim( $_POST['group-desc'] ) ) ) {
		bp_core_add_message( __( 'Please fill in all of the required fields', 'buddypress' ), 'error' );
		return false;
	}
	else if(!empty($_POST['group_plus_textfield_field2']) && !check_email_address($_POST['group_plus_textfield_field2'])){
		bp_core_add_message( __( 'Please enter a valid email address', 'buddypress' ), 'error' );
		return false;
	}
	
	if ( !$bp->groups->new_group_id = groups_create_group( array( 'group_id' => $new_group_id, 'name' => $_POST['group-name'], 'description' => $_POST['group-desc'], 'creator_id' => '1', 'slug' => groups_check_slug( sanitize_title( esc_attr( $_POST['group-name'] ) ) ), 'date_created' => bp_core_current_time(), 'status' => 'hidden', 'enable_forum' => 0 ) ) ) {
		bp_core_add_message( __( 'There was an error saving group details, please try again.', 'buddypress' ), 'error' );
		return false;
	}
	else{ //no errors, moving on to meta info		
		//store cookie for refresh
		setcookie( 'bp_new_group_id', $bp->groups->new_group_id, time()+60*60*24, COOKIEPATH );
		
		update_shop_info($bp->groups->new_group_id);
		bp_core_add_message( __( 'Store created and pending review.', 'buddypress' ), 'notice' );
		
		//shop avatar
		/*if ( !empty( $_FILES ) && isset( $_POST['upload'] ) ) {
			// Normally we would check a nonce here, but the group save nonce is used instead
			
			// Pass the file to the avatar upload handler
			if ( bp_core_avatar_handle_upload( $_FILES, 'groups_avatar_upload_dir' ) ) {
			  // Make sure we include the jQuery jCrop file for image cropping
			  add_action( 'wp_print_scripts', 'bp_core_add_jquery_cropper' );
			}
		}
		// If the image cropping is done, crop the image and save a full/thumb version
		if ( isset( $_POST['avatar-crop-submit'] ) && isset( $_POST['upload'] ) ) {
			// Normally we would check a nonce here, but the group save nonce is used instead			
			if ( !bp_core_avatar_handle_crop( array( 'object' => 'group', 'avatar_dir' => 'group-avatars', 'item_id' => $bp->groups->current_group->id, 'original_file' => $_POST['image_src'], 'crop_x' => $_POST['x'], 'crop_y' => $_POST['y'], 'crop_w' => $_POST['w'], 'crop_h' => $_POST['h'] ) ) )
			  bp_core_add_message( __( 'There was an error saving the group avatar, please try uploading again.', 'buddypress' ), 'error' );
			else
			  bp_core_add_message( __( 'The group avatar was uploaded successfully!', 'buddypress' ) );
		}*/
		return true;
	}
}


function update_shop_info( $group_id ) {
	global $bp, $wpdb;
	
	$errors = array();
	
	$plain_fields = array(
		'group-city',
		'group-city-id',
		'group_plus_map_field',
		'group_plus_textfield_field',
		'group_plus_textfield_field2',
		'group_plus_textfield_field3',
		'group_plus_textfield_field4',
		'group_plus_textfield_field5',
		'group_plus_textarea_field',
		'group_plus_textarea_field2',
		'group_plus_textarea_field3',
		'invite_status'
	);
	
	foreach( $plain_fields as $field ) {
		$key = $field;
		
		if ( isset( $_POST[$key] ) ) {
			$value = $_POST[$key];
			groups_update_groupmeta( $group_id, $field, $value );
		}
	}
}


function check_email_address($email) {
  // First, we check that there's one @ symbol, 
  // and that the lengths are right.
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    // Email invalid because wrong number of characters 
    // in one section or wrong number of @ symbols.
    return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
    if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
      return false;
    }
  }
  // Check if domain is IP. If not, 
  // it should be valid domain name
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
    }
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|↪([A-Za-z0-9]+))$", $domain_array[$i])) {
        return false;
      }
    }
  }
  return true;
}


function add_opengraph() {
	echo "<!-- Begin Facebook Open Graph Protocol --> \n";
	// Start with some values that don't change.
	//echo "<meta property=\"fb:admins"\ content=\"YOUR FACEBOOK USER ID"/>n"; // Insert your Facebook user ID in here
	echo "<meta property=\"fb:app_id\" content=\"171592323037608\" />\n";
	echo "<meta property=\"og:site_name\" content=\"" . get_bloginfo('name') . "\"/>\n"; // Sets the site name to the one in your WordPress settings
	echo "<meta property=\"og:description\" content=\"" . get_bloginfo('description', 'display') . "\">\n"; 
	
	$currPage = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
		
	if (is_singular()) { // If we are on a blog post/page
        echo "<meta property=\"og:type\" content=\"article\"/>\n"; // Sets the content type to be article.
    
        if($currPage == 'profileDetails3' || $currPage == 'profiledetails3' ){
		    $userid 	= bp_loggedin_user_id();
			echo "<meta property=\"og:url\" content=\"" . (($userid == '') ? TH_HOMEPAGE : bp_core_get_user_domain($userid)) . "\"/>\n"; // Gets the permalink to the post/page
			echo "<meta property=\"og:title\" content=\"" . get_bloginfo('name') . "\"/>\n"; // Gets the page title
		}
		else{
			echo "<meta property=\"og:title\" content=\"" . get_the_title() . "\"/>\n"; // Gets the page title
			echo "<meta property=\"og:url\" content=\"" . get_permalink() . "\"/>\n"; // Gets the permalink to the post/page	
		}
	}
	if(has_post_thumbnail( $post->ID )) { // If the post has a featured image.
		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo "<meta property=\"og:image\" content=\"" . esc_attr( $thumbnail[0] ) . "\"/>\n"; // If it has a featured image, then display this for Facebook
	} else {
		$main_image = TH_TEMPLATE_DIR_IMAGES."/th_logo.jpg"; // Replace this with a link to a default preview image.
		echo "<meta property=\"og:image\" content=\"" . $main_image . "\"/>\n";
	}

	echo "<!-- End Facebook Open Graph Protocol --> \n \n";
}
add_action( 'wp_head', 'add_opengraph', 5 );


function add_twittercards() {
	echo "<!-- Begin Twitter Cards --> \n";
	
	echo "<meta name=\"twitter:card\" content=\"summary\">\n";
	echo "<meta name=\"twitter:description\" content=\"" . get_bloginfo('description', 'display') . "\">\n";
	
	$currPage = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
		
	if (is_singular()) { // If we are on a blog post/page
		
        if($currPage == 'profileDetails3' || $currPage == 'profiledetails3' ){
		    $userid 	= bp_loggedin_user_id();
			echo "<meta name=\"twitter:url\" content=\"" . (($userid == '') ? TH_HOMEPAGE : bp_core_get_user_domain($userid)) . "\"/>\n"; // Gets the permalink to the post/page
			echo "<meta name=\"twitter:title\" content=\"" . get_bloginfo('name') . "\"/>\n"; // Gets the page title
		}
		else{
			echo "<meta name=\"twitter:title\" content=\"" . get_the_title() . "\"/>\n"; // Gets the page title
			echo "<meta name=\"twitter:url\" content=\"" . get_permalink() . "\"/>\n"; // Gets the permalink to the post/page		
		}
    }
	if(has_post_thumbnail( $post->ID )) { // If the post has a featured image.
		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo "<meta name=\"twitter:image\" content=\"" . esc_attr( $thumbnail[0] ) . "\"/>\n"; // If it has a featured image, then display this for Facebook
	} else {
		$main_image = TH_TEMPLATE_DIR_IMAGES."/th_logo.jpg"; // Replace this with a link to a default preview image.
		echo "<meta name=\"twitter:image\" content=\"" . $main_image . "\"/>\n";
	}

	echo "<!-- End Twitter Cards --> \n \n";
}
add_action( 'wp_head', 'add_twittercards', 5 );


function add_googlemeta() {
	echo "<!-- Begin Google Meta --> \n";
	
	$currPage = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
		
	echo "<meta itemprop=\"name\" content=\"" . get_bloginfo('name') . "\"/>\n"; // Gets the page title
	echo "<meta itemprop=\"description\" content=\"" . get_bloginfo('description', 'display') . "\">\n";
		
	if (is_singular()) { // If we are on a blog post/page
		
        if($currPage == 'profileDetails3' || $currPage == 'profiledetails3' ){
		    echo "<meta itemprop=\"title\" content=\"" . get_bloginfo('name') . "\"/>\n"; // Gets the page title
		}
		else{
			echo "<meta itemprop=\"title\" content=\"" . get_the_title() . "\"/>\n"; // Gets the page title
		}
    }
	echo "<!-- End Google Meta --> \n \n";
}
add_action( 'wp_head', 'add_googlemeta', 5 );

/**
 * Add site-specific Open Graph protocol customizations to the Facebook plugin
 *
 * @param array $meta_tags an associative array of Open Graph protocol properties with keys expressed as full IRIs
 * @return array plugin default values with site-specific customizations
 */
/*function my_ogp_filter( $meta_tags ) {
	$meta_tags['http://ogp.me/ns#image'][] = array(
		'url' => TH_TEMPLATE_DIR_IMAGES.'/th_logo.jpg',
		'type' => 'image/png',
		'width' => 300,
		'height' => 300
	);
	return $meta_tags;
}
add_filter( 'fb_meta_tags', 'my_ogp_filter' );
*/

//!TH_ERRORS
if(!function_exists('th_errors')) { 
	// used for tracking error messages
	function th_errors(){
	    static $wp_error; // Will hold global variable safely
	    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
	}
}

//check if a new shop needs to be created
function is_shop($shopName){
	global $bp;
	
	print_r('is_new_shop(), $shopName: '.$shopName);
	/*if(bp_has_groups(array('user_id' => false) )):
		while ( bp_groups() ){ 
			bp_the_group();
			if ($shopName == bp_get_group_name() ) return false;
		}
	}*/
	//print_r('true');	
	return (BP_Groups_Group::group_exists($shopName));//true;
}


function socials($atts){
	extract( shortcode_atts( array(
		"link" => ''
	), $atts ) );

	$ref = ($link == '') ? TH_HOMEPAGE : get_permalink();
	
	$socialButtons = '<ul class="sharelist">
	    <li class="share_fb">
	    	<a href="http://facebook.com/share.php?u=' . $ref . '&amp;t=' . urlencode(the_title('','', false)) . '" target="_blank">Facebook</a></li>
	    <li class="share_twitter">
	    	<a href="http://twitter.com/home?status=' . urlencode(the_title('','', false)) . $ref . '" target="_blank">Twitter</a></li>
	    <li class="share_pinterest">
	    	<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(the_title('','', false)) . '" target="_blank">Pinterest</a></li> 
	    <li class="share_gplus">
	    	<a href="https://plus.google.com/share?url=' . $ref .'" target="_blank">Google Plus</a></li> 
	    <li class="share_digg">
	    	<a href="http://digg.com/submit?phase=2&amp;url=' . $ref .'&amp;title=' . urlencode(the_title('','', false)) . '" target="_blank">Digg</a></li> 
	    <li class="share_su">
	    	<a href="http://stumbleupon.com/submit?url=' . $ref . '&amp;title=' . urlencode(the_title('','', false)) . '" target="_blank">StumbleUpon</a></li>
	    <li class="share_deli">
	    	<a href="http://del.icio.us/post?url='. $ref . '&amp;title=' . urlencode(the_title('','', false)) . '" target="_blank">Del.icio.us</a></li>
     </ul>';
     
     return $socialButtons;
}
add_shortcode('socials', 'socials');


if(!class_exists('TH')):
class TH {
	//todo: revise jquery call here
	function jquery() { if (!is_admin()) { wp_enqueue_script('jquery', 'ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js'); } }
	
	function initialize() { @$this->register_my_menu(); @$this->create_post_types(); }
	function register_my_menu() { 
		register_nav_menu('nav-menu', 	__('Navigation Menu')); 
		register_nav_menu('nav-footer', __('Navigation Menu Footer'));
		register_nav_menu('nav-base', __('Navigation Menu Base'));
	}
	function new_excerpt_more($excerpt) { return str_replace( '[...]', '...', $excerpt ); }
	function create_post_types() {
		register_post_type('th_feature', array('labels' => array('name'=>__('Feature Articles'), 'singular_name'=>__('Feature Article')), 'taxonomies'=> array('category'), 'supports' => array('title','editor','thumbnail','excerpt'), 'public'=>true, 'menu_position'=>4, 'rewrite'=>array('slug'=>'feature-articles'),'menu_icon'=>TH_TEMPLATE_DIR_IMAGES.'/featurearticles.png'));
		register_post_type('th_blog', array('labels' => array('name'=>__('Blog Posts'), 'singular_name'=>__('Blog Post')), 'taxonomies'=> array('category'), 'supports' => array('title','editor','thumbnail','excerpt', 'comments'), 'public'=>true, 'menu_position'=>4, 'rewrite'=>array('slug'=>'blog'),'menu_icon'=>TH_TEMPLATE_DIR_IMAGES.'/blog.png'));
		register_post_type('th_ads', array('labels' => array('name'=>__('Ads'), 'singular_name'=>__('Ad'), 'all_items' =>__('All Ads')), 'taxonomies'=> array('category'), 'supports' => array( 'thumbnail'), 'public'=>true, 'menu_position'=>4, 'rewrite'=>array('slug'=>'ads'),'menu_icon'=>TH_TEMPLATE_DIR_IMAGES.'/blog.png'));
		//removing
		//unset( $wp_post_types[ 'th_artists' ] );
        
		
	}

	//function scripts() { if (!is_admin()) { wp_enqueue_script('oauthpopup', TH_TEMPLATE_DIR_SCRIPTS.'/oauthpopup.js', array('jquery')); } }
	function showSocialConnects($extraClass = ""){
		$socialButtons = '<div class="th_connect '.$extraClass.'">
		    <a class="share_fb" href="https://facebook.com/tattooherocom" target="_blank">Facebook</a>
		    <a class="share_twitter" href="https://twitter.com/tattoo_hero" target="_blank">Twitter</a>
		    <a class="share_instagram" href="https://instagram.com/tattoohero" target="_blank">Instagram</a>
		    <a class="share_pinterest" href="https://pinterest.com/tattooherocom" target="_blank">Pinterest</a>
	     </div>';
	     
	     echo $socialButtons;
	}
	
	
	
	function admin_print_scripts() { wp_deregister_script('autosave'); wp_enqueue_script('media-upload'); wp_enqueue_script('thickbox'); }
	function admin_print_styles() { wp_enqueue_style('thickbox'); }

	function deregister_styles() { wp_deregister_style('bp-css'); }

	function clean_up_post($post) { $post = strip_tags($post); $post = stripslashes($post); $post = trim($post); return $post; }

	function the_excerpt($excerpt,$charlength) {
		$charlength++;
		$excerpt = trim($excerpt);
		if (strlen($excerpt) > $charlength) {
			$subex = substr($excerpt, 0, $charlength - 5);
			$exwords = explode(' ', $subex);
			$excut = - ( strlen( $exwords[ count($exwords) - 1 ]));
			if ($excut < 0) { $excerpt = substr($subex, 0, $excut); } else { $excerpt = $subex; } $excerpt .=  '...';
		}
		return $excerpt;
	}	
	
	//!CHECK PROFILE DETAILS STEP
	function checkProfileDetailsProgress(){
		//one should never be able to get here without being logged in, but just to be sure
		if(!is_user_logged_in()) return false;
		$userID = bp_loggedin_user_id();
		
		$profileDetailsStep = get_user_meta($userID, bp_get_user_meta_key('profileDetailsStep'), true);
		
		//for existing user that did not have this meta field activated yet
		if($profileDetailsStep == ''){
			update_user_meta($userID, 'profileDetailsStep', 1 );
			$profileDetailsStep = get_user_meta($userID, bp_get_user_meta_key('profileDetailsStep'), true);
		}
		
		//print_r('step: '.$profileDetailsStep);
		return $profileDetailsStep;
	}
	
	function setProfileDetailsProgress($step){
		$userID = bp_loggedin_user_id();
		update_user_meta($userID, 'profileDetailsStep', $step );
	}
	
	//!ENCODE EMAILS via http://rot13.de/
	function saveEncodeEmail($content){ ?>
		<script type="text/javascript">document.write(<?php echo urldecode($content); ?>.replace(/[a-zA-Z]/g, function(c){ return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26); }) );</script>
		<?php
	}
	
	//!MAINNAV
	function get_menu() { 
		global $post, $bp;

		$detailsProgress = @$this->checkProfileDetailsProgress();

		//don't display the nav if details not completed
		//if(@$this->checkProfileDetailsProgress() && @$this->checkProfileDetailsProgress() < 3) return; 
		
		if($detailsProgress && $detailsProgress < 3) return;
		
		$menu = '<header class="main-header">
		    <div class="container">
			    <h1 class="grid_2 logo small">
			    	<a class="white" href="http://tattoohero.com/" rel="bookmark" title="Go to Tattoo Hero frontpage"><svg xmlns="http://www.w3.org/2000/svg" id="th_medium-logo_medium" width="175" height="30" viewBox="0 0 175 30"><svg xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" height="30" viewBox="60.642 0 174.5 30" width="175" version="1.1" y="0" x="0px" enable-background="new 60.642 0 174.5 30"><path d="m87.354 4.77l-0.013-0.021c0.401-0.315 0.661-0.805 0.661-1.354 0-0.953-0.772-1.725-1.725-1.725-0.242 0-0.472 0.05-0.681 0.14l-0.011-0.02s-3.045 1.67-6.68 1.67c-0.753 0-2.685-0.032-3.405-0.032-4.943 0-8.382 2.75-8.382 6.516 0 1.678 0.83 3.324 2.544 4.318 0.348 0.242 0.769 0.385 1.225 0.385 1.187 0 2.147-0.962 2.147-2.148s-0.961-2.147-2.147-2.147c-0.314 0-0.612 0.069-0.882 0.19-0.093-0.296-0.136-0.606-0.136-0.926 0-1.801 1.997-3.568 4.911-3.568 0.785 0 2.848 0.131 3.732 0.131 1.833 0 3.732-0.458 5.271-1.276l-9.691 16.927c-1.638 2.849-3.569 4.846-6.024 4.846-3.176 0-4.257-2.75-1.932-5.107l-1.834-1.769c-4.387 3.896-1.768 9.561 3.372 9.561 3.373 0 6.549-1.899 9.43-6.876l10.216-17.715h0.034z"></path><path d="m145.44 19.274l-0.229 0.262c-1.539 1.67-3.438 2.488-5.141 2.521l3.209-5.533c2.062-3.569 0.164-6.941-3.045-6.941-2.619 0-4.649 1.048-6.613 4.453l-1.67 2.913-1.393 2.382-0.179 0.205c-1.539 1.67-3.438 2.488-5.141 2.521l3.209-5.533c2.062-3.569 0.164-6.941-3.045-6.941-2.619 0-4.649 1.048-6.613 4.453l-1.67 2.913-1.392 2.382-0.18 0.205c-2.456 2.849-4.846 5.107-6.352 5.107-0.754 0-1.179-0.523-0.394-1.866l7.302-12.736h2.783l0.981-1.637h-2.815l3.209-5.566h-0.015c0.401-0.697 0.165-1.589-0.53-1.995-0.699-0.409-1.598-0.174-2.007 0.525l-0.873 1.493h0.007l-3.196 5.543h-5.009l3.203-5.556c0.409-0.699 0.174-1.598-0.524-2.006-0.699-0.409-1.598-0.174-2.007 0.525l-0.86 1.47h-0.008l-3.209 5.566h-3.077l-0.982 1.637h3.11l-5.332 9.235c-0.001-0.001-0.003-0.002-0.005-0.002l-0.229 0.262c-2.456 2.849-4.846 5.107-6.352 5.107-0.754 0-1.18-0.523-0.394-1.866l5.501-9.626h-1.932c0.131-1.44-1.343-3.438-3.569-3.438-1.833 0-4.387 1.44-5.828 3.929l-4.78 8.251c-1.44 2.521-0.425 4.846 2.325 4.846 1.375 0 3.077-0.884 4.715-2.227 0.163 1.31 1.211 2.227 2.946 2.227 1.719 0 3.597-1.124 5.439-2.743-0.049 1.576 1.026 2.743 2.975 2.743 1.72 0 3.597-1.124 5.439-2.743-0.049 1.576 1.027 2.743 2.975 2.743 1.977 0 4.163-1.485 6.265-3.502-0.005 4.338 6.027 5.182 9.713 0.522h0.753c1.237 0 2.913-0.463 4.426-1.291-0.755 4.993 5.759 6.214 9.652 1.291h0.753c2.03 0 5.239-1.244 6.974-3.307-0.26-0.457-0.85-0.98-1.3-1.177zm-55.238 0.589c-2.063 3.241-4.093 4.78-5.501 4.78-0.753 0-1.179-0.523-0.393-1.866l4.747-8.283c1.834-3.176 5.468-2.357 3.798 0.72l-2.651 4.649zm10.578 4.781c-0.754 0-1.179-0.523-0.394-1.866l7.302-12.736h5.009l-5.332 9.235c-0.001-0.001-0.003-0.002-0.005-0.002l-0.229 0.262c-2.48 2.848-4.87 5.107-6.37 5.107zm22.56-5.141h-0.164c-1.113 0-1.997 0.917-1.997 2.03 0 0.426 0.131 0.818 0.327 1.146l-0.032 0.066c-1.604 2.75-5.501 2.226-3.471-1.311l2.587-4.485 1.637-2.782c2.39-4.061 5.664-2.456 3.602 1.08l-2.5 4.256zm14.83 0h-0.164c-1.113 0-1.997 0.917-1.997 2.03 0 0.426 0.131 0.818 0.327 1.146l-0.032 0.066c-1.604 2.75-5.501 2.226-3.471-1.311l2.587-4.485 1.637-2.782c2.39-4.061 5.664-2.456 3.602 1.08l-2.5 4.256z"></path><path d="m198.8 5.49l2.26-1.244c-0.721-2.75-6.188-5.271-10.184 1.604l-4.846 8.349h-5.107l5.469-9.43-0.016-0.025c0.455-0.31 0.756-0.832 0.756-1.425 0-0.952-0.773-1.725-1.725-1.725-0.539 0-1.021 0.248-1.338 0.637-5.885 4.201-12.992 2.729-15.686 7.417-1.713 2.991-0.49 5.323 1.203 6.623 0.303 0.363 0.725 0.624 1.205 0.724 0.094 0.042 0.186 0.082 0.277 0.118l0.045-0.078c0.039 0.002 0.078 0.006 0.119 0.006 1.186 0 2.148-0.962 2.148-2.148s-0.963-2.147-2.148-2.147c-0.227 0-0.445 0.035-0.65 0.101-0.082-0.545 0.027-1.203 0.486-1.986 1.57-2.717 7.398-2.815 11.066-4.682l-6.908 11.983c-7.859 13.652-20.693 6.385-16.273-1.212 1.375-2.357 3.766-2.979 3.766-2.979l-0.623-2.521s-3.438 0.163-5.729 4.093c-6.254 10.738 11.329 21.576 21.413 4.125l2.193-3.831h5.107l-6.09 10.576h3.438l11.264-19.514c1.9-3.275 4.13-3.635 5.11-1.409z"></path><path d="m232.43 19.274l-0.23 0.262c-1.537 1.67-3.438 2.488-5.139 2.521l3.207-5.533c2.062-3.569 0.164-6.941-3.045-6.941-2.619 0-4.648 1.048-6.613 4.453l-1.67 2.913-1.391 2.381-0.18 0.206c-2.457 2.849-4.846 5.107-6.352 5.107-0.754 0-1.18-0.523-0.395-1.866l3.406-5.959c1.178-2.062 0.621-3.273-0.492-4.616l-1.898-2.292c-0.49-0.622-0.49-1.31 0.197-1.964l-1.77-1.933c-1.932 1.735-2.094 3.667-0.883 5.239l0.295 0.393c-1.506 2.128-3.537 5.01-5.764 7.629l-0.229 0.262c-2.455 2.849-5.598 5.107-7.988 5.107-2.096 0-3.34-1.31-1.768-4.06l0.326-0.59c2.193 0.917 6.32 0.491 8.873-3.929l0.492-0.852c1.832-3.143 0.064-5.5-2.719-5.5-2.75 0-4.943 1.44-6.385 3.929l-3.568 6.123c-2.127 3.634-0.295 6.974 3.732 6.974 3.863 0 7.695-3.013 10.51-6.286 1.637-1.899 2.914-3.602 5.631-7.302l0.459 0.557c0.426 0.557 0.59 1.244 0.197 1.932l-3.602 6.254c-1.441 2.521-0.426 4.846 2.324 4.846 1.977 0 4.162-1.485 6.264-3.503-0.004 4.339 6.029 5.183 9.713 0.523h0.754c2.029 0 5.238-1.244 6.975-3.307-0.23-0.458-0.82-0.981-1.27-1.178zm-35.2-4.78c1.965-3.372 5.436-2.815 3.406 0.72l-0.492 0.852c-1.637 2.783-3.83 2.718-5.107 2.26l2.19-3.832zm27.93 5.009h-0.164c-1.111 0-1.996 0.917-1.996 2.03 0 0.426 0.131 0.818 0.328 1.146l-0.033 0.066c-1.605 2.75-5.5 2.226-3.471-1.311l2.586-4.485 1.637-2.782c2.391-4.061 5.664-2.456 3.602 1.08l-2.5 4.256z"></path></svg></svg>
			          </a>
			        </h1>
			        <button type="button" class="btn btn-navbar right" data-toggle="collapse" data-target=".main-nav">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>';
		
		$menu .= '<nav class="main-nav grid_10 collapse left" role="navigation">';
		
		$args = array('theme_location' => 'nav-menu','menu' => NULL,'container' => false,'container_class' => false,'container_id' => false,'menu_class' => NULL,'menu_id' => NULL,'echo' => false,'fallback_cb' => 'wp_page_menu','before' => NULL,'after' => NULL,'link_before' => NULL,'link_after' => NULL,'items_wrap' => '<ul class="left">%3$s</ul>','depth' => 0,'walker' => new Main_Nav_Menu());

		$menu .= wp_nav_menu($args);
		
		// /* Location Search box */
		// $city_id = get_location_pref();
		// $city_name = get_city_name($city_id);
		// if ($city_name == ""){
		// 	$city_name = "Enter a city (min 3 letters) ...";
		// }
		
		// 	$menu .= '<ul class="right">
		// 		<li class="location">
		// 			<form name="location_pref" action="" method="post">
		// 				<input type="text" name="th_location" id="th_location" placeholder = "'.$city_name.'">
		// 				<input type="hidden" name="th_location_id" id="th_location_id" value = "'.$city_id.'">
		// 			</form>
		// 		</li>';
		// if($city_id != ""){
		// 	$menu .= '
		// 		<li class="deleteLocation">
		// 			<form name="clear-location_pref" action="" method="post">
		// 				<input type="submit" id="clear-loc-pref" name="clear-loc-pref" value = "x">
		// 			</form>
		// 		</li>';
		// }
		// /* End of Location Search box */
		
		// if( is_user_logged_in( ) ) {
		// 	$menu .= '<li class="btnUpload">
		// 			<a href="'.bp_loggedin_user_domain().'upload/" id="btnUpload">Upload</a>
		// 		</li>
		// 		<li class="user-options">
		// 			<a  data-toggle="collapse" data-target="#u-options" href="'.bp_loggedin_user_domain().'"><span>'.bp_get_loggedin_user_fullname().' '.'</span>'.bp_get_loggedin_user_avatar( 'type=thumb&width=20&height=20' ).'</a>
		// 			<ul id="u-options">
		// 				<li id="profile"><a href="'.bp_loggedin_user_domain().'">Your Profile</a></li>
		// 				<hr />
		// 				<li><a href="'.bp_loggedin_user_domain().'profile/edit">Edit Your Profile</a></li>
		// 				<li><a href="'.wp_logout_url(TH_HOMEPAGE).'">Logout</a></li>
		// 			</ul>
		// 		</li>	
		// 		</ul>';
		// } else {
		// 	$menu .= '<li><a href="'.TH_HOMEPAGE.'/login">Login</a></li><li><a class="btn btn-primary" href="'.TH_HOMEPAGE.'/register">Sign up</a></li></ul>';
		// }
		$menu .= '</nav>
			</div>
		</header>';

		return $menu;
	}
	
	//!FOOTER MENU
	function get_footer_menu(){
		$args = array('theme_location' => 'nav-footer','menu' => NULL,'container' => false,'container_class' => false,'container_id' => false,'menu_class' => NULL,'menu_id' => NULL,'echo' => false,'fallback_cb' => 'wp_page_menu','before' => NULL,'after' => NULL,'link_before' => NULL,'link_after' => NULL,'items_wrap' => '%3$s','depth' => 0, 'walker' => new Footer_Nav_Menu());
		echo wp_nav_menu($args);
	}
	
	//!BASE MENU
	function get_base_menu(){
		$args = array('theme_location' => 'nav-base','menu' => NULL,'container' => false,'container_class' => false,'container_id' => false,'menu_class' => NULL,'menu_id' => NULL,'echo' => false,'fallback_cb' => 'wp_page_menu','before' => NULL,'after' => NULL,'link_before' => NULL,'link_after' => NULL,'items_wrap' => '%3$s','depth' => 0, 'walker' => new Footer_Nav_Menu());
		echo wp_nav_menu($args);
	}
	
	//!HOME
	function get_home_feature() { 
		if(!is_user_logged_in( )){
			echo '<a href="/register/"><img src="'.TH_HOMEPAGE.'/wp-content/themes/tattoohero/images/th_banner_file3e.jpg" width="100%"/></a>';
		}else{
			$args = array('post_type'=>'th_feature','order'=>'DESC');
			$feature = new WP_Query( $args );
			echo '
			<section class="feature flexslider">
		    <ul class="slides">
			';
			$i = 0;
			while($feature->have_posts()): $feature->the_post(); 
				$i++;
				$image_url = wp_get_attachment_image_src( get_post_thumbnail_id($feature->post->ID), 'full');
				$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($feature->post->ID), array(100,100));
				if (get_the_title() == "Upload Your Tattoos" && !(is_user_logged_in())){
				$list .= '
		        <li class="shop"  data-thumb="'.$thumb_url[0].'" data-title="'.get_the_title().'" data-description="'.get_the_excerpt().'">
		        <a href="/register/"><img src="'.$image_url[0].'"/></a></li>
				';
				}elseif (get_the_title() == "Upload Your Tattoos" && (is_user_logged_in())){
				$list .= '
		        <li class="shop" data-thumb="'.$thumb_url[0].'" data-title="'.get_the_title().'" data-description="'.get_the_excerpt().'">
		        <a href="'.bp_loggedin_user_domain().'upload/"><img src="'.$image_url[0].'"/></a></li>
				';
				}else{
				$list .= '
		        <li class="shop"  data-thumb="'.$thumb_url[0].'" data-title="'.get_the_title().'" data-description="'.get_the_excerpt().'">
		        <a href="'.get_the_content().'"><img src="'.$image_url[0].'"/></a></li>
				';
				}
			endwhile;
			wp_reset_postdata();
			echo $list;
			echo ' </section></ul>';
		}
		$args = array('post_type'=> 'th_blog', 'category_name' => 'featured', 'order'=> 'DESC');
	  
	  	$query = new WP_Query( $args );
			
		if ( $query->have_posts() ) {
			echo '<section class="feature-articles grid_12">
					<header><h2>Featured Articles</h2></header><div class="inner_container">';
			
			//$temp = NULL;
			
			while ($query->have_posts()): $query->the_post();
				$temp .= '<article class="post col_1of3">
								<a href="'.get_permalink().'">'.get_the_post_thumbnail().'</a>
								<div class="content">
									<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
									<div class="meta">
										<div class="date">'.get_the_date('F jS Y').'</div>
										<p>'.@$this->the_excerpt(get_the_excerpt(),200).'</p>
									</div>
								</div>
							</article>';
					
				
				/*echo '<article class="master col_1of3">
						<a href="';
				echo get_permalink();
				echo '">';
				echo get_the_post_thumbnail($query->ID);
				echo '</a>
						<div class="content">
							<h2><a href="';
						echo get_permalink();
						echo '">';
						echo get_the_title();
						echo '</a></h2>
							<div class="meta">
								<div class="shop">';
								echo the_field('shop');
								echo '</div>
									  <div class="location">';
								echo the_field('location');
								echo '</div>
							</div>
						</div>
					</article>';
					*/
			endwhile;
			
			$temp .= '</div></section>';
			
		}
		wp_reset_postdata();
		echo $temp;
	}

	
	//!LOGIN
	function get_login($u,$p) { 
		$results = array("success"=>false,"message"=>NULL);
		$login = array('user_login'=>$u,'user_password'=>$p,'remember'=>false);
		$user = wp_signon($login,false);
		if ( is_wp_error($user) ) { $results["message"] = '<p>'.$user->get_error_message().'</p>'; }
		else {
			$results['message'] = "<p>You are logged in.</p>";
			$results['success'] = true;
		}
		if(!$u && !$p) { 
			$results['message'] = "<p>You must enter a Username and Password</p>";
		}
		return $results;
	}
	
	//!USER PROFILE
	function get_main_profile(){
		global $bp;
		$userid = bp_loggedin_user_id();
		?>
		<div class="user_profile">
			<div class="user-avatar col_1of3">
				<?php bp_core_get_userlink($userid); ?>
				<a href="<?php echo bp_core_get_user_domain( $userid ); ?>">
					<?php echo bp_core_fetch_avatar ( array( 'item_id' => $userid, 'type' => 'full' ) ); ?>
				</a>
			</div><!-- .user-avatar -->			
			<div class="member-details col_2of3">		
				<h2>
					<a href="<?php echo bp_core_get_user_domain($userid); ?>"><?php echo bp_core_get_user_displayname($userid); ?></a>
				</h2>
				<h3 class="user-nicename">@<?php echo bp_core_get_username($userid); ?></h3>
				<div class="meta">
					<?php 
						$city 		= xprofile_get_field_data( "City" , $userid );
						$province 	= xprofile_get_field_data( "Province/State" , $userid );
						$country	= xprofile_get_field_data( "Country" , $userid );
						$memtype 	= xprofile_get_field_data( "Member Type" , $userid );
					?>
					<?php if($city): ?>
						<span class="user-city"><?=$city;?><?php if($province) { echo trim(','); } ?> </span>
					<?php endif; ?>
					<?php if($province): ?>
						<span class="user-province"> <?=$province;?><?php if($country) { echo trim(','); } ?></span>
					<?php endif; ?>
					<?php if($country): ?>
						<span class="user-country"> <?=$country;?></span>
					<?php endif; ?>
					<?php if($memtype): ?>
						<div class="user-memtype"> <?=$memtype;?></div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php
	}
	
	
	function share($title, $summary, $url){
		global $bp;
		$userid 	= bp_loggedin_user_id();
		
		$title		= (!isset($title) || trim($title) ==='') ? the_title('','', false) : $title;
		$title 		= urlencode($title);
		
		$summary 	= (!isset($summary) || trim($summary) ==='') ? translate("A community based website that connect tattoo enthusiasts with shops and artists.","tattoohero") : $summary;
		$summary 	= urlencode($summary);
		
		$ref 		= (!isset($url) || trim(url) === '') ? TH_HOMEPAGE : $url;
		$img 		= TH_TEMPLATE_DIR_IMAGES."/th_logo.jpg";
		//$img 		= urlencode($img);
		
		//if (is_user_logged_in()) {
			$socialButtons = '<ul class="sharelist">
			    <li class="share_fb">
			    	<a href="http://facebook.com/sharer/sharer.php?u=' . $ref . '&amp;t='. $title . '+' . $summary . '" target=_blank">Facebook</a></li>
			    <li class="share_twitter">
			    	<a href="http://twitter.com/intent/tweet?original_referer='. urlencode($ref) .'&amp;text=' . $title . ',+' . $summary . '&amp;url=' . urlencode($ref) . '&amp;via=tattooherocom" title="Share on Twitter" target="_blank">Twitter</a></li>
			    <li class="share_pinterest">
			    	<a href="http://pinterest.com/pin/create/button/?url=' . $ref . '&media='. $img .'&description=' . $title . ', ' . $summary . '  target="_blank">Pinterest</a></li> 
			    <li class="share_gplus">
			    	<a href="https://plus.google.com/share?url=' . $ref .' target="_blank">Google Plus</a></li> 
			    <li class="share_digg">
			    	<a href="http://digg.com/submit?phase=2&amp;url=' . $ref .'&amp;title=' . $title . ' target="_blank">Digg</a></li> 
			    <li class="share_su">
			    	<a href="http://stumbleupon.com/submit?url=' . $ref . '&amp;title=' . $title . ' target="_blank">StumbleUpon</a></li>
		     </ul>';
		     
		     echo $socialButtons;
		     return false;
	     //}
	}
	//!ERRORS
	function show_errors() { 
		if($codes = th_errors()->get_error_codes()) {
			echo '<div class="th_message errors">
					<h4>Sorry, but we gotta fix all issues before continuing...</h4>';
				
			    // Loop error codes and display errors
			   foreach($codes as $code){
				   	$errorType = th_errors()->get_error_code($code);
				   	$errorData = th_errors()->get_error_data($code);
			        $message = th_errors()->get_error_message($code);
			        echo '<div class="th_error"><a href="#'.$errorData.'">' . __($errorData, 'tattoohero') . '</a>: '. $message . ' ('.$errorType. ')</div>';
			    }
			echo '</div>';
		}	
	}
	
	
	function get_blog_posts() {
		$temp = NULL;
		
		$current = ($_GET['post']) ? $_GET['post'] : 1;
		    
		$args = array('post_type'=>'th_blog', 'posts_per_page' => 12,'order'=>'DESC', 'paged' => $current);
		$blog_query = new WP_Query( $args );
		
		$temp = '<section class="blog-posts grid_12">
					<div class="inner_container">';
						
		if ($blog_query->have_posts()): while ($blog_query->have_posts()): $blog_query->the_post();
			$temp .= '<article class="post col_1of3">
							<a href="'.get_permalink().'">'.get_the_post_thumbnail($blog_query->ID).'</a>
							<div class="content">
								<header>
									<h2><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
									<div class="meta">
										<span class="date">'.get_the_date('F jS Y').'</span>
									</div>
								</header>
								<div class="excerpt">
									<p>'.@$this->the_excerpt(get_the_excerpt(),200).'</p>
								</div>
								<footer>
									<div class="meta">
										<div class="comments"></div>
									</div>
								</footer>
							</div>
						</article>
					';
		endwhile; endif; 
		wp_reset_postdata();
		
		$pagination = array(
	        'base' => add_query_arg( 'post', '%#%' ),
	        'format' => '?post=%#%',
	        'total' => $blog_query->max_num_pages,
	        'current' => $current,
	        'prev_text' => __('Prev'),
	        'next_text' => __('Next'),
	        'type' => 'plain'
		);
    
		$temp .= '</div>
			</section><div class="clr"></div>' . paginate_links( $pagination );
		
		return $temp;

	}
	function get_blog_posts_sidebar($thpostID){
		$temp = NULL;
		$args = array('post_type'=>'th_blog','order'=>'DESC');
		$blog_query = new WP_Query( $args );
		if ($blog_query->have_posts()): while ($blog_query->have_posts()): $blog_query->the_post();
			//if ($blog_query->ID == $thpostID){
			if ($thpostID != $blog_query->post->ID){

			$temp .= '
				<article class="post col_1of3">
					<a href="'.get_permalink().'">'.get_the_post_thumbnail($blog_query->ID).'</a>
					<div class="content">
						<header>
							<h2><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
							<div class="meta">
								<span class="date">'.get_the_date('F jS Y').'</span>
								<span class="author">'.get_the_author_link().'</span>
							</div>
						</header>

						<footer>
							<div class="meta">
								<div class="comments">17 comments</div>
							</div>
						</footer>
					</div>
				</article>
			';
			}
		endwhile; endif; wp_reset_postdata();
		return $temp;
	}


	function get_artist_names(){
		$users = get_users();
		$x=0;
		$user = array();
		foreach($users as $u):
			$user[$x]['name'] = $u->user_nicename;
			$user[$x]['id'] = $u->ID;
			$x++;
		endforeach;
		return $user;
	}

	/**
	 *	@function	admin_menu
	 *	@params 	<void>
	 *	@return 	<void>
	 */	
	function admin_menu() { @$this->menu_pages(); }

	/**
	 *	@function	menu_pages
	 *	@params 	<void>
	 *	@return 	<void>
	 */	
	function menu_pages() {
		add_menu_page('General Settings', 'General Settings', 'administrator', 'general_settings', array($this,'menu_page_general'));
	}


	/**
	 *	@function	menu_page_general
	 *	@params 	<void>
	 *	@return 	<void>
	 */	
	function menu_page_general() {

		if( class_exists( 'TH_BLOCK' )):
			$block = new TH_BLOCK();
			$block->add_tab('general_settings','General Settings',array(&$this,'general_settings'));
			$block->draw_block('General Settings','Theme: Tattoo Hero');
		else:
			echo '<div id="message" class="updated">Please install TH ADMIN Core</div>';
		endif;		
	}

	function create_alpha_key($source) {
		global $wpdb;
		$wpdb->alpha_keys = $wpdb->prefix . 'alpha_keys';
		$key = md5($source.date('y'));
		// echo $wpdb->alpha_keys;
		$data = $wpdb->query($wpdb->prepare("INSERT INTO $wpdb->alpha_keys (Key,Source) VALUES (%s,%s)", $key, $source));
		// $data = $wpdb->query($wpdb->prepare("INSERT INTO $wpdb->alpha_keys (Key,Source) VALUES (%s,%s)", $key, $source));
		// echo $data;
		if($data) {
			return true;
		}

	}

	function general_settings(){
		global $wpdb;
		$wpdb->alpha_keys = $wpdb->prefix . 'alpha_keys';
		$hidden_field_name = md5('generate_key');
		if(isset($_POST[$hidden_field_name])) {
			$source = $this->clean_up_post($_POST['key_source']);
			if($_POST['key_source']) {
				$insert = $this->create_alpha_key($_POST['key_source']);
				if($insert){
					$msg = '<div class="alert alert-success"><p>Your Key was successfully generated</p></div>';
				} else {
					$msg = '<div class="alert alert-error"><p>Something went wrong</p></div>';
				}
			} else {
				$msg = '<div class="alert alert-error"><p>Make sure to fill out the field</p></div>';
			}
		}
		echo $msg;
		$keys = $wpdb->get_results("SELECT * FROM $wpdb->alpha_keys");
		?>
		<style>
			.keys ul { list-style: none; } 
			.keys li {  padding: 10px; }
			.keys li:nth-child(even) { background: #efefef; }
		</style>
		<form method="post">
			<div class="inner_container">
				<div class="col_1of2">
					<input type="hidden" name="<?=$hidden_field_name;?>" value="1" />
					<input type="text" name="key_source" class="widefat" placeholder="Key Source" />
					<button type="submit" class="button button-primary">Generate</button>
				</div>
			</div>
		</form>
		<div class="keys">
			<ul>
			<?php if($keys) : foreach($keys as $key): ?>
					<li>
						<p>URL: http://www.tattoohero.com/alpha/?key=<?=$key->Key;?></p>
						<p>Source: <?=$key->Source; ?></p>
					</li>
			<?php endforeach; endif; ?>
			</ul>
		</div>
		<?php
	}
}


$TH = new TH();
global $TH;

else : exit("Class 'TH' already exists"); 
endif;

if (isset($TH)) {
	/* Actions */
	if(is_admin()){
		add_action('admin_menu', array(&$TH, 'admin_menu'));
		add_action('admin_print_styles', array(&$TH, 'admin_print_styles'));
		add_action('admin_print_scripts', array(&$TH, 'admin_print_scripts'));
	}
	add_action('init',array(&$TH,'initialize'));
	//add_action('wp_print_scripts', array(&$TH, 'jquery'));
	add_filter('wp_trim_excerpt', array(&$TH, 'new_excerpt_more' ));
	add_action('wp_enqueue_scripts', array(&$TH, 'deregister_styles'));
	add_theme_support('post-thumbnails', array('post','page','th_feature', 'th_blog', 'th_artists'));
	
}


/*  DEBUGGING  */
function printArray($array){
	foreach ($array as $key => $value){
		echo "$key => $value".'<br/>';
		if(is_array($value)){ //If $value is an array, print it as well!
		    printArray($value);
		}  
	} 
}


function block_wp_admin_init() {
	/*if (strpos(strtolower($_SERVER['REQUEST_URI']),'/wp-admin/') !== false) {
		if ( !is_site_admin() ) {
			wp_redirect( get_option('siteurl'), 302 );
		}
	}*/
	
	if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
		wp_redirect( home_url() );
		exit;
	}
}
add_action('admin_init','block_wp_admin_init',0);
show_admin_bar(false);

function set_bp_avatar_constants() {
	define ( ‘BP_AVATAR_FULL_WIDTH’, 300 );
	define ( ‘BP_AVATAR_FULL_HEIGHT’, 300 );
}
add_action( 'bp_loaded', 'set_bp_avatar_constants', 8 );

add_action("login_head", "my_login_head");
function my_login_head() {
	echo "
	<style>
	body.login #login h1 a {
		background: url('".get_bloginfo('template_url')."/images/th_wp_logo.png') no-repeat scroll center top transparent;
		height: 90px;
		width: 330px;
	}
	</style>
	";
}

bp_core_remove_nav_item('Example');

function th_store_alpha_source_key($key){
	global $wpdb;
	$wpdb->th_alpha_keys = $wpdb->prefix . "alpha_keys";
	$keyInfo = $wpdb->get_results("SELECT * FROM $wpdb->alpha_keys WHERE key = $key;");
	//print_r($keyInfo);
}

//registration redirect so that the thank you page is recognised by google analytics
function thankyou_redirect(){
	if ( 'completed-confirmation' == bp_get_current_signup_step() ) {
		wp_redirect( home_url().'/thankyou/');
		exit();
	}
}
add_action('bp_head','thankyou_redirect');


function th_on_logout() {
    session_destroy();
}
add_action('wp_logout', 'th_on_logout');



//! UPLOADED IMAGES AJAX
function th_new_images_form(){
	$albumID = $_POST['albumId'];
	//echo('<h1>ALBUM ID = '.$albumID.'</h1>');
	//echo('<h1>ALBUM ID POST = '.$_POST['albumId'].'</h1>');
	
	if(isset($albumID)) th_edit_new_images_form($albumID, -1);//-1 == all		
	
	die();
}
add_action("wp_ajax_th_new_images_form", "th_new_images_form");



//displays a multi-image form to add the meta field data 
function th_edit_new_images_form($albumID, $size){
	$num = $size || 6;
	
	//printf('this is where the new form will go');
	/*while( list( $field, $value ) = each( $_POST )) {
	   echo "<p>" . $field . " = " . $value . "</p>\n";
	}
	*/
	
	$exists = false;
	while( list( $field, $value ) = each( $_POST )) {
		if(ctype_digit($field)) $exists = true;
	}
	//print_r('exists= '.($exists? 'yes':'no'));
	
	if ($exists && isset($_POST['submit'])){//array_key_exists('submit', $_POST)){//
		//print_r('th_edit_new_images_form(), entered update()<br/>');
		th_update_image_meta();
		th_gallery_album($albumID, $num);		
	} else {
		//store album ID in case of page refresh
		if($_SESSION['albumIDs'] == null) $_SESSION['albumIDs'] = array($albumID);
		else if(!in_array($albumID, $_SESSION['albumIDs']))	array_push($_SESSION['albumIDs'], $albumID);
				
		$meta_query = array(
		    array(
		        'key' => 'firstupdate',
		        'value' => NULL,
		        'compare' => 'NOT EXISTS'
		    )
		);

		$images = array();
		$images = th_get_media(array('meta_query' => $meta_query, 'posts_per_page'=>$num, 'post_parent'=>$albumID));
				
		if (sizeof($images) > 0)	th_edit_image_form($albumID, $images);
		else						th_gallery_album($albumID, $num);
	}
}


//!USER IS ARTIST CHECK
function th_isArtist($user){
	$memberType = xprofile_get_field_data('Member Type', $user);
	//if ($memberType == 'Tattoo Artist' || $memberType == 'Tattoo Artist (Apprentice)') return true;
	//else return false;
	return ($memberType == 'Tattoo Artist' || $memberType == 'Tattoo Artist (Apprentice)'); 
}


//!ARTIST AUTOCOMPLETE OPTIONS FOR ALBUM IMAGES
function th_artists_options(){
	global $bp; 
	
	$user_id = bp_loggedin_user_id();
	//unset($_SESSION['albumIDs']);
	$randID = rand(0, 9999);
	
?>	
	<div class="artist_options" id="<?php echo $randID; ?>">
		<input type="checkbox" id="same_artist<?php echo $randID; ?>" name="same artist" />
		<label for="same_artist<?php echo $randID; ?>" id="same_artist_label<?php echo $randID; ?>"><h2>Same artist for all images</h2></label>
		<div class="hide">
<?php if (th_isArtist($user_id)):?>			  
			  <input type="radio" id="i_am" name="artist" value="<?php echo bp_core_get_user_displayname($user_id); ?>"><span>I am the artist</span>
			  <input type="radio" id="selectableArtist" name="artist" value="" />
<?php else: ?><input type="radio" id="selectableArtist" class="hide" name="artist" value="" />Artist: 
<?php endif;?>
			  <input type="text" id="singleArtist<?php //echo $randID; ?>" class="singleArtist" name="artist_name" placeholder="Artist" value="" />
		</div>
	</div>
	<script>
		jQuery(function() {
			//this does not work with multiple albums???????
			/*$artistOptions = $('div.artist_options#<?php echo $randID; ?>');
			$hideBox  		 = $artistOptions.find('div.hide');
			$selArtist		 = $hideBox.find('input#selectableArtist');
			$singleArtist	 = $artistOptions.find("#singleArtist");
			$sameArtist		 = $('#same_artist_label<?php echo $randID; ?>');
			*/
			//console.log('<?php echo $randID; ?>');
			
			/* Activate artist options */
			jQuery('#same_artist_label<?php echo $randID; ?>').click(function(){
				jQuery(this).next().slideToggle();
			});
			
			//Artist options: selecting the artist name
			function artistSelect(item, options){
				if(jQuery(item).is(':checked')){
					name = jQuery(item).val();
					if (name.length > 0){
						jQuery('div.artist_options#<?php echo $randID; ?>').parent().find(".artists").each(function(){
							$item = jQuery(this);
							$item.find('input[type=text]').val(name);
							if(typeof options != 'undefined'){
								$item.find('input[type=hidden]').val( options.value );
								$item.find('.artistDescription').html( options.desc );
							}
						});
					}
				}
			}; 
			
			jQuery('div.artist_options#<?php echo $randID; ?>').find('div.hide').children(':radio').each(function(){
				jQuery(this).click(function(event, data){ //jquery CANNOT be in no conflict mode
					(jQuery(this).attr('id') == "selectableArtist") ? artistSelect(this, data) : artistSelect(this);
				})
			});
			
			//text field click
			jQuery('div.artist_options#<?php echo $randID; ?>').find("#singleArtist").click(function(){
				//activte radio button
				jQuery('div.artist_options#<?php echo $randID; ?>').find('input#selectableArtist').prop("checked", true);
			})
			//catch key input
			.keyup(function(e){
				// get keycode of current keypress event
				var code = (e.keyCode ? e.keyCode : e.which);
	    
				// do nothing if it's an arrow key
			    if(code == 37 || code == 38 || code == 39 || code == 40) {
			        return;
			    }
				artistName = jQuery( this ).val();
				if (artistName.length >= 3)
					jQuery('div.artist_options#<?php echo $randID; ?>').find('input#selectableArtist').val(artistName).trigger('click');
			}).keyup();
			
			
			jQuery('div.artist_options#<?php echo $randID; ?>').find("#singleArtist").autocomplete({
				minLength: 0,
				source: projects,
				focus: function( event, ui ) {
					jQuery( this ).val( ui.item.label );
					jQuery('div.artist_options#<?php echo $randID; ?>').find('input#selectableArtist').prop("checked", true);
					
					return false;
				},
				select: function( event, ui ) {
					jQuery('div.artist_options#<?php echo $randID; ?>').find('input#selectableArtist').val( ui.item.label )
						.trigger('click', {value : ui.item.value, desc : ui.item.desc});
					return false;
				}
			})
			
			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			  	return jQuery( "<li>" )
				    .append( "<a>" + item.label + "<br>" + item.desc + "</a>")
				    .appendTo( ul );
			};		
		});
	</script>
<?php	
}
//add_action('th_artists_options','th_artists_for_images_options');

function th_current_page(){
	global $tpl_name;
	//print_r('name: '.$tpl_name);
	if($tpl_name == null) return false;
	else return $tpl_name;
}

//!ALBUM WITH EDIT FIELDS
function th_edit_image_form($albumID, $images){
	//print_r('th_edit_image_form($albumID, $images)');
	//console.log('th_edit_image_form()');
	//$album = $albumID || null;
	
	/*	ALBUM TITLE							*/
	if($albumID != null && (!th_current_page() || th_current_page() == "details2")): ?>
	<h2><?php th_album_title($albumID); ?></h2>
	<?php endif;
	
	/*	PREPARE ARTIST AUTOCOMPLETE DATA 	*/
	artistselectfield();
	?>

	<div class="inner_container verticalSpacer">
		<?php /*		ARTIST ENTRY OPTIONS WITH UNIQUE ID		 */
			th_artists_options();
		?>	
		<form method="post" id="<?php echo $albumID; ?>" enctype="multipart/form-data">
			<ul class="edit-list">
				<?php
					$unique = rand(0, 9999);					
					$i = 0;				
					
					foreach ($images as $image){
					 	$artistid 		= '';
					 	$artistname 	= '';
					 	$artistlocation = '';
					 	
					 	//printf('Artist ID: '.$image['artist_id']);
					 	if (is_numeric($image['artist_id'])){				
					 		$artistid 		= $image['artist_id'];
					 		$artistdata 	= get_userdata($artistid);
					 		$artistname 	= xprofile_get_field_data('Name', $artistid);
					 		$artistlocation = th_get_user_location($image['artist_id']);
				
					 	}else $artistname 	= $image['artist_id'];
					 								
				?>			
				<li class="col_1of3">
					<ul>
						<li class="media-<?=$image['post_id'];?>">
							<a href="<?=$image['permalink'];?>">
								<?php $image_attr = wp_get_attachment_image_src($image['post_id'], 'medium'); ?>
								<img class="lazy" data-original="<?php echo $image_attr[0]; ?>" width="<?php echo $image_attr[1]; ?>" height="<?php echo $image_attr[2]; ?>" />
							</a>
						</li>
						<li class="media-<?=$image['post_id'];?>-form fieldContent">
							<input type="hidden" id="postid" name="<?php echo $unique.$i ?>[postid]" value = "<?php echo $image['post_id'] ?>" >
							<!--<label for="up-title">Title:</label>-->
						    	<input type="text" name="<?php echo $unique.$i?>[title]" value="<?php echo $image['title'] ?>" placeholder="<?php _e('Title', 'tattoohero') ?>">
						    <!--<label for="up-description">Description:</label>-->
						    	<textarea rows="3" name="<?php echo $unique.$i?>[description]" placeholder="<?php _e('Description, #hashtags, @users', 'tattoohero') ?>"><?php echo $image['description'] ?></textarea>
						</li>
						<?php echo (autocompleteartist($unique.$i)); ?>
						<li class="media-<?=$image['post_id'];?>-artist artists">
							<!--<label for="up-artist<?php echo ($i); ?>">Artist:</label>-->
							<!-- <img id="project-icon" src="images/transparent_1x1.png" class="ui-state-default" alt="" /> -->
							<input type="text" id="up-artist<?php echo ($unique.$i); ?>" name="<?php echo $unique.$i?>[artist]" value="<?php echo $artistname ?>" placeholder="<?php _e('Artist', 'tattoohero') ?>" required>
							<input type="hidden" id="up-artist-id<?php echo ($unique.$i);?>" name="<?php echo $unique.$i?>[artist_id]" value="<?php echo $artistid ?>" />
							<p id="up-artist-description<?php echo ($unique.$i); ?>" class="artistDescription"><?php echo $artistlocation ?></p>
						</li>
					</ul>
				</li>
				<!--<div class="clr"></div>-->

				<?php $i++; 
			
			} ?>
			</ul>
			<input type="hidden" value="0"/>
			<input type="submit" <?php //if(!th_current_page() || th_current_page() == "details2") echo 'class="hide"'; ?> name="submit" value="Submit" />
		</form>
	</div>
	<?php
}


function th_update_image_meta(){
	foreach ($_POST as $subpost){
		$postid = $subpost['postid'];
		$postarray = array();
		$postarray['ID'] = $postid;
		$postarray['post_title'] = $subpost['title'];
		$postarray['post_content'] = $subpost['description'];
		wp_update_post( $postarray );

		update_post_meta($postid, 'firstupdate', 1);
		if($subpost['artist'] == xprofile_get_field_data('Name', $subpost['artist_id'])){
	    	update_post_meta($postid, 'artist', $subpost['artist_id']);
	    }else{
	    	update_post_meta($postid, 'artist', $subpost['artist']);
	    }
	}
}
function th_get_user_location($userid){
  $tempstring = '';
  $tempstring_set = false;
  $city = xprofile_get_field_data('City', $userid);
  $prov_state = xprofile_get_field_data('Province/State', $userid);
  $country = xprofile_get_field_data('Country', $userid);
  if($city){
    $tempstring = $city;
    $tempstring_set = true;
  }
  if($prov_state){
    if($tempstring_set){
      $tempstring = $tempstring.', ';
    }
    $tempstring = $tempstring.$prov_state;
    $tempstring_set = true;
  }
  if($country){
    if($tempstring_set){
      $tempstring = $tempstring.', ';
    }
    $tempstring = $tempstring.$country;
    $tempstring_set = true;
  }
  return $tempstring;
}

function th_hide_image(){
	if (current_user_can('level_10') & $_GET['a'] == 'hide') {
		update_post_meta($_GET['postid'], 'bp_media_privacy', 10);
		update_post_meta($_GET['postid'], 'privacy_changed_by', bp_loggedin_user_id() );
	}

}
add_action('bp_head', 'th_hide_image');

function th_delete_post(){
	if($_GET['a'] == 'delete'){
		wp_redirect('/');
	}
}
add_action('template_redirect', 'th_delete_post');


function th_insurance_send(){
	if (array_key_exists('ins_name', $_POST)){
		$captcha_instance = new ReallySimpleCaptcha();
		if($captcha_instance->check( $_POST['ins_captcha_prefix'], $_POST['ins_captcha_response'] )){
			$captcha_instance->remove( $_POST['ins_captcha_prefix']);
			$to = "steve@tattoohero.com";
			$subject = "Request for Quote";
			$message = "Name: ".$_POST[ins_name]."\n"."Email: ".$_POST[ins_email]."\n"."Shop Name: ".$_POST[ins_shop];
			$success = wp_mail($to , $subject , $message);
			if ($success){
				wp_redirect(home_url());
				exit();
			}
		}else{
			$captcha_instance->remove( $_POST['ins_captcha_prefix']);
			printf("Captha Key was wrong. Try again.");
		}
		//wp_redirect(home_url());
	}

}
add_action('template_redirect', 'th_insurance_send');

function oa_social_login_set_custom_css($css_theme_uri)
{
	//Added to use custom CSS
	//$css_theme_uri = 'http://public.oneallcdn.com/css/api/socialize/themes/buildin/connect/large-v1.css';
	
	$css_theme_uri = TH_TEMPLATE_DIR.'/style.css';

	//Done
	return $css_theme_uri;
}
add_filter('oa_social_login_default_css', 'oa_social_login_set_custom_css');
add_filter('oa_social_login_widget_css', 'oa_social_login_set_custom_css');
 

// STEVES FILES
require_once('upload-file.php');
require_once('th_bp_media_changes.php');
require_once('th_bp_media_query.php');
require_once('th_vlc_functions.php');
require_once('template-uploadtest.php');
require_once('th_location_functions.php');
require_once('groups/group-meta-fields.php');
require_once('groups/group-meta-filter.php');
//require_once('scripts/js_loader.php'); //included in this->th_load_scripts
?>
