<?php if(isset($_REQUEST['key'])) { th_store_alpha_source_key($_REQUEST['key']); } 
global $tpl_name;
//global $aIDs;
if (!$tpl_name) { $tpl_name = 'default'; }

global $TH;

profileIncompleteRedirect();
//update_user_meta(bp_loggedin_user_id(), 'profileDetailsStep', 1);

?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9"> <![endif]-->
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/Article">
	<head>
		<link rel="shortcut icon" type="image/x-icon" 				href="<?=TH_TEMPLATE_DIR;?>/favicon.ico" />
		<link rel="apple-touch-icon-precomposed" sizes="114×114" 	href="<?=TH_TEMPLATE_DIR;?>/apple-touch-icon-114×114-precomposed.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72×72" 		href="<?=TH_TEMPLATE_DIR;?>/apple-touch-icon-72×72-precomposed.png" />
		<link rel="apple-touch-icon-precomposed" 					href="<?=TH_TEMPLATE_DIR;?>/touch-icon-iphone-precomposed.png" />
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" 		content="width=device-width,minimum-scale=1" />
		<meta name="description" 	content="<?php get_bloginfo('description', 'display'); ?> "/>
		
		<title>
			<?php 
				global $page, $paged; 
				wp_title( '|', true, 'right' ); 
				bloginfo('name'); 
				$site_description = get_bloginfo('description', 'display'); 
				if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description"; 
			?>
		</title>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-36983999-5', 'tattoohero.com');
		  ga('send', 'pageview');
		
		</script>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		
		<?php do_action( 'bp_head' ); ?>
		<?php wp_head(); ?>
	</head>		
	<body <?php body_class(); ?>>
		<div id="fb-root"></div>
		<?php do_action( 'bp_before_header' ); ?>
		<?=$TH->get_menu(); ?>
		<div class = "main-adspace">
			<div class="container">
				<?php 
					if(function_exists('oiopub_banner_zone') & !(is_front_page()) & !($pagename == "insurance")) {
						if(is_user_logged_in() && th_isArtist(bp_loggedin_user_id())) oiopub_banner_zone(3, 'center');
						else	oiopub_banner_zone(1, 'center');
					}
				?>
			</div>
		</div>
		<div class="spacer">