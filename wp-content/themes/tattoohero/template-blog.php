<?php /* TEMPLATE NAME: BLOG */ ?>
<?php get_header(); ?>
	<div class="subpage container blog_posts">
			<?php do_action( 'bp_before' ); ?>
			<h2><?php _e('Articles','tattoohero'); ?></h2>
			<?=$TH->get_blog_posts(); ?>
	</div>

<?php get_footer(); ?>
