<?php 
/* TEMPLATE NAME: THANK YOU */ 
$tpl_name = 'thankyou';

get_header(); 
	
global $TH;
?>
	<div class="subpage container thankyou">
		<div class="grid_12">

			<?php do_action( 'bp_before' ); ?>

			<div role="main">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<h2 id="thanks"><?php the_title(); ?></h2>

					<?php the_content(); ?>

				<?php endwhile; endif;
				$TH->showSocialConnects('inverse'); ?>
			</div>
		</div><!-- .grid_12 -->
	</div>

<?php get_footer(); ?>
